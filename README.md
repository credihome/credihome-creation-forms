# credihome-creation-forms

> Conjunto de componentes que formam o formulário de cadastro de leads da Credihome

[![NPM](https://img.shields.io/npm/v/credihome-creation-forms.svg)](https://www.npmjs.com/package/credihome-creation-forms) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save credihome-creation-forms
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'credihome-creation-forms'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [](https://github.com/)
