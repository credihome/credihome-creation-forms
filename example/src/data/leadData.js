export const leadData = {
  analyst: 170,
  manager: 106,
  createdAt: "2020-10-02 21:12:35.756",
  source: "dashboard",
  email: "email@email.com",
  name: "Teste exemplo",
  simulation: {
    duration: 200,
    mortgageValue: 250000,
    type: "mortgage",
    realtyPrice: 400000.51,
    firstPayment: 2615.5
  },
  leadId: "fe6c3340-04f3-11eb-afa5-2142a562d4b4",
  cpf: "86205340097",
  dateOfBirth: "2000-01-01",
  facId: 43976,
  phone: "11111111111",
  buyers: [
    {
      name: "Teste exemplo",
      cpf: "86205340097",
      phones: [
        {
          type: "residential",
          number: "22222222222"
        },
        {
          type: "cellphone",
          number: "11111111111"
        }
      ],
      dateOfBirth: "2000-01-01",
      email: "email@email.com",
      maritalStatus: "single",
      // stableUnion: true,
      // stableUnionDate: "2020-05-01",
      // maritalStatus: "married",
      // maritalStatus: "divorced",
      // marriageSystem: "separate_property",
      // weddingDate: "2020-05-14",
      homeCountry: "Brasil",
      // spouse: {
      //   name: "Teste parceiro",
      //   cpf: "83166844112",
      //   dateOfBirth: "1993-06-12",
      //   liveTogether: false
      // }
    },
  ],
  realty: {
    valuationContact: {

    }
  },
  status: "new",
  lastSimulation: {
    duration: 200,
    mortgageValue: 250000,
    type: "mortgage",
    realtyPrice: 400000.51,
    firstPayment: 2615.5
  },
  partnerEtapa: "analise_credito",
  partnerStatus: "active",
  proposalStatus: "pending",
  proposalId: 11185,
  partnerName: "GoBacklog",
  partnerId: 6850,
  proposalTeam: {
    commercialManager: 106,
    operationalManager: 59,
    proposalConsultant: 98,
    proposalCoordinator: 95,
    leadConsultant: 170,
    creditAnalyst: 70
  },
  leadStatus: "won"
}

export const leadDataTwoBuyers = {
  analyst: 147,
  buyers: [
    {
      address: {
        city: "São Paulo",
        complement: "Boate Azul",
        neighborhood: "Pinheiros",
        number: "09",
        residenceTime: 108,
        streetName: "Rua Amaro Cavalheiro",
        type: "owned",
        uf: "SP",
        zipcode: "05425010"
      },
      company: {
        name: "MUITA GRANA INC"
      },
      contractType: "employee",
      cpf: "63067922018",
      dateOfBirth: "1990-01-01",
      dateOfEmployment: "2006-10-30",
      docs: [
        {
          category: "identification",
          docNumber: "434.545-0",
          issueDate: "2011-09-11",
          issuedBy: "SSP",
          issueLocation: "SP",
          type: "rg"
        }
      ],
      email: "leona78@gmail.com",
      homeCountry: "Brasil",
      homeState: "SP",
      homeTown: "Agudos",
      jobTitle: "advogado",
      jobType: "advogado",
      maritalStatus: "single",
      monthlyIncome: 3000,
      name: "Leandro Nardonni",
      phones: [
        {
          number: "11941133368",
          type: "cellphone"
        },
        {
          number: "1144482635",
          type: "residential"
        },
        {
          number: "11914433357",
          type: "business"
        }
      ],
      stableUnion: false
    },
    {
      address: {
        city: "São Paulo",
        complement: "Alien",
        neighborhood: "Pinheiros",
        number: "16",
        residenceTime: 36,
        streetName: "Rua Amaro Cavalheiro",
        type: "owned",
        uf: "SP",
        zipcode: "05425010"
      },
      company: {
        name: "MUITA GRANA INC"
      },
      contractType: "self_employed",
      cpf: "39145270058",
      dateOfBirth: "1992-10-30",
      dateOfEmployment: "2006-10-30",
      docs: [
        {
          category: "identification",
          docNumber: "754.234-4",
          issueDate: "2011-12-12",
          issuedBy: "SSP",
          issueLocation: "SP",
          type: "rg"
        }
      ],
      email: "will@da.com",
      homeCountry: "Brasil",
      homeState: "RJ",
      homeTown: "Angra dos Reis",
      jobTitle: "ascensorista",
      jobType: "agenteDeViagem",
      maritalStatus: "single",
      monthlyIncome: 2000,
      name: "Ana Lucia Gardal",
      phones: [
        {
          number: "11914433357",
          type: "cellphone"
        },
        {
          number: "1144482635",
          type: "residential"
        },
        {
          number: "11941133357",
          type: "business"
        }
      ],
      stableUnion: false
    }
  ],
  cpf: "63067922018",
  createdAt: "2020-12-16 19:54:28.764",
  dateOfBirth: "1990-01-01",
  email: "leona78@gmail.com",
  facId: 45043,
  lastSimulation: {
    duration: 360,
    exibirNomesOperadores: true,
    firstPayment: 2495.1,
    mortgageValue: 280000,
    parseBankName: true,
    realtyPrice: 520651.3,
    type: "mortgage"
  },
  leadId: "81c5c5c0-3fd8-11eb-839a-e354059d6590",
  leadLostReason: null,
  leadStatus: "won",
  manager: 23,
  name: "Leandro Nardonni",
  partnerEtapa: "analise_credito",
  partnerId: 3214,
  partnerName: "Site CrediHome",
  partnerStatus: "active",
  phone: "11941133368",
  processId: "fb5002b0-3fd9-11eb-8bd4-c7b91a6bbe6b",
  proposalCreatedAt: "2020-12-16 20:05:17",
  proposalId: 11754,
  proposalLostReason: null,
  proposalStatus: "pending",
  proposalTeam: {
    commercialManager: 23,
    leadConsultant: 147,
    proposalConsultant: 147
  },
  realty: {
    age: 0,
    city: "Americana",
    hasRealty: false,
    monthlyFees: 0,
    purchaseTime: "oneMonth",
    realtyType: "condo",
    settlementDebt: 0,
    totalArea: 0,
    uf: "SP",
    useType: "residence"
  },
  score: {
    scoreContact: {
      ffid: {
        label: "ffid",
        score: 3.3
      },
      preenchimento: {
        label: "66.67%",
        score: 66.67
      },
      value: 66
    },
    scoreCredit: {
      cr: {
        label: "+40%",
        score: 0
      },
      idade: {
        label: "26 a 40",
        score: 5
      },
      Imovel_Definido: {
        label: "nao",
        score: 1
      },
      ltv: {
        label: "31% a 60%",
        score: 5
      },
      populacao: {
        label: "200K a 300K",
        score: 3
      },
      renda: {
        label: "2.5K a 4.0K",
        score: 2
      },
      Tempo_De_Compra: {
        label: "30 a 90 dias",
        score: 4
      },
      Tipo_De_Renda: {
        label: "Assalariado / Autonomo",
        score: 3
      },
      Tipo_Do_Imovel: {
        label: "Apartamento",
        score: 5
      },
      UF_Do_Imovel: {
        label: "SP",
        score: 5
      },
      Valor_Da_Entrada: {
        label: "Sim",
        score: 5
      },
      Valor_Do_Financiamento: {
        label: "201K a 401K",
        score: 3
      },
      Valor_Do_Imovel: {
        label: "501K a 1M",
        score: 5
      },
      value: 75
    },
    scoreVersion: "v00.C.x.XX"
  },
  simulation: {
    duration: 360,
    firstPayment: 2495.1,
    mortgageValue: 280000,
    realtyPrice: 520651.3,
    type: "mortgage"
  },
  source: "whitelabel",
  status: "new",
  userId: 147793
}

export const spouse = {
   analyst: 170,
   manager: 106,
   createdAt: "2020-10-02 21:12:35.756",
   source: "dashboard",
   email: "email@email.com",
   name: "Teste exemplo",
   simulation: {
      duration: 200,
      mortgageValue: 250000,
      type: "mortgage",
      realtyPrice: 400000.51,
      firstPayment: 2615.5
   },
   leadId: "fe6c3340-04f3-11eb-afa5-2142a562d4b4",
   cpf: "86205340097",
   dateOfBirth: "2000-01-01",
   facId: 43976,
   phone: "11111111111",
   buyers: [
      {
         name: "Teste exemplo",
         cpf: "86205340097",
         phones: [
            {
               type: "residential",
               number: "22222222222"
            },
            {
               type: "cellphone",
               number: "11111111111"
            }
         ],
         dateOfBirth: "2000-01-01",
         email: "email@email.com",
         maritalStatus: "single",
         // maritalStatus: "married",
         // marriageSystem: "separate_property",
         // weddingDate: "2020-05-14",
         stableUnion: true,
         // stableUnion: false,
         stableUnionDate: "2020-05-01",
         homeCountry: "Brasil",
         spouse: {
            name: "Teste parceiro",
            cpf: "83166844112",
            dateOfBirth: "1993-06-12",
            liveTogether: true
         },
         address: {
            type: "family",
            zipcode: "05351035",
            streetName: "Rua Doutor Hélio Fidélis",
            number: "222",
            complement: "2",
            neighborhood: "Cidade São Francisco",
            uf: "SP",
            city: "São Paulo",
            residenceTime: 2
         }
      },
      {
         name: "Teste parceiro",
         cpf: "83166844112",
         phones: [
            {
               type: "residential",
               number: "22222222222"
            },
            {
               type: "cellphone",
               number: "11111111111"
            }
         ],
         dateOfBirth: "1993-06-12",
         email: "email@email.com",
         maritalStatus: "divorced",
         // maritalStatus: "single",
         // maritalStatus: "married",
         marriageSystem: "separate_property",
         weddingDate: "2020-05-14",
         stableUnion: true,
         stableUnionDate: "2020-05-01",
         // stableUnion: false,
         homeCountry: "Brasil",
         spouse: {
            name: "Teste exemplo",
            cpf: "86205340097",
            dateOfBirth: "2000-01-01",
            liveTogether: true
         },
      }
   ],
   status: "new",
   lastSimulation: {
      duration: 200,
      mortgageValue: 250000,
      type: "mortgage",
      realtyPrice: 400000.51,
      firstPayment: 2615.5
   },
   partnerEtapa: "analise_credito",
   partnerStatus: "active",
   proposalStatus: "pending",
   proposalId: 11185,
   partnerName: "GoBacklog",
   partnerId: 6850,
   proposalTeam: {
      commercialManager: 106,
      operationalManager: 59,
      proposalConsultant: 98,
      proposalCoordinator: 95,
      leadConsultant: 170,
      creditAnalyst: 70
   },
   leadStatus: "won"
}
