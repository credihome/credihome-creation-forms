/**
* @category Variáveis utilizadas em todos formulários
* @const
* @example
* const style = {
  activeBackgroundColorButton: "mediumvioletred",
  hoverBackgroundColorButton: "deeppink",
  backgroundColorLogo: "transparent",
  hasBackground: true,
  hasBackgroundMobile: true,
  hasFavicon: true,
  backgroundColorSection: "rgba(43, 156, 185, 0.6)",
  borderLeftColorSection: "rgba(43, 156, 185, 1)",
  primaryBackgroundColorButton: "pink",
  secondaryBackgroundColorButton: "palevioletred",
  primaryTextColor: "purple",
  styledDetailsLogo: "padding: 8px; border-radius: 8px",
  headerColor: "#fff",
  utils: {
    heightLogoDesktop: "25px",
    heightLogoMobile: "50px",
    pageTitle: "new page title",
    sloganCommercial: "sloganCommercial",
    analise: {
      approvedHeader: "analise Parabens {name} voce foi aprovado.",
      approvedMessage: "analise clique me",
      redirectText: "analise Voltar para a página inicial da credihome",
      cardLayout: true,
      redirectLink: "http://www.credihome.com.br"
    },
    contato: {
      title: "contato Pronto"
    },
    dadosPessoais: {
      description: "dadosPessoais dados pessoais:",
      othersBuyers: "dadosPessoais Segundo comprador.",
      othersProponents: "dadosPessoais Segundo proponentes.",
      primaryBuyer: "dadosPessoais Primeiro comprador.",
      primaryProponent: "dadosPessoais Primeiro Proponente."
    },
    documentos: {
      description: "documentos Documento de identificacao:",
      othersBuyers: "documentos Documentos Segundo comprador",
      othersProponents: "documentos Documentos Segundo proponentes",
      primaryBuyer: "documentos Documentos Primeiro comprador",
      primaryProponent: "documentos Documentos Primeiro Proponente"
    },
    endereco: {
      description: "description Endereço residencial",
      othersBuyers: "othersBuyers Endereco Segundo comprador",
      othersProponents: "othersProponents Endereco Segundo proponentes",
      primaryBuyer: "primaryBuyer Endereco Primeiro comprador",
      primaryProponent: "primaryProponent Endereco Primeiro Proponente"
    },
    imovel: {
      title: "imovel Fale sobre o imóvel"
    },
    profissional: {
      othersBuyers: "profissional Segundo comprador",
      othersProponents: "profissional Segundo proponentes",
      primaryBuyer: "profissional Primeiro comprador",
      primaryProponent: "profissional Primeiro Proponente"
    },
    proponentes: {
      minimumIncomeMessage: "Renda familiar total deve ser maior que R$ {minimumIncome}. Por favor, adicione mais compradores para compor a renda. A renda informada foi de  R$ {totalIncome}!",
      minimumIncomeMessageProponents: "Renda familiar total deve ser maior que R$ {minimumIncome}. Por favor, adicione mais proponentes para compor a renda. A renda informada foi de  R$ {totalIncome}!",
      title: "Composição da renda"
    },
    resultados: {
      continue: "Quero meu crédito",
      disclaimer: "Disclaimer",
      header: "Header de resultado"
    },
    tempo: {
      title: "Quando voce espera realizar a compra do seu imóvel?"
    }
  },
  validation: {
    error: {
      backgroundColor: "lightcoral",
      color: "crimson"
    },
    info: {
      backgroundColor: "#DAD7D7",
      color: "black"
    },
    warning: {
      backgroundColor: "#EFA500",
      color: "#2B3855"
    }
  }
}
*/

export const style = {
  activeBackgroundColorButton: "mediumvioletred",
  hoverBackgroundColorButton: "deeppink",
  backgroundColorLogo: "transparent",
  hasBackground: true,
  hasBackgroundMobile: true,
  hasFavicon: true,
  backgroundColorSection: "rgba(43, 156, 185, 0.6)",
  borderLeftColorSection: "rgba(43, 156, 185, 1)",
  primaryBackgroundColorButton: "pink",
  secondaryBackgroundColorButton: "palevioletred",
  primaryTextColor: "purple",
  styledDetailsLogo: "padding: 8px; border-radius: 8px",
  headerColor: "#fff",
  utils: {
    heightLogoDesktop: "25px",
    heightLogoMobile: "50px",
    pageTitle: "new page title",
    sloganCommercial: "sloganCommercial",
    analise: {
      approvedHeader: "analise Parabens {name} voce foi aprovado.",
      approvedMessage: "analise clique me",
      redirectText: "analise Voltar para a página inicial da credihome",
      cardLayout: true,
      redirectLink: "http://www.credihome.com.br"
    },
    contato: {
      title: "contato Pronto"
    },
    dadosPessoais: {
      description: "dadosPessoais dados pessoais:",
      othersBuyers: "dadosPessoais Segundo comprador.",
      othersProponents: "dadosPessoais Segundo proponentes.",
      primaryBuyer: "dadosPessoais Primeiro comprador.",
      primaryProponent: "dadosPessoais Primeiro Proponente."
    },
    documentos: {
      description: "documentos Documento de identificacao:",
      othersBuyers: "documentos Documentos Segundo comprador",
      othersProponents: "documentos Documentos Segundo proponentes",
      primaryBuyer: "documentos Documentos Primeiro comprador",
      primaryProponent: "documentos Documentos Primeiro Proponente"
    },
    endereco: {
      description: "description Endereço residencial",
      othersBuyers: "othersBuyers Endereco Segundo comprador",
      othersProponents: "othersProponents Endereco Segundo proponentes",
      primaryBuyer: "primaryBuyer Endereco Primeiro comprador",
      primaryProponent: "primaryProponent Endereco Primeiro Proponente"
    },
    imovel: {
      title: "imovel Fale sobre o imóvel"
    },
    profissional: {
      othersBuyers: "profissional Segundo comprador",
      othersProponents: "profissional Segundo proponentes",
      primaryBuyer: "profissional Primeiro comprador",
      primaryProponent: "profissional Primeiro Proponente"
    },
    proponentes: {
      minimumIncomeMessage: "Renda familiar total deve ser maior que R$ {minimumIncome}. Por favor, adicione mais compradores para compor a renda. A renda informada foi de  R$ {totalIncome}!",
      minimumIncomeMessageProponents: "Renda familiar total deve ser maior que R$ {minimumIncome}. Por favor, adicione mais proponentes para compor a renda. A renda informada foi de  R$ {totalIncome}!",
      title: "Composição da renda"
    },
    resultados: {
      continue: "Quero meu crédito",
      disclaimer: "Disclaimer",
      header: "Header de resultado"
    },
    tempo: {
      title: "Quando voce espera realizar a compra do seu imóvel?"
    }
  },
  validation: {
    error: {
      backgroundColor: "lightcoral",
      color: "crimson"
    },
    info: {
      backgroundColor: "#DAD7D7",
      color: "black"
    },
    warning: {
      backgroundColor: "#EFA500",
      color: "#2B3855"
    }
  }
}
