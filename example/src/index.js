import React from 'react'
import ReactDOM from 'react-dom'
import {IntlProvider} from 'react-intl'

import App from './App'

ReactDOM.render(
  <IntlProvider
    messages={{
      credihomeForms: {
        contato: {
          name: {
            id: `credihomeForms.contato.title`,
            defaultMessage: 'Teste se o cara tiver intl',
          }
        }
      }
    }}>
    <App />
  </IntlProvider>,
  document.getElementById('root'))
