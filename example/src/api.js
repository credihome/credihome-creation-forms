/* eslint-disable no-console */
import axios from 'axios';
import config from './config';

const instance = axios.create({
  baseURL: config.api.url,
});

instance.interceptors.request.use(
  model => {
    const interceptor = model;

    interceptor.headers.Authorization = `Bearer ${config.api.token}`;
    return interceptor;
  },
  error => Promise.reject(error),
);

const api = {
  getDictionary: () =>
    instance.get(`dictionary`).then(response => response.data),
};

export default api;
export {instance};
