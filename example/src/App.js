import React, { useState, useLayoutEffect, useRef } from 'react'
import styled from 'styled-components'
import {
  FormStructure,
  ValorOperacao,
  Contato,
  Finalidade,
  Tempo,
  DadosPessoais,
  Documento,
  Endereco,
  InfoProfissional,
  Realty
} from 'credihome-creation-forms'
import { Select } from 'credihome-react-library'
import { useIntl } from 'react-intl'
import { style } from './data/style'
import { leadData, leadDataTwoBuyers, spouse } from './data/leadData'
import { instance } from './api'
import SideBar from './components/SideBar';

const Container = styled.div`
  font-family: 'Open Sans', 'Arial', sans-serif;
  display: flex;
  flex-direction: row;
  background-color:rgba(255, 255, 255, 0.5);
`;

const WrapperContent = styled.div`
  width: 100%;
  margin: 0;
  position: absolute;
  background-color: white;
  padding: 48px;
  -webkit-box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0);
  box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0);
  @media (min-width: 768px) {
    left: calc(180px + 18%);
    width: 35%;
    margin: 0 auto;
  }
`;

const options = [
  { label: 'Banana', key: 'banana' },
  { label: 'Caqui', key: 'caqui' },
  { label: 'Kiwi', key: 'Kiwi' },
  { label: 'Banana com manga', key: 'banana-manga' },
  { label: 'Uva', key: 'uva' },
  { label: 'Super salada de frutas vermelhas', key: 'frutas-vermelhas' },
  { label: 'Suped duper salada de frutas vermelhas com leite condensado', key: 'frutas-vermelhas-leite' },
];


const allForms = ['CustomForm', 'ValorOperacao', 'Contato', 'Finalidade', 'Tempo', 'DadosPessoais', 'Documento',  'Endereco', 'InfoProfissional', 'Realty'];
const editAllForms = ['CustomForm', 'ValorOperacao', 'Contato', 'Finalidade', 'Tempo', 'DadosPessoais', 'Documento',  'Endereco', 'InfoProfissional', 'Realty'];

export default function App() {
  const [checked, setChecked] = useState(false)
  const [errors, setErrors] = useState(false)
  const [submitting, setSubmitting] = useState(false)
  const intl = useIntl()
  const firstUpdate = useRef(true);

  const [toRenderForm, setToRenderForm] = useState(allForms);

  useLayoutEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    alert(errors)
  }, [errors])

  function handleChangeInput(v) {
    if (v === '[]') {
      return setToRenderForm([])
    }
    else if (v.length === allForms.length) {
      return setToRenderForm(allForms);
    }
    else if (toRenderForm.includes(v)) {
      const index = allForms.indexOf(v);
      const newValue = editAllForms.splice(index, 1);
      return setToRenderForm(newValue);
    } else {
      setToRenderForm([...toRenderForm, v]);
    }
  }

  return (
    <Container>
      <link rel="stylesheet" type="text/css" href="https://cdn.credihome.com.br/static/icons/chicons.css" />
      <SideBar listItems={allForms} handleChangeInput={handleChangeInput} checkedItems={toRenderForm} />
      <WrapperContent>
        {toRenderForm.includes('CustomForm') && (
          <FormStructure
            initialValues={{ hello: '' }}
            onSubmit={() => alert('Hello World!')}
            textSubmit="Hello"
            onBack={() => alert('Good bye World!')}
            textBack="Bye"
            textSubtitle="Hello Form"
            style={style}
          >
            Hello World!
            <input />
            <Select
              handleChange={(v) => { console.log(v) }}
              value=''
              options={options}
              id="simpleSelect"
              maxHeight="150px;"
              noOptionString={"Welp, there's nothing here!"}
              readOnly
            />
          </FormStructure>)}
        {toRenderForm.includes('ValorOperacao') &&
          <ValorOperacao
            onSubmit={(values) => alert(JSON.stringify(values))}
            style={style}
            initialValues={{ type: 'mortgage' }}
            // messages={{ submit: 'wow' }}
            sendAPI={true}
            leadData={leadData}
            axios={instance}
          />
        }
        {toRenderForm.includes('Contato') &&
          <Contato
            onSubmit={(values) => alert(JSON.stringify(values))}
            onBack={() => { console.log("Back") }}
            style={style}
            disabledSubmit={!checked}
            intl={intl}
            sendAPI={true}
            leadData={leadData}
            axios={instance}
            setError={setErrors}
            setSubmitting={setSubmitting}
          >
            <div>
              <input id="teste" type="checkbox" checked={checked} onChange={() => setChecked(!checked)} />
              <label htmlFor="teste">Teste</label>
              <p>Submitting: {submitting ? "true" : "false"}</p>
            </div>
          </Contato>
        }
        {toRenderForm.includes('Finalidade') &&
          <Finalidade
            onSubmit={(values, sendData) => { console.log(sendData); alert(JSON.stringify(sendData), null, '\t') }}
            onBack={() => { console.log("Back") }}
            style={style}
            prepareData
            leadData={leadData}
            setError={setErrors}
          />
        }
        {toRenderForm.includes('Tempo') &&
          <Tempo
            onSubmit={(values) => alert(JSON.stringify(values))}
            onBack={() => { console.log("Back") }}
            style={style}
          />
        }
        {toRenderForm.includes('Documento') &&
          <Documento
            onSubmit={(values) => alert(JSON.stringify(values))}
            onBack={() => { console.log("Back") }}
            intl={intl}
            style={style}
            leadData={leadDataTwoBuyers}
            index={'1'}
            axios={instance}
            sendAPI
          />
        }
        {toRenderForm.includes('DadosPessoais') &&
          <DadosPessoais
            onSubmit={(values, sendData) => { console.log(values); console.log(sendData); alert(JSON.stringify(sendData), null, '\t') }}
            onBack={() => { console.log("Back") }}
            style={style}
            leadData={leadData}
            sendAPI={true}
            prepareData
            index='0'
            axios={instance}
          />
        }
        {toRenderForm.includes('Endereco') &&
          <Endereco
            onSubmit={(values, sendData) => { console.log(values); console.log(sendData); alert(JSON.stringify(sendData), null, '\t') }}
            onBack={() => { console.log("Back") }}
            style={style}
            leadData={spouse}
            sendAPI={true}
            prepareData
            index='0'
            axios={instance}
          />
        }
        {toRenderForm.includes('InfoProfissional') &&
          <InfoProfissional
            onSubmit={(values, sendData) => { console.log(values); console.log(sendData); alert(JSON.stringify(sendData), null, '\t') }}
            onBack={() => { console.log("Back") }}
            intl={intl}
            style={style}
            leadData={leadDataTwoBuyers}
            index={'1'}
            axios={instance}
            sendAPI
          />
        }
        {toRenderForm.includes('Realty') &&
          <Realty
            onSubmit={(values, sendData) => { console.log(values); console.log(sendData); alert(JSON.stringify(sendData), null, '\t') }}
            onBack={() => { console.log("Back") }}
            style={style}
            leadData={leadDataTwoBuyers}
            sendAPI={true}
            prepareData
            axios={instance}
            product='carencia'
            unmask
          />
        }
      </WrapperContent>
    </Container>
  )
}
