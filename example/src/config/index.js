// import {partnerId} from "../pages/whiteLabel/assets/custom/data.json"

export default {
  api: {
    url: 'https://api-site.credihome.com.br/staging',
    // url: 'http://localhost:8080',
    token:
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjF9.7pZJ5DH00bBVM5i-p3bew1WzMrb5V1T-ZAMluQBs02k',
  },
  apiApp: {
    url: 'https://api-app.credihome.com.br/staging',
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjQiLCJ1c2VybmFtZSI6IjI5NTg0Mzg4NDg3IiwiZXhwIjoxNTkzNTQ5ODc1LCJqdGkiOiI0OlNrakV1NnoyWiIsImlhdCI6MTUwNzE0OTg3NX0.CP4YhJQwFF0WYOIH3IWP9h9fo0U5oegmm-kZ0v6V9xs',
  },
  cdn: {
    url: 'https://cdn.credihome.com.br/dev',
  },
  partnerId: 1,
};
