import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function SideBar(props) {
  const { listItems, handleChangeInput, checkedItems } = props;
  return (
    <SideBartStyled>
      <span>Escolha entre</span>
      <StyledButtonAll onClick={() => checkedItems.length === listItems.length ? handleChangeInput('[]') : handleChangeInput(listItems)}>Exibir todos</StyledButtonAll>
      <span>Ou</span>
      {listItems.map((item) => (
        <div>
          <input type="checkbox" id={item} name={item} onChange={() => handleChangeInput(item)}
            checked={checkedItems.includes(item)}>
          </input>
          <label for={item}>{item}</label>
        </div>
      ))}

      <i>
        Já vi o pequeno bug, mas não deu p dar atenção
      </i>
    </SideBartStyled>
  );
}

const SideBartStyled = styled.div`
  position: fixed;
  height: 100vh;
  width: 180px;
  background-color: white;
  -webkit-box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0); 
  box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0);
  display: flex;
  flex-direction: column;
  padding: 12px;
  & > span {
    margin: 8px;
    text-align: center;
    font-size: 14px;
    color: #afafaf;
  }
  & > i {
    color: #afafaf;
    margin: 12px;
    text-align: center;
  }
  & > div {
    margin: 2px 0;
    color: #db7093;
    font-size: 18px;
    & > input {
      cursor: pointer;
    }
  }
  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

const StyledButtonAll = styled.button`
  background-color: white;
  border: 2px solid #db7093;
  height: 40px;
  color: #db7093;
  border-radius: 8px;
  padding: 4px 26px;
  font-size: medium;
  font-weight: bold;
  text-decoration: underline;
  cursor: pointer;
`;

SideBar.propTypes = {
  listItems: PropTypes.array.isRequired,
  handleChangeInput: PropTypes.func.isRequired, 
  checkedItems: PropTypes.func,
};

SideBar.defaultProps = {
  checkedItems: [],
}

export default SideBar;
