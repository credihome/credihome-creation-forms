import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.tempo';

const intlMessages = defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Quando você espera realizar a compra do seu imóvel?',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.tempo.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Tempo
* @alias Mensagens de Tempo
* @const
* @type {Object}
* @example
* const const messages = {
  title: 'Quando você espera realizar a compra do seu imóvel?',
  submit: 'Continuar',
  back: 'Voltar',
};
*/
const strMessages = {
  title: 'Quando você espera realizar a compra do seu imóvel?',
  submit: 'Continuar',
  back: 'Voltar',
};

export { scope, intlMessages, strMessages };
