/* eslint-disable import/no-named-default, no-console */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { WLTheme } from 'credihome-react-library';
import { RadioBlocks } from '../../components/Radio';
import FormStructure from '../FormStructure';
import api from '../../api';
import utils from '../../utils/utils';
import { default as defaultValidation } from './validationSchema';
import { intlMessages, strMessages } from './defaultMessages';
import intlVerification from '../../utils/intlVerification';

/**
 * Formulário de Tempo do WL.
 *
 * @category Formulários - Simulação
 * @component
 */

function Tempo(props) {
  const {
    children,
    initialValues,
    validationSchema,
    onSubmit,
    onBack,
    disabledSubmit,
    style,
    messages,
    validationMessages,
    intl,
    sendAPI,
    prepareData,
    leadData,
    axios,
    setError,
    setSubmitting,
  } = props;

  const [listPurchase, setListPurchase] = useState([]);

  useEffect(() => {
    async function getDictionary() {
      await api.getDictionary()
        .then((res) => setListPurchase(res.purchaseTime))
        .catch((err) => console.log(err));
    }
    getDictionary();
  }, []);

  const purchaseOptions = utils.objectToArray(listPurchase)
    .map((item) => ({
      key: item.id,
      label: item.value,
      order: item.order,
    }))
    .sort((a, b) => a.order - b.order);

  if (listPurchase.length === 0) {
    return 'Carregando...';
  }

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={({
          purchaseTime: undefined,
          ...initialValues,
        })}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages) : defaultValidation(intl, validationMessages)}
        onSubmit={onSubmit}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'tempo', 'submit', intlMessages, messages, style, strMessages)}
        textBack={intlVerification(intl, 'tempo', 'back', intlMessages, messages, style, strMessages)}
        textSubtitle={intlVerification(intl, 'tempo', 'title', intlMessages, messages, style, strMessages)}
        disabledSubmit={disabledSubmit}
        style={style}
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        leadDataManipulation={{
          purchaseTime: { path: ['lastSimulation.purchaseTime'] },
        }}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
      >
        <Field>
          {({ form }) => (
            <RadioBlocks
              options={purchaseOptions}
              id="purchaseTime"
              name="purchaseTime"
              errors={form.errors}
              touched={form.touched}
              value={form.values.purchaseTime}
              onChangeItem={(key) => form.setFieldValue('purchaseTime', key)}
              partnerColors={style}
            />
          )}
        </Field>
        {children}
      </FormStructure>
    </WLTheme>
  );
}

Tempo.propTypes = {
  /**
 * Continuação do formulário, caso o usuário queira adicionar algum HTML entre o último input
 * e os botões.<br>
 * Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
 */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /**
 * Valores iniciais do formulário se ele não começa vazio,
 * se entrar valores diferentes dos alterados no formulário eles vão ser retornados no submit também.
 */
  initialValues: PropTypes.object.isRequired,
  /**
 * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
 */
  validationSchema: PropTypes.func,
  /**
 * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
 * <ul>
 * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
 * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
 * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
 * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
 * <ul>
 */
  onSubmit: PropTypes.func.isRequired,
  /**
 * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
 */
  onBack: PropTypes.func,
  /**
 * Flag que se true dá ao botão submit o atributo disabled.
 */
  disabledSubmit: PropTypes.bool,
  /**
 * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
 */
  style: PropTypes.object.isRequired,
  /**
 * Objeto contendo as mensagens a serem substituídas.<br>
 * <i>Verificar o 'Mensagens de Tempo' para ver quais as mensagens que podem ser substituídas.</i>
 */
  messages: PropTypes.object,
  /**
 * Objeto contendo as mensagens de erro a serem substituídas.</br>
 * <i>Verificar o 'Mensagens de Validação' para ver quais as mensagens que podem ser substituídas.</i>
 */
  validationMessages: PropTypes.object,
  /**
 * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.tempo.{campo}'.
 * <i>Verificar o 'Mensagens de Tempo' para ver quais as mensagens que podem ser substituídas.</i>
 */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
 * Flag que quando true, após a confirmação do usuário, envia os dados do lead
 * atualizados para a API da credihome.
 * Se estiver true, leadData e axios são obrigatórios.
 */
  sendAPI: PropTypes.bool,
  /**
 * Dados do lead a serem atualizados, deve seguir o padrão da credihome se sendAPI for true.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true<./i>
 */
  leadData: PropTypes.object,
  /**
 * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  axios: PropTypes.func,
  /**
 * Função que recebe um erro caso a chamada da API retorne erro.
 */
  setError: PropTypes.func,
  /**
 * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
 * de rodar tudo que deveria após a submissão.
 */
  setSubmitting: PropTypes.func,
};

Tempo.defaultProps = {
  children: null,
  validationSchema: null,
  onBack: null,
  disabledSubmit: false,
  messages: null,
  validationMessages: null,
  intl: null,
  sendAPI: null,
  prepareData: null,
  leadData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
};

export default Tempo;
