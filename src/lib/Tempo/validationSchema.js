import * as Yup from 'yup';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from '../../utils/validationMessages';

const validationSchema = (intl, validationMessages) => {
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);

  return Yup.object().shape({
    purchaseTime: Yup.string().required(msgRequiredField),
  });
};
export default validationSchema;
