import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { WLTheme, Select, DatePicker } from 'credihome-react-library';
import FormStructure from '../FormStructure';
import { InputField } from '../../components/Input/InputField';
import { default as defaultValidation } from './validationSchema';
import objectToArray from '../../utils/objectToArray';
import api from '../../api';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from './defaultMessages';
import {
  phoneMask, dateMask, phoneUnmask, numberUnmask, cnpjMask,
} from '../../utils/masks';

/**
 * Formulário de Informação Profissional.
 *
 * @category Formulários - Dados do Proponente
 * @component
 */

function InfoProfissional(props) {
  const {
    /* initialValues, */
    style,
    validationSchema,
    intl,
    onBack,
    messages,
    validationMessages,
    children,
    index,
    product,
    disabledSubmit,
    leadData,
    onSubmit,
    sendAPI,
    prepareData,
    axios,
    setError,
    setSubmitting,
    unmask,
  } = props;

  const { buyers } = leadData;
  const [dictionary, setDictionary] = useState(undefined);

  const initialValues = {
    contractType: buyers[index]?.contractType,
    jobTitle: buyers[index]?.jobTitle,
    jobType: buyers[index]?.jobType,
    companyName: buyers[index]?.company?.name,
    phone:
      buyers[index]?.phones.filter((phone) => phone.type === 'business')[0]
        ?.number || '',
    cnpjCompany: buyers[index]?.company?.cnpj,
    dateOfEmployment: (buyers[index]?.dateOfEmployment || '')
      .split('/')
      .reverse()
      .join('-'),
    monthlyIncome: buyers[index]?.monthlyIncome,
  };

  useEffect(() => {
    async function getDictionary() {
      await api.getDictionary()
        .then((res) => setDictionary(res))
        .catch((err) => console.log(err));
    }
    getDictionary();
  }, []);

  if (!dictionary) return 'Carregando...';

  const contractTypesItems = objectToArray(dictionary.contractType)
    .sort((a, b) => b.score - a.score)
    .map((item) => ({ key: item.id, label: item.name }));

  if (!leadData.buyers[index].phones) {
    leadData.buyers[index].phones = [];
  }

  let businessIndex;
  businessIndex = leadData.buyers[0].phones.findIndex((p) => p.type === 'business');
  if (businessIndex === -1) {
    businessIndex = leadData.buyers[0].phones.length;
    leadData.buyers[0].phones.push({ type: 'cellphone', number: '' });
  }

  const jobTypeItems = objectToArray(dictionary.jobType)
    .map((item) => ({
      key: item.id,
      label: item.name,
    }))
    .sort((a, b) => {
      const textA = a.label.toUpperCase();
      const textB = b.label.toUpperCase();
      if (textA < textB) {
        return -1;
      }
      if (textA > textB) {
        return 1;
      }
      return 0;
    });
  const jobTitleItems = objectToArray(dictionary.jobTitle)
    .map((item) => ({
      key: item.id,
      label: item.name,
    }))
    .sort((a, b) => {
      const textA = a.label.toUpperCase();
      const textB = b.label.toUpperCase();
      if (textA < textB) {
        return -1;
      }
      if (textA > textB) {
        return 1;
      }
      return 0;
    });

  const getLabelDateOfEmployment = (contractType) => {
    if (
      contractType === 'retired_public_agent'
      || contractType === 'pensioner'
      || contractType === 'retired'
    ) {
      return intlVerification(intl, 'infoProfissional', 'labelRetiredDate', intlMessages, messages, style, strMessages);
    }
    if (contractType === 'owner') {
      return intlVerification(intl, 'infoProfissional', 'labelCompanyDate', intlMessages, messages, style, strMessages);
    }
    return intlVerification(intl, 'infoProfissional', 'labelOccupationDate', intlMessages, messages, style, strMessages);
  };

  const getMessage = (buyerIndex) => {
    if (product === 'mortgage' || leadData.lastSimulation.type === 'mortgage') {
      if (buyerIndex === 0) {
        return intlVerification(intl, 'infoProfissional', 'primaryBuyer', intlMessages, messages, style, strMessages);
      }
      return intlVerification(intl, 'infoProfissional', 'othersBuyers', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
    }
    if (buyerIndex === 0) {
      return intlVerification(intl, 'infoProfissional', 'primaryProponent', intlMessages, messages, style, strMessages);
    }
    return intlVerification(intl, 'infoProfissional', 'othersProponents', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
  };

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={initialValues}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages, buyers, index) : defaultValidation(intl, validationMessages, buyers, index)}
        onSubmit={(values, sendData, res) => (unmask ? onSubmit({
          ...values, cnpj: phoneUnmask(values.cnpj), phone: phoneUnmask(values.phone), monthlyIncome: numberUnmask(values.monthlyIncome),
        }, sendData, res) : onSubmit(values, sendData, res))}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'infoProfissional', 'submit', intlMessages, messages, style, strMessages)}
        textBack={intlVerification(intl, 'infoProfissional', 'back', intlMessages, messages, style, strMessages)}
        textSubtitle={getMessage(index)}
        disabledSubmit={disabledSubmit}
        style={style}
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        leadDataManipulation={{
          contractType: { path: [`buyers.${index}.contractType`] },
          jobTitle: { path: [`buyers.${index}.jobTitle`] },
          jobType: { path: [`buyers.${index}.jobType`] },
          companyName: { path: [`buyers.${index}.company.name`] },
          phone: { path: [`buyers.${index}.phones.${businessIndex}.number`], unmask: (value) => phoneUnmask(value) },
          cnpjCompany: { path: [`buyers.${index}.company.cnpj`], unmask: (value) => phoneUnmask(value) },
          dateOfEmployment: { path: [`buyers.${index}.dateOfEmployment`] },
          monthlyIncome: { path: [`buyers.${index}.monthlyIncome`], unmask: numberUnmask },
        }}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
      >

        <Field>
          {({ form }) => (
            <Select
              id="contractType"
              errors={form.errors}
              touched={form.touched}
              options={contractTypesItems}
              handleBlur={form.handleBlur}
              handleFocus={form.handleFocus}
              handleChange={(v) => {
                form.setFieldValue('contractType', v);
              }}
              value={form.values.contractType}
              maxHeight="50vh"
              readOnly
              placeholder={intlVerification(intl, 'infoProfissional', 'labelOcupation', intlMessages, messages, style, strMessages)}
              noUp
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <Select
              id="jobType"
              options={jobTypeItems}
              value={form.values.jobType}
              handleBlur={form.handleBlur}
              handleFocus={form.handleFocus}
              handleChange={(v) => {
                form.setFieldValue('jobType', v);
              }}
              errors={form.errors}
              touched={form.touched}
              maxHeight="50vh"
              placeholder={intlVerification(intl, 'infoProfissional', 'labelJobType', intlMessages, messages, style, strMessages)}
              noUp
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <Select
              id="jobTitle"
              options={jobTitleItems}
              value={form.values.jobTitle}
              handleBlur={form.handleBlur}
              handleFocus={form.handleFocus}
              handleChange={(v) => {
                form.setFieldValue('jobTitle', v);
              }}
              errors={form.errors}
              touched={form.touched}
              maxHeight="50vh"
              placeholder={intlVerification(intl, 'infoProfissional', 'labelJobTitle', intlMessages, messages, style, strMessages)}
              noUp
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <InputField
              id="companyName"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.companyName}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'infoProfissional', 'labelCompanyName', intlMessages, messages, style, strMessages)}
              partnerColors={style}
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <InputField
              id="phone"
              type="text"
              mask={phoneMask}
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.phone}
              errors={form.errors}
              touched={form.touched}
              partnerColors={style}
              placeHolder={intlVerification(intl, 'infoProfissional', 'labelPhone', intlMessages, messages, style, strMessages)}
            />
          )}
        </Field>
        <Field>
          {({ form }) => (
            form.values.contractType === 'owner' && (
            <InputField
              id="cnpjCompany"
              type="text"
              mask={cnpjMask}
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.cnpjCompany}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'infoProfissional', 'labelCnpjCompany', intlMessages, messages, style, strMessages)}
            />
            )
          )}
        </Field>
        <Field>
          {({ form }) => (
            <DatePicker
              id="dateOfEmployment"
              type="text"
              mask={dateMask}
              handleChange={(v) => form.setFieldValue('dateOfEmployment', v)}
              handleBlur={() => form.setFieldTouched('dateOfEmployment', true)}
              value={form.values.dateOfEmployment}
              errors={form.errors}
              touched={form.touched}
              displayFormat="DD/MM/YYYY"
              valueFormat="YYYY-MM-DD"
              placeholder={getLabelDateOfEmployment(form.values.contractType)}
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <InputField
              id="monthlyIncome"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.monthlyIncome}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'infoProfissional', 'labelMonthlyIncome', intlMessages, messages, style, strMessages)}
              partnerColors={style}
              footer={intlVerification(intl, 'infoProfissional', 'labelMonthlyIncomeDescription', intlMessages, messages, style, strMessages)}
              maskmoney
            />
          )}
        </Field>

        {children}
      </FormStructure>
    </WLTheme>
  );
}

InfoProfissional.propTypes = {
  /**
* Continuação do formulário, caso o usuário queira adicionar algum HTML entre o último input
* e os botões.<br>
* Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
*/
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /**
  * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
  */
  validationSchema: PropTypes.func,
  /**
  * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
  * <ul>
  * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
  * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
  * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
  * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
  * <ul>
  */
  onSubmit: PropTypes.func.isRequired,
  /**
  * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
  */
  onBack: PropTypes.func,
  /**
  * Flag que se true dá ao botão submit o atributo disabled.
  */
  disabledSubmit: PropTypes.bool,
  /**
  * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
  */
  style: PropTypes.object.isRequired,
  /**
  * Objeto contendo as mensagens a serem substituídas.&lt;br>
  * Verificar Documento/defaultMessages.js para ver quais as mensagens que podem ser substituídas
  */
  messages: PropTypes.object,
  /**
  * Objeto contendo as mensagens de erro a serem substituídas.&lt;/br>
  * Verificar utils/validationMessages.js para ver quais as mensagens que podem ser substituídas
  */
  validationMessages: PropTypes.object,
  /**
  * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.finalidade.{campo}'
  */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
  * Flag que quando true, após a confirmação do usuário, envia os dados do lead
  * atualizados para a API da credihome.
  * Se estiver true, leadData e axios são obrigatórios.
  */
  sendAPI: PropTypes.bool,
  /**
  * Dados do lead a serem atualizados, deve seguir o padrão da credihome se sendAPI for true.<br>
  * <i>Obrigatório quando sendAPI ou prepareData são true<./i>
  */
  leadData: PropTypes.object.isRequired,
  /**
  * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
  * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
  */
  axios: PropTypes.func,
  /**
  * Função que recebe um erro caso a chamada da API retorne erro.
  */
  setError: PropTypes.func,
  /**
  * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
  * de rodar tudo que deveria após a submissão.
  */
  setSubmitting: PropTypes.func,
  /**
  * Produto escolhido pelo usuário, se for null ele será igual a leadData.lastSimulation.type
  */
  product: PropTypes.string,
  /**
  * Flag que quando true passa os dados de onSubmit sem máscara.
  */
  unmask: PropTypes.bool,
  /**
  * Posição do proponente que está sendo alterado.
  */
  index: PropTypes.string.isRequired,
};

InfoProfissional.defaultProps = {
  children: null,
  // initialValues: {},
  validationSchema: null,
  onBack: null,
  disabledSubmit: false,
  messages: null,
  validationMessages: null,
  intl: null,
  sendAPI: null,
  prepareData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
  unmask: null,
  product: null,
};

export default InfoProfissional;
