/* eslint-disable prettier/prettier */
import * as Yup from 'yup';
import utils from '../../utils/utils';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from '../../utils/validationMessages';


const validationSchema = (intl, validationMessages, buyers, index) => {
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages)
  const msgInvalidPhone = intlVerification(intl, 'validation', 'ValidationInvalidPhone', intlMessages, validationMessages, {}, strMessages)
  const msgInvalidCNPJ = intlVerification(intl, 'validation', 'ValidationInvalidCNPJ', intlMessages, validationMessages, {}, strMessages)

  return Yup.object().shape({
    contractType: Yup.string().required(msgRequiredField),
    jobTitle: Yup.string().when(['contractType'], (contractType, schema) =>
      contractType === 'owner' || contractType === 'employee'
        ? schema.required(msgRequiredField)
        : schema,
    ),
    jobType: Yup.string().when(['contractType'], (contractType, schema) =>
      contractType === 'owner' || contractType === 'employee'
        ? schema.required(msgRequiredField)
        : schema,
    ),
    companyName: Yup.string().when(['contractType'], (contractType, schema) =>
      contractType === 'owner' || contractType === 'employee'
        ? schema.required(msgRequiredField)
        : schema,
    ),
    phone: utils.customYupSchemas
      .masked()
      .required(msgRequiredField)
      .min(10, msgInvalidPhone),
    cnpjCompany: Yup.string().when(['contractType'], (contractType, schema) =>
      contractType !== 'owner'
        ? schema
        : utils.customYupSchemas
          .masked()
          .required(msgRequiredField)
          .test('valid_cnpj', msgInvalidCNPJ, utils.isCNPJValid),
    ),
    dateOfEmployment: Yup.string().required(msgRequiredField),
    monthlyIncome: utils.customYupSchemas
      .moneyMasked()
      .test(
        'check_valid_monthlyIncome',
        'A renda não pode ser 0',
        value => Number(value).toFixed(0) !== '0',
      )
      .required(msgRequiredField),
  });
};

export default validationSchema;
