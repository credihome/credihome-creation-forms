import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.infoProfissional';

const intlMessages = defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Qual a finalidade do empréstimo?',
  },
  primaryBuyer: {
    id: `${scope}.primaryBuyer`,
    defaultMessage: 'Fale da sua renda',
  },
  primaryProponent: {
    id: `${scope}.primaryProponent`,
    defaultMessage: 'Fale da sua renda',
  },
  othersBuyers: {
    id: `${scope}.othersBuyers`,
    defaultMessage: 'Informe a renda do {buyer}º comprador',
  },
  othersProponents: {
    id: `${scope}.othersProponents`,
    defaultMessage: 'Informe a renda do {buyer}º proponente',
  },
  info: {
    id: `${scope}.info`,
    defaultMessage: 'Informações profissionais',
  },
  labelOcupation: {
    id: `${scope}.labelOcupation`,
    defaultMessage: 'Tipo de renda',
  },
  labelJobType: {
    id: `${scope}.labelJobType`,
    defaultMessage: 'Cargo',
  },
  labelJobTitle: {
    id: `${scope}.labelJobTitle`,
    defaultMessage: 'Profissão',
  },
  labelCompanyName: {
    id: `${scope}.labelCompanyName`,
    defaultMessage: 'Nome da empresa que trabalha',
  },
  labelPhone: {
    id: `${scope}.labelPhone`,
    defaultMessage: 'Telefone comercial',
  },
  labelCnpjCompany: {
    id: `${scope}.labelCnpjCompany`,
    defaultMessage: 'CNPJ (somente se você for proprietário da empresa)',
  },
  labelMonthlyIncome: {
    id: `${scope}.labelMonthlyIncome`,
    defaultMessage: 'Renda total',
  },
  labelMonthlyIncomeDescription: {
    id: `${scope}.labelMonthlyIncomeDescription`,
    defaultMessage: 'Salário + Outras fontes',
  },
  labelOccupationDate: {
    id: `${scope}.labelOccupationDate`,
    defaultMessage: 'Data de admissão',
  },
  labelRetiredDate: {
    id: `${scope}.labelRetiredDate`,
    defaultMessage: 'Data de aposentadoria',
  },
  labelCompanyDate: {
    id: `${scope}.labelCompanyDate`,
    defaultMessage: 'Data de constituição',
  },
  ValidationInvalidPhone: {
    id: `${scope}.ValidationInvalidPhone`,
    defaultMessage: 'Telefone inválido',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.finalidade.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Finalidade
* @alias Mensagens de Finalidade
* @const
* @type {Object}
* @example
* const messages = {
  title: 'Qual a finalidade do empréstimo?', 
  primaryBuyer: 'Fale da sua renda',
  primaryProponent: 'Fale da sua renda',
  othersBuyers: (val) => `Informe a renda do ${val.buyer}º comprador`,
  othersProponents: (val) => `Informe a renda do ${val.buyer}º proponente`, 
  info: 'Informações profissionais', 
  labelOcupation: 'Tipo de renda', 
  labelJobType: 'Cargo', 
  labelJobTitle: 'Profissão', 
  labelCompanyName: 'Nome da empresa que trabalha', 
  labelPhone: 'Telefone comercial', 
  labelCnpjCompany: 'CNPJ (somente se você for proprietário da empresa)', 
  labelMonthlyIncome: 'Renda total', 
  labelMonthlyIncomeDescription: 'Salário + Outras fontes', 
  labelOccupationDate: 'Data de admissão', 
  labelRetiredDate: 'Data de aposentadoria', 
  labelCompanyDate: 'Data de constituição', 
  ValidationInvalidPhone: 'Telefone inválido', 
  submit: 'Continuar', 
  back: 'Voltar',
};
*/
const strMessages = {
  title: 'Qual a finalidade do empréstimo?',
  primaryBuyer: 'Fale da sua renda',
  primaryProponent: 'Fale da sua renda',
  othersBuyers: (val) => `Informe a renda do ${val.buyer}º comprador`,
  othersProponents: (val) => `Informe a renda do ${val.buyer}º proponente`,
  info: 'Informações profissionais',
  labelOcupation: 'Tipo de renda',
  labelJobType: 'Cargo',
  labelJobTitle: 'Profissão',
  labelCompanyName: 'Nome da empresa que trabalha',
  labelPhone: 'Telefone comercial',
  labelCnpjCompany: 'CNPJ (somente se você for proprietário da empresa)',
  labelMonthlyIncome: 'Renda total',
  labelMonthlyIncomeDescription: 'Salário + Outras fontes',
  labelOccupationDate: 'Data de admissão',
  labelRetiredDate: 'Data de aposentadoria',
  labelCompanyDate: 'Data de constituição',
  ValidationInvalidPhone: 'Telefone inválido',
  submit: 'Continuar',
  back: 'Voltar',
};

export { scope, intlMessages, strMessages };
