/* eslint-disable import/no-named-default, import/no-unresolved */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { WLTheme, Select } from 'credihome-react-library';
import api from '../../api';
import FormStructure from '../FormStructure';
import { InputField } from '../../components/Input/InputField';
import { default as defaultValidation } from './validationSchema';
import { intlMessages, strMessages } from './defaultMessages';
import intlVerification from '../../utils/intlVerification';
import objectToArray from '../../utils/objectToArray';
import utils from '../../utils/utils';
import { Address } from '../Address';

/**
 * Formulário de Endereço.
 *
 * @category Formulários - Dados do Proponente
 * @component
 */

function Endereco(props) {
  const {
    children,
    // initialValues,
    validationSchema,
    onSubmit,
    onBack,
    disabledSubmit,
    style,
    messages,
    validationMessages,
    intl,
    sendAPI,
    prepareData,
    leadData,
    axios,
    setError,
    setSubmitting,
    unmask,
    index,
    product,
  } = props;

  // const [isMounted, setMounted] = useState(false);
  const [adressTypeItems, setAdressTypeItems] = useState([]);
  const [dictionary, setDictionary] = useState([]);

  useEffect(() => {
    async function getDictionary() {
      await api.getDictionary()
        .then((res) => {
          setAdressTypeItems(objectToArray(res.addressType).map((item) => ({
            key: item.id,
            label: item.name,
          })));
          setDictionary(res);
        })
        .catch((err) => console.log(err));
    }
    getDictionary();
  }, []);

  // const edit = match.params[0];
  const spouseIndex = leadData.buyers.findIndex(
    (buyer) => utils.unmask(buyer.spouse?.cpf) === utils.unmask(leadData.buyers[index].cpf),
  );

  const getDataFromSpouse = spouseIndex !== -1
  && leadData.buyers[spouseIndex]?.spouse?.liveTogether
  && spouseIndex < index;

  const saveDataToSpouse = spouseIndex !== -1
  && leadData.buyers[spouseIndex]?.spouse?.liveTogether
  && spouseIndex > index;

  const yearsAndMonths = [
    { key: 'years', label: 'anos' },
    { key: 'months', label: 'meses' },
  ];
  let inYears = false;
  if (
    !leadData.buyers[index]?.address?.residenceTime
  || leadData.buyers[index]?.address?.residenceTime % 12 === 0
  ) inYears = true;

  const initialValues = getDataFromSpouse
    ? {
      ...leadData.buyers[spouseIndex].address,
      type: leadData.buyers[index]?.address?.type,
      residenceTime: leadData.buyers[spouseIndex].address.residenceTime % 12 === 0
        ? leadData.buyers[spouseIndex].address.residenceTime / 12
        : leadData.buyers[spouseIndex].address.residenceTime,
      timeUnit:
        leadData.buyers[spouseIndex].address.residenceTime % 12 === 0
          ? 'years'
          : 'months',
    }
    : {
      type: leadData.buyers[index]?.address?.type,
      zipcode: leadData.buyers[index]?.address?.zipcode,
      streetName: leadData.buyers[index]?.address?.streetName,
      number: leadData.buyers[index]?.address?.number,
      complement: leadData.buyers[index]?.address?.complement,
      neighborhood: leadData.buyers[index]?.address?.neighborhood,
      uf: leadData.buyers[index]?.address?.uf,
      city: leadData.buyers[index]?.address?.city,
      residenceTime: (inYears ? leadData.buyers[index]?.address?.residenceTime / 12 : leadData.buyers[index]?.address?.residenceTime) || undefined,
      timeUnit: inYears ? 'years' : 'months',
    };

  // if (!dictionary) return null;

  const getMessage = (buyerIndex) => {
    if (product === 'home_equity' || leadData.lastSimulation.type === 'home_equity') {
      if (buyerIndex === '0') {
        return intlVerification(intl, 'endereco', 'primaryProponent', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
      }
      return intlVerification(intl, 'endereco', 'othersProponents', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
    }
    if (buyerIndex === '0') {
      return intlVerification(intl, 'endereco', 'primaryBuyer', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
    }
    return intlVerification(intl, 'endereco', 'othersBuyers', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
  };

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={initialValues}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages) : defaultValidation(intl, validationMessages)}
        onSubmit={(values, sendData, res) => (unmask ? onSubmit({ ...values, residenceTime: values.timeUnit === 'years' ? values.residenceTime * 12 : values.residenceTime }, sendData, res) : onSubmit(values, sendData, res))}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'endereco', 'submit', intlMessages, messages, style, strMessages)}
        textBack={intlVerification(intl, 'endereco', 'back', intlMessages, messages, style, strMessages)}
        textSubtitle={getMessage(index)}
        disabledSubmit={disabledSubmit}
        style={style}
        alignSubtitle="center"
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        leadDataManipulation={{
          type: { path: [`buyers.${index}.address.type`] },
          zipcode: { path: [`buyers.${index}.address.zipcode`, saveDataToSpouse && `buyers.${spouseIndex}.address.zipcode`] },
          streetName: { path: [`buyers.${index}.address.streetName`, saveDataToSpouse && `buyers.${spouseIndex}.address.streetName`] },
          number: { path: [`buyers.${index}.address.number`, saveDataToSpouse && `buyers.${spouseIndex}.address.number`] },
          complement: { path: [`buyers.${index}.address.complement`, saveDataToSpouse && `buyers.${spouseIndex}.address.complement`] },
          neighborhood: { path: [`buyers.${index}.address.neighborhood`, saveDataToSpouse && `buyers.${spouseIndex}.address.neighborhood`] },
          uf: { path: [`buyers.${index}.address.uf`, saveDataToSpouse && `buyers.${spouseIndex}.address.uf`] },
          city: { path: [`buyers.${index}.address.city`, saveDataToSpouse && `buyers.${spouseIndex}.address.city`] },
          residenceTime: { path: [`buyers.${index}.address.residenceTime`, saveDataToSpouse && `buyers.${spouseIndex}.address.residenceTime`], unmask: (v, values) => (values.timeUnit === 'years' ? v * 12 : v) },
        }}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
      >
        <Field>
          {({ form }) => (
            <Select
              id="type"
              errors={form.errors}
              touched={form.touched}
              options={adressTypeItems}
              handleBlur={form.handleBlur}
              handleChange={(v) => {
                form.setFieldValue('type', v);
              }}
              value={form.values.type}
              maxHeight="50vh"
              readOnly
              placeholder={intlVerification(intl, 'endereco', 'labelType', intlMessages, messages, style, strMessages)}
              noUp
              partnerColors={style}
            />
          )}
        </Field>
        <Field>
          {({ form }) => (
            <Address
              dictionary={dictionary}
              values={form.values}
              errors={form.errors}
              touched={form.touched}
              setFieldValue={form.setFieldValue}
              setValues={form.setValues}
              handleChange={form.handleChange}
              handleBlur={form.handleBlur}
              messages={messages}
              disabled={getDataFromSpouse}
              setFieldTouched={form.setFieldTouched}
              setFieldError={form.setFieldError}
              intlMessages={intlMessages}
              style={style}
              strMessages={strMessages}
            />
          )}
        </Field>
        <Field>
          {({ form }) => (
            <div className="row">
              <div style={{ width: '70%' }}>
                <InputField
                  id="residenceTime"
                  type="number"
                  min="0"
                  max={form.values.timeUnit === 'months' ? '11' : ''}
                  onChange={(e) => {
                    form.handleChange(e);
                    if (e.target.value < 0) {
                      form.setFieldValue('residenceTime', -e.target.value);
                    }
                  }}
                  onBlur={form.handleBlur}
                  value={form.values.residenceTime}
                  errors={form.errors}
                  touched={form.touched}
                  disabled={getDataFromSpouse}
                  placeHolder={intlVerification(intl, 'endereco', 'residenceTime', intlMessages, messages, style, strMessages)}
                  partnerColors={style}
                />
              </div>

              <div style={{ width: '30%' }}>
                <Select
                  id="timeUnit"
                  errors={form.errors}
                  touched={form.touched}
                  options={yearsAndMonths}
                  handleBlur={form.handleBlur}
                  handleChange={(v) => {
                    form.setFieldValue('timeUnit', v);
                    if (v === 'months' && form.values.residenceTime > 11) {
                      form.setFieldValue('residenceTime', 11);
                    }
                  }}
                  value={form.values.timeUnit}
                  maxHeight="50vh"
                  disabled={getDataFromSpouse}
                  readOnly
                  noUp
                  partnerColors={style}
                />
              </div>
            </div>
          )}
        </Field>
        {children}
      </FormStructure>
    </WLTheme>
  );
}

Endereco.propTypes = {
  /**
 * Continuação do formulário, caso o usuário queira adicionar algum HTML entre o último input
 * e os botões.<br>
 * Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
 */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /**
 * Valores iniciais do formulário se ele não começa vazio,
 * se entrar valores diferentes dos alterados no formulário eles vão ser retornados no submit também.
 */
  // initialValues: PropTypes.object,
  /**
 * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
 */
  validationSchema: PropTypes.func,
  /**
 * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
 * <ul>
 * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
 * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
 * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
 * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
 * <ul>
 */
  onSubmit: PropTypes.func.isRequired,
  /**
 * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
 */
  onBack: PropTypes.func,
  /**
 * Flag que se true dá ao botão submit o atributo disabled.
 */
  disabledSubmit: PropTypes.bool,
  /**
 * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
 */
  style: PropTypes.object.isRequired,
  /**
 * Objeto contendo as mensagens a serem substituídas.<br>
 * <i>Verificar o 'Mensagens de Endereco' para ver quais as mensagens que podem ser substituídas.</i>
 */
  messages: PropTypes.object,
  /**
 * Objeto contendo as mensagens de erro a serem substituídas.</br>
 * <i>Verificar o 'Mensagens de Validação' para ver quais as mensagens que podem ser substituídas.</i>
 */
  validationMessages: PropTypes.object,
  /**
 * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.valorOperacao.{campo}'.
 * <i>Verificar o 'Mensagens de Endereco' para ver quais as mensagens que podem ser substituídas.</i>
 */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
 * Flag que quando true, após a confirmação do usuário, envia os dados do lead
 * atualizados para a API da credihome.
 ** Se estiver true, leadData e axios são obrigatórios.
 */
  sendAPI: PropTypes.bool,
  /**
 * Dados do lead a serem atualizados, deve seguir o padrão da credihome se sendAPI for true.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  leadData: PropTypes.object,
  /**
 * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  axios: PropTypes.func,
  /**
 * Função que recebe um erro caso a chamada da API retorne erro.
 */
  setError: PropTypes.func,
  /**
 * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
 * de rodar tudo que deveria após a submissão.
 */
  setSubmitting: PropTypes.func,
  /**
  * Flag que quando true passa os dados de onSubmit sem máscara.
  */
  unmask: PropTypes.bool,
  /**
  * Posição do proponente que está sendo alterado.
  */
  index: PropTypes.string.isRequired,
  /**
  * Produto escolhido pelo usuário, se for null ele será igual a leadData.lastSimulation.type
  */
  product: PropTypes.string.isRequired,
};

Endereco.defaultProps = {
  children: null,
  // initialValues: {},
  onBack: null,
  validationSchema: null,
  disabledSubmit: false,
  messages: null,
  validationMessages: null,
  intl: null,
  sendAPI: null,
  prepareData: null,
  leadData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
  unmask: null,
};

export default Endereco;
