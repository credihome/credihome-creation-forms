import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.endereco';

const intlMessages = defineMessages({
  primaryBuyer: {
    id: `${scope}.primaryBuyer`,
    defaultMessage: 'Fale mais sobre você',
  },
  primaryProponent: {
    id: `${scope}.primaryProponent`,
    defaultMessage: 'Fale mais sobre você',
  },
  othersBuyers: {
    id: `${scope}.othersBuyers`,
    defaultMessage: 'Informe os dados do {buyer}º comprador',
  },
  othersProponents: {
    id: `${scope}.othersProponents`,
    defaultMessage: 'Informe os dados do {buyer}º proponente',
  },
  currentAddress: {
    id: `${scope}.currentAddress`,
    defaultMessage: 'Endereço residencial (atual)',
  },
  labelType: {
    id: `${scope}.labelType`,
    defaultMessage: 'Tipo de residência',
  },
  zipcode: {
    id: `${scope}.zipcode`,
    defaultMessage: 'CEP',
  },
  address: {
    id: `${scope}.address`,
    defaultMessage: 'Rua',
  },
  number: {
    id: `${scope}.number`,
    defaultMessage: 'Número',
  },
  complement: {
    id: `${scope}.complement`,
    defaultMessage: 'Complemento',
  },
  neighborhood: {
    id: `${scope}.neighborhood`,
    defaultMessage: 'Bairo',
  },
  uf: {
    id: `${scope}.uf`,
    defaultMessage: 'Estado',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'Cidade',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
  forgetZipcode: {
    id: `${scope}.forgetZipcode`,
    defaultMessage: 'Não sei meu CEP',
  },
  residenceTime: {
    id: `${scope}.residenceTime`,
    defaultMessage: 'Tempo neste endereço',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.endereco.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Endereco
* @alias Mensagens de Endereço
* @const
* @type {Object}
* @example
* const messages = {
  primaryBuyer: 'Fale mais sobre você',
  primaryProponent: 'Fale mais sobre você',
  othersBuyers: 'Informe os dados do {buyer}º comprador',
  othersProponents: 'Informe os dados do {buyer}º proponente',
  currentAddress: 'Endereço residencial (atual)',
  labelType: 'Tipo de residência',
  zipcode: 'CEP',
  address: 'Rua',
  number: 'Número',
  complement: 'Complemento',
  neighborhood: 'Bairo',
  uf: 'Estado',
  city: 'Cidade',
  submit: 'Continuar',
  back: 'Voltar',
  forgetZipcode: 'Não sei meu CEP',
  residenceTime: 'Tempo neste endereço',
};
*/
const strMessages = {
  primaryBuyer: 'Fale mais sobre você',
  primaryProponent: 'Fale mais sobre você',
  othersBuyers: 'Informe os dados do {buyer}º comprador',
  othersProponents: 'Informe os dados do {buyer}º proponente',
  currentAddress: 'Endereço residencial (atual)',
  labelType: 'Tipo de residência',
  zipcode: 'CEP',
  address: 'Rua',
  number: 'Número',
  complement: 'Complemento',
  neighborhood: 'Bairo',
  uf: 'Estado',
  city: 'Cidade',
  submit: 'Continuar',
  back: 'Voltar',
  forgetZipcode: 'Não sei meu CEP',
  residenceTime: 'Tempo neste endereço',
};

export { scope, intlMessages, strMessages };
