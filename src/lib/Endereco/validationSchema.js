import * as Yup from 'yup';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from '../../utils/validationMessages';

const validationSchema = (intl, validationMessages) => {
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);
  const msgMoreThanZero = intlVerification(intl, 'validation', 'ValidationMoreThanZero', intlMessages, validationMessages, {}, strMessages);
  const msgLessThanYear = intlVerification(intl, 'validation', 'ValidationLessThanYear', intlMessages, validationMessages, {}, strMessages);

  const lessThanYear = (months) => parseInt(months, 10) < 12;
  const moreThanZero = (months) => parseInt(months, 10) >= 0;

  return Yup.object().shape({
    type: Yup.string().required(msgRequiredField),
    zipcode: Yup.string().matches(/^[0-9]{5}-{0,1}[0-9]{3}$/, 'CEP incompleto.').required(msgRequiredField),
    streetName: Yup.string().required(msgRequiredField),
    number: Yup.string().required(msgRequiredField),
    complement: Yup.string().nullable(),
    neighborhood: Yup.string()
      .nullable()
      .required(msgRequiredField),
    city: Yup.string().required(msgRequiredField),
    uf: Yup.string().required(msgRequiredField),
    residenceTime: Yup.string().when(['timeUnit'], (timeUnit, schema) => (timeUnit === 'months'
      ? schema
        .required(msgRequiredField)
        .test('less_than_year', msgLessThanYear, lessThanYear)
        .test('more_than_zero', msgMoreThanZero, moreThanZero)
      : schema.required(msgRequiredField)
        .test('more_than_zero', msgMoreThanZero, moreThanZero))),
    timeUnit: Yup.string().required(msgRequiredField),
  });
};

export default validationSchema;
