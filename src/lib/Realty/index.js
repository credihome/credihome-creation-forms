/* eslint-disable import/no-named-default, no-shadow */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'formik';
import { WLTheme, Select } from 'credihome-react-library';
import _ from 'lodash';
import FormStructure from '../FormStructure';
import { InputField } from '../../components/Input/InputField';
import { LabelSmall, SectionHeader } from '../../components/Text';
import objectToArray from '../../utils/objectToArray';
import api from '../../api';
import { default as defaultValidation } from './validationSchema';
import { intlMessages, strMessages } from './defaultMessages';
import intlVerification from '../../utils/intlVerification';

import { RadioRound } from '../../components/Radio';
import { integerMask, numberUnmask } from '../../utils/masks';
import { Address } from '../Address';
import { Localization } from '../Localization';

/**
 * Formulário Realty.
 *
 * @category Formulários - Dados do Imóvel
 * @component
 */

function Realty(props) {
  const {
    children,
    /* initialValues, */
    validationSchema,
    onSubmit,
    onBack,
    disabledSubmit,
    style,
    messages,
    validationMessages,
    intl,
    sendAPI,
    prepareData,
    leadData,
    axios,
    setError,
    setSubmitting,
    unmask,
    product,
  } = props;
  console.log(leadData);
  const buyerAddress = leadData.buyers[0]?.address;

  const [dictionary, setDictionary] = useState(undefined);

  useEffect(() => {
    async function getDictionary() {
      await api.getDictionary()
        .then((res) => setDictionary(res))
        .catch((err) => console.log(err));
    }
    getDictionary();
  }, []);

  if (!dictionary) return 'Carregando...';

  const propertyTypes = objectToArray(dictionary.propertyType).map((item) => ({
    key: item.id,
    label: item.name,
  }));

  const useType = objectToArray(dictionary.useType).map((item) => ({
    key: item.id,
    label: item.name,
  }));

  const renderRelativesList = _.sortBy(
    objectToArray(dictionary.relativesList).map((item) => ({
      key: item.id,
      label: item.name,
      order: item.order,
    })),
    'order',
  );

  const yesAndNo = [{ key: true, label: 'Sim' }, { key: false, label: 'Não' }];

  const initialValues = {
    hasRealty: product === 'home_equity' ? true : leadData?.realty?.hasRealty,
    realtyType: leadData?.realty?.realtyType,
    useType: leadData?.realty?.useType,
    onlyGuarantee: leadData?.realty?.onlyGuarantee,
    realtyOwn: leadData?.realty?.realtyOwn,
    owner: leadData?.realty?.owner,
    paidProperty: leadData?.realty?.paidProperty,
    settlementDebt: leadData?.realty?.settlementDebt,
  };

  const initialAddress = {
    zipcode: leadData?.realty?.zipcode,
    neighborhood: leadData?.realty?.neighborhood,
    uf: leadData?.realty?.uf,
    streetName: leadData?.realty?.streetName,
    city: leadData?.realty?.city,
    cep_unico: undefined,
    number: leadData?.realty?.number,
    complement: leadData?.realty?.complement,
    age: leadData?.realty?.age,
    totalArea: leadData?.realty?.totalArea,
    monthlyFees: leadData?.realty?.monthlyFees,
  };

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={{ ...initialValues, ...initialAddress }}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages, product) : defaultValidation(intl, validationMessages, product)}
        onSubmit={(values, sendData, res) => (unmask
          ? onSubmit({
            ...values, settlementDebt: numberUnmask(values.settlementDebt), monthlyFees: numberUnmask(values.monthlyFees), age: parseInt(values.age, 10), totalArea: parseInt(values.totalArea, 10),
          }, sendData, res)
          : onSubmit(values, sendData, res))}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'realty', 'submit', intlMessages, messages, style, strMessages)}
        textBack={intlVerification(intl, 'realty', 'back', intlMessages, messages, style, strMessages)}
        textSubtitle={intlVerification(intl, 'realty', 'title', intlMessages, messages, style, strMessages)}
        disabledSubmit={disabledSubmit}
        style={style}
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
        leadDataManipulation={{
          hasRealty: { path: ['realty.hasRealty'] },
          realtyType: { path: ['realty.realtyType'] },
          useType: { path: ['realty.useType'] },
          onlyGuarantee: { path: ['realty.onlyGuarantee'] },
          realtyOwn: { path: ['realty.realtyOwn'] },
          owner: { path: ['realty.owner'] },
          paidProperty: { path: ['realty.paidProperty'] },
          settlementDebt: { path: ['realty.settlementDebt'], unmask: (value) => numberUnmask(value) },
          zipcode: { path: ['realty.zipcode'] },
          neighborhood: { path: ['realty.neighborhood'] },
          uf: { path: ['realty.uf'] },
          streetName: { path: ['realty.streetName'] },
          city: { path: ['realty.city'] },
          number: { path: ['realty.number'] },
          complement: { path: ['realty.complement'] },
          age: { path: ['realty.age'], unmask: (value) => parseInt(value, 10) },
          totalArea: { path: ['realty.totalArea'], unmask: (value) => parseInt(value, 10) },
          monthlyFees: { path: ['realty.monthlyFees'], unmask: (value) => numberUnmask(value) },
        }}
      >
        <SectionHeader partnerColors={style}>
          {product === 'carencia'
            ? intlVerification(intl, 'realty', 'infoHeader', intlMessages, messages, style, strMessages)
            : intlVerification(intl, 'realty', 'title', intlMessages, messages, style, strMessages)}
        </SectionHeader>
        <Field>
          {({ form }) => (
            <Select
              id="realtyType"
              errors={form.errors}
              touched={form.touched}
              options={propertyTypes}
              handleBlur={form.handleBlur}
              handleChange={(v) => {
                form.setFieldValue('realtyType', v);
              }}
              value={form.values.realtyType}
              maxHeight="50vh"
              readOnly
              placeholder={intlVerification(intl, 'realty', 'propertyType', intlMessages, messages, style, strMessages)}
              noUp
              partnerColors={style}
            />
          )}
        </Field>
        {product !== 'home_equity' && (
          <div>
            <Field>
              {({ form }) => (
                <Select
                  id="useType"
                  errors={form.errors}
                  touched={form.touched}
                  options={useType}
                  handleBlur={form.handleBlur}
                  handleChange={(v) => {
                    form.setFieldValue('useType', v);
                  }}
                  value={form.values.useType}
                  maxHeight="50vh"
                  readOnly
                  placeholder={intlVerification(intl, 'realty', 'useType', intlMessages, messages, style, strMessages)}
                  noUp
                  partnerColors={style}
                />
              )}
            </Field>
            <SectionHeader partnerColors={style}>
              {intlVerification(intl, 'realty', 'localHeader', intlMessages, messages, style, strMessages)}
            </SectionHeader>
          </div>
        )}

        {product !== 'home_equity' && (
          <div className="row" style={{ marginBottom: '12px' }}>
            <LabelSmall
              inputFor="hasRealty"
              text={intlVerification(intl, 'realty', 'hasRealty', intlMessages, messages, style, strMessages)}
              partnerColors={style}
            />
            <Field>
              {({ form }) => (
                <RadioRound
                  name="hasRealty"
                  id="hasRealty"
                  caption={intlVerification(intl, 'realty', 'hasRealty', intlMessages, messages, style, strMessages)}
                  errors={form.errors}
                  touched={form.touched}
                  options={yesAndNo}
                  value={form.values.hasRealty}
                  onChangeItem={(key) => {
                    form.setFieldTouched('hasRealty', true);
                    return form.setValues(
                      key === true
                        ? {
                          ...form.values,
                          hasRealty: key,
                          city: initialAddress.city,
                          uf: initialAddress.uf,
                        }
                        : {
                          ...form.values,
                          hasRealty: key,
                          age: undefined,
                          totalArea: undefined,
                          monthlyFees: undefined,
                          zipcode: undefined,
                          streetName: undefined,
                          number: undefined,
                          complement: undefined,
                          neighborhood: undefined,
                          city: undefined,
                          uf: undefined,
                        },
                    );
                  }}
                  partnerColors={style}
                />
              )}
            </Field>
          </div>
        )}

        <FieldArray
          name="area"
          render={(arr) => (
            (arr.form.values.hasRealty || product === 'home_equity')
            && (
            <div>
              <Field>
                {({ form }) => (
                  <InputField
                    id="age"
                    type="text"
                    mask={integerMask}
                    onChange={form.handleChange}
                    onBlur={form.handleBlur}
                    value={form.values.age}
                    errors={form.errors}
                    touched={form.touched}
                    placeHolder={intlVerification(intl, 'realty', 'age', intlMessages, messages, style, strMessages)}
                    partnerColors={style}
                  />
                )}
              </Field>
              <Field>
                {({ form }) => (
                  <InputField
                    id="totalArea"
                    type="text"
                    mask={integerMask}
                    onChange={form.handleChange}
                    onBlur={form.handleBlur}
                    value={form.values.totalArea}
                    errors={form.errors}
                    touched={form.touched}
                    placeHolder={intlVerification(intl, 'realty', 'area', intlMessages, messages, style, strMessages)}
                    partnerColors={style}
                  />
                )}
              </Field>
              <Field>
                {({ form }) => (
                  <InputField
                    id="monthlyFees"
                    type="text"
                    onChange={form.handleChange}
                    onBlur={form.handleBlur}
                    value={form.values.monthlyFees}
                    errors={form.errors}
                    touched={form.touched}
                    placeHolder={intlVerification(intl, 'realty', 'monthlyFees', intlMessages, messages, style, strMessages)}
                    maskmoney
                    partnerColors={style}
                  />
                )}
              </Field>
              {product === 'home_equity' && (
              <>
                <div className="row" style={{ marginBottom: '12px' }}>
                  <LabelSmall
                    inputFor="onlyGuarantee"
                    text={intlVerification(intl, 'realty', 'onlyGuarantee', intlMessages, messages, style, strMessages)}
                    partnerColors={style}
                  />
                  <Field>
                    {({ form }) => (
                      <RadioRound
                        name="onlyGuarantee"
                        id="onlyGuarantee"
                        caption={intlVerification(intl, 'realty', 'onlyGuarantee', intlMessages, messages, style, strMessages)}
                        errors={form.errors}
                        touched={form.touched}
                        options={yesAndNo}
                        value={form.values.onlyGuarantee}
                        onChangeItem={(key) => {
                          form.setFieldTouched('onlyGuarantee', true);
                          if (buyerAddress === undefined) {
                            console.error('ERROR: Endereço do comprador não existe');
                          }
                          return form.setValues(
                            key === true
                              ? {
                                ...form.values,
                                onlyGuarantee: key,
                                zipcode: buyerAddress?.zipcode,
                                neighborhood: buyerAddress?.neighborhood,
                                streetName: buyerAddress?.streetName,
                                city: buyerAddress?.city,
                                number: buyerAddress?.number,
                                complement: buyerAddress?.complement,
                                uf: buyerAddress?.uf,
                              }
                              : {
                                ...form.values,
                                onlyGuarantee: key,
                                zipcode: initialAddress.zipcode,
                                neighborhood: initialAddress.neighborhood,
                                streetName: initialAddress.streetName,
                                city: initialAddress.city,
                                number: initialAddress.number,
                                complement: initialAddress.complement,
                                uf: initialAddress.uf,
                              },
                          );
                        }}
                        partnerColors={style}
                      />
                    )}
                  </Field>
                </div>

                <div className="row" style={{ marginBottom: '12px' }}>
                  <LabelSmall
                    inputFor="realtyOwn"
                    text={intlVerification(intl, 'realty', 'realtyOwn', intlMessages, messages, style, strMessages)}
                    partnerColors={style}
                  />
                  <Field>
                    {({ form }) => (
                      <RadioRound
                        name="realtyOwn"
                        id="realtyOwn"
                        caption={intlVerification(intl, 'realty', 'realtyOwn', intlMessages, messages, style, strMessages)}
                        errors={form.errors}
                        touched={form.touched}
                        options={yesAndNo}
                        value={form.values.realtyOwn}
                        onChangeItem={(key) => {
                          form.setFieldTouched('realtyOwn', true);
                          return form.setValues({
                            ...form.values,
                            realtyOwn: key,
                            owner: key === true ? 'self' : undefined,
                          });
                        }}
                        partnerColors={style}
                      />
                    )}
                  </Field>
                </div>
                <Field>
                  {({ form }) => form.values.realtyOwn === false && (
                  <Select
                    id="owner"
                    errors={form.errors}
                    touched={form.touched}
                    options={renderRelativesList}
                    handleBlur={form.handleBlur}
                    handleChange={(v) => form.setFieldValue('owner', v)}
                    value={form.values.owner}
                    maxHeight="50vh"
                    readOnly
                    placeholder={intlVerification(intl, 'realty', 'owner', intlMessages, messages, style, strMessages)}
                    noUp
                    partnerColors={style}
                  />
                  )}
                </Field>
                <div className="row" style={{ marginBottom: '12px' }}>
                  <LabelSmall
                    inputFor="paidProperty"
                    text={intlVerification(intl, 'realty', 'paidProperty', intlMessages, messages, style, strMessages)}
                    partnerColors={style}
                  />
                  <Field>
                    {({ form }) => (
                      <RadioRound
                        name="paidProperty"
                        id="paidProperty"
                        caption={intlVerification(intl, 'realty', 'paidProperty', intlMessages, messages, style, strMessages)}
                        errors={form.errors}
                        touched={form.touched}
                        options={yesAndNo}
                        value={form.values.paidProperty}
                        onChangeItem={(key) => {
                          form.setFieldTouched('paidProperty', true);
                          return form.setValues({
                            ...form.values,
                            paidProperty: key,
                            settlementDebt: key === true ? 0 : undefined,
                          });
                        }}
                        partnerColors={style}
                      />
                    )}
                  </Field>
                </div>
                <FieldArray
                  name="settlementDebt"
                  render={(arr) => (
                    (arr.form.values.paidProperty === false)
                      && (
                      <Field>
                        {({ form }) => (
                          <InputField
                            id="settlementDebt"
                            type="text"
                            onChange={form.handleChange}
                            onBlur={form.handleBlur}
                            value={form.values.settlementDebt}
                            errors={form.errors}
                            touched={form.touched}
                            placeHolder={intlVerification(intl, 'realty', 'settlementDebt', intlMessages, messages, style, strMessages)}
                            maskmoney
                            partnerColors={style}
                            style={style}
                          />
                        )}
                      </Field>
                      )
                  )}
                />
              </>
              )}
              <Field>
                {({ form }) => (form.values.onlyGuarantee === false
                  || (product !== 'home_equity' && form.values.hasRealty)) && (
                    <div>
                      <SectionHeader partnerColors={style}>
                        {intlVerification(intl, 'realty', 'addressHeader', intlMessages, messages, style, strMessages)}
                      </SectionHeader>
                      <Address
                        dictionary={dictionary}
                        values={form.values}
                        errors={form.errors}
                        touched={form.touched}
                        setFieldValue={form.setFieldValue}
                        setFieldTouched={form.setFieldTouched}
                        setFieldError={form.setFieldError}
                        setValues={form.setValues}
                        handleChange={form.handleChange}
                        handleBlur={form.handleBlur}
                        messages={messages}
                        intl={intl}
                        style={style}
                      />
                    </div>
                )}
              </Field>
            </div>
            )
          )}
        />

        <FieldArray
          name="Localization"
          render={(arr) => arr.form.values.hasRealty === false && (
            <Localization
              dictionary={dictionary}
              values={arr.form.values}
              handleBlur={arr.form.handleBlur}
              setFieldValue={arr.form.setFieldValue}
              setValues={arr.form.setValues}
              handleChange={arr.form.handleChange}
              errors={arr.form.errors}
              touched={arr.form.touched}
              labelState="uf"
              labelCity="city"
              messages={messages}
              intl={intl}
            />
          )}
        />

        {children}
      </FormStructure>
    </WLTheme>
  );
}

Realty.propTypes = {
  /**
 * Continuação do formulário, caso o usuário queira adicionar algum HTML entre o último input
 * e os botões.<br>
 * Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
 */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  // initialValues: PropTypes.object,
  /**
 * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
 */
  validationSchema: PropTypes.func,
  /**
  * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
  * <ul>
  * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
  * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
  * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
  * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
  * <ul>
  */
  onSubmit: PropTypes.func.isRequired,
  /**
  * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
  */
  onBack: PropTypes.func,
  /**
  * Flag que se true dá ao botão submit o atributo disabled.
  */
  disabledSubmit: PropTypes.bool,
  /**
  * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
  */
  style: PropTypes.object.isRequired,
  /**
  * Objeto contendo as mensagens a serem substituídas.<br>
  * <i>Verificar o 'Mensagens de Contato' para ver quais as mensagens que podem ser substituídas.</i>
  */
  messages: PropTypes.object,
  /**
  * Objeto contendo as mensagens de erro a serem substituídas.</br>
  * <i>Verificar o 'Mensagens de Validação' para ver quais as mensagens que podem ser substituídas.</i>
  */
  validationMessages: PropTypes.object,
  /**
  * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.contato.{campo}'.
  * <i>Verificar o 'Mensagens de Contato' para ver quais as mensagens que podem ser substituídas.</i>
  */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
  * Flag que quando true, após a confirmação do usuário, envia os dados do lead
  * atualizados para a API da credihome.
  * Se estiver true, leadData e axios são obrigatórios.
  */
  sendAPI: PropTypes.bool,
  /**
  * <b>Obrigatório para esse formulário, substituí o que seria initialValues nos outros forms</b>
  * Dados do lead a serem atualizados, é obrigatório há muitas regras que dependem dele no WL.<br/>
  * Coisas como ter nome, CPF, email, telefone e data de nascimento do primeiro proponente já
  * escrito, ou precisar da lista de buyers para saber se os CPF não são igauis ou se o proponente
  * é spouse de alguém.
  */
  leadData: PropTypes.object.isRequired,
  /**
  * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
  * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
  */
  axios: PropTypes.func,
  /**
  * Função que recebe um erro caso a chamada da API retorne erro.
  */
  setError: PropTypes.func,
  /**
  * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
  * de rodar tudo que deveria após a submissão.
  */
  setSubmitting: PropTypes.func,
  /**
  * Flag que quando true passa os dados de onSubmit sem máscara.
  */
  unmask: PropTypes.bool,
  /**
  * Produto escolhido pelo usuário, se for null ele será igual a leadData.lastSimulation.type
  */
  product: PropTypes.string,
};

Realty.defaultProps = {
  children: null,
  // initialValues: {},
  validationSchema: null,
  onBack: null,
  disabledSubmit: false,
  messages: null,
  validationMessages: null,
  intl: null,
  sendAPI: null,
  prepareData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
  unmask: null,
  product: null,
};

export default Realty;
