import * as Yup from 'yup';
import intlVerification from '../../utils/intlVerification';
import utils from '../../utils/utils';
import { intlMessages, strMessages } from '../../utils/validationMessages';

const validationSchema = (intl, validationMessages, product) => {
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidArea = intlVerification(intl, 'validation', 'ValidationInvalidArea', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidAge = intlVerification(intl, 'validation', 'ValidationInvalidMaxAge', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidMonthlyFees = intlVerification(intl, 'validation', 'ValidationInvalidMonthlyFees', intlMessages, validationMessages, {}, strMessages);

  return Yup.object().shape({
    realtyOwn: Yup.bool().when('product', (t, schema) =>
      product === 'mortgage' || product === 'carencia' ? schema : schema.required(msgRequiredField),
    ),
    owner: Yup.string().when('product', (t, schema) =>
      product === 'mortgage' || product === 'carencia' ? schema : schema.required(msgRequiredField),
    ),
    onlyGuarantee: Yup.bool().when('product', (t, schema) =>
      product === 'mortgage' || product === 'carencia' ? schema : schema.required(msgRequiredField),
    ),
    paidProperty: Yup.bool().when('product', (t, schema) =>
      product === 'mortgage' || product === 'carencia' ? schema : schema.required(msgRequiredField),
    ),
    hasRealty: Yup.bool().when('product', (t, schema) => schema.required(msgRequiredField),
    ),
    realtyType: Yup.string().required(msgRequiredField),
    useType: Yup.string().when('product', (t, schema) =>
      product === 'mortgage' || product === 'carencia' ? schema.required(msgRequiredField) : schema,
    ),
    zipcode: Yup.string().when('hasRealty', (hasRealty, schema) =>
      hasRealty === true
        ? schema
          .required(msgRequiredField)
          .matches(/^[0-9]{5}-{0,1}[0-9]{3}$/, 'CEP incompleto.')
        : schema.nullable(true),
    ),
    streetName: Yup.string().when('hasRealty', (hasRealty, schema) =>
      hasRealty === true ? schema.required(msgRequiredField) : schema,
    ),
    number: Yup.string()
      .nullable()
      .when('hasRealty', (hasRealty, schema) =>
        hasRealty === true ? schema.required(msgRequiredField) : schema,
      ),
    neighborhood: Yup.string()
      .nullable()
      .when('hasRealty', (hasRealty, schema) =>
        hasRealty === true ? schema.required(msgRequiredField) : schema,
      ),
    city: Yup.string().required(msgRequiredField),
    uf: Yup.string().required(msgRequiredField),
    totalArea: utils.customYupSchemas
      .masked()
      .nullable()
      .when('hasRealty', (hasRealty, schema) =>
        hasRealty === true // eslint-disable-next-line prettier/prettier
          ? schema.required(msgRequiredField).test('is_valid_area', msgInvalidArea, value => value > 0 && value < 1000000)
          : schema,
      ),
    age: utils.customYupSchemas
      .masked()
      .nullable()
      .when('hasRealty', (hasRealty, schema) =>
        hasRealty === true // eslint-disable-next-line prettier/prettier
          ? schema.required(msgRequiredField).test('is_valid_age_max', msgInvalidAge, value => value < 100)
          : schema,
      ),
    monthlyFees: utils.customYupSchemas
      .masked()
      .when('hasRealty', (hasRealty, schema) =>
        hasRealty === true // eslint-disable-next-line prettier/prettier
          ? schema.required(msgRequiredField).test('is_val_montFe', msgInvalidMonthlyFees, value => value > 0 && value < 100000000)
          : schema,
      ),
  });
};
export default validationSchema;
