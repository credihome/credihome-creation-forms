import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.realty';

const intlMessages = defineMessages({
  title: {
    id: `${scope}.header`,
    defaultMessage: 'Fale sobre o imóvel.',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Finalizar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
  propertyType: {
    id: `${scope}.propertyType`,
    defaultMessage: 'Tipo de imóvel',
  },
  useType: {
    id: `${scope}.useType`,
    defaultMessage: 'Finalidade do imóvel',
  },
  hasRealty: {
    id: `${scope}.hasRealty`,
    defaultMessage: 'Possui um imóvel definido?',
  },
  localHeader: {
    id: `${scope}.localHeader`,
    defaultMessage: 'Localização do imóvel',
  },
  address: {
    id: `${scope}.address`,
    defaultMessage: 'Endereço',
  },
  zipcode: {
    id: `${scope}.zipcode`,
    defaultMessage: 'CEP',
  },
  neighborhood: {
    id: `${scope}.neighborhood`,
    defaultMessage: 'Bairro',
  },
  number: {
    id: `${scope}.number`,
    defaultMessage: 'Número',
  },
  complement: {
    id: `${scope}.complement`,
    defaultMessage: 'Complemento',
  },
  uf: {
    id: `${scope}.uf`,
    defaultMessage: 'UF',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'Cidade',
  },
  localizationState: {
    id: `${scope}.localizationState`,
    defaultMessage: 'UF',
  },
  localizationCity: {
    id: `${scope}.localizationCity`,
    defaultMessage: 'Cidade',
  },
  area: {
    id: `${scope}.area`,
    defaultMessage: 'Área do imóvel (m²)',
  },
  age: {
    id: `${scope}.age`,
    defaultMessage: 'Idade do imóvel (anos)',
  },
  monthlyFees: {
    id: `${scope}.monthlyFees`,
    defaultMessage: 'Valor do condomínio + IPTU',
  },
  infoHeader: {
    id: `${scope}.infoHeader`,
    defaultMessage: 'Informações do imóvel',
  },
  localHeader: {
    id: `${scope}.localHeader`,
    defaultMessage: 'Localização do imóvel',
  },
  forgetZipcode: {
    id: `${scope}.forgetZipcode`,
    defaultMessage: 'Não sei meu CEP',
  },
  addressHeader: {
    id: `${scope}.addressHeader`,
    defaultMessage: 'Endereço do imóvel',
  },
  ValidationRequiredField: {
    id: `${scope}.ValidationRequiredField`,
    defaultMessage: 'Campo obrigatório',
  },
  ValidationInvalidArea: {
    id: `${scope}.ValidationInvalidArea`,
    defaultMessage: 'Deve ser maior que zero ou menor que um milhão',
  },
  ValidationInvalidMaxAge: {
    id: `${scope}.ValidationInvalidMaxAge`,
    defaultMessage: 'Idade acima do  limite',
  },
  ValidationInvalidMonthlyFees: {
    id: `${scope}.ValidationInvalidMonthlyFees`,
    defaultMessage:
      'O valor deve ser maior do que zero e menor do que um milhão',
  },
  onlyGuarantee: {
    id: `${scope}.onlyGuarantee`,
    defaultMessage: 'O imóvel dado como garantia é o mesmo onde mora?',
  },
  realtyOwn: {
    id: `${scope}.realtyOwn`,
    defaultMessage: 'O imóvel está em seu nome?',
  },
  owner: {
    id: `${scope}.owner`,
    defaultMessage: 'De quem é o imóvel?',
  },
  paidProperty: {
    id: `${scope}.paidProperty`,
    defaultMessage: 'O imóvel esta quitado?',
  },
  settlementDebt: {
    id: `${scope}.settlementDebt`,
    defaultMessage: 'Saldo devedor (opcional)',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.realty.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Realty
* @alias Mensagens de Realty
* @const
* @type {Object}
* @example
* const messages = {
  title: 'Fale sobre o imóvel.',
  submit: 'Finalizar',
  back: 'Voltar',
  propertyType: 'Tipo de imóvel',
  useType: 'Finalidade do imóvel',
  hasRealty: 'Possui um imóvel definido?',
  address: 'Endereço',
  zipcode: 'CEP',
  neighborhood: 'Bairro',
  number: 'Número',
  complement: 'Complemento',
  uf: 'UF',
  city: 'Cidade',
  localizationState: 'UF',
  localizationCity: 'Cidade',
  area: 'Área do imóvel (m²)',
  age: 'Idade do imóvel (anos)',
  monthlyFees: 'Valor do condomínio + IPTU',
  infoHeader: 'Informações do imóvel',
  localHeader: 'Localização do imóvel',
  forgetZipcode: 'Não sei meu CEP',
  addressHeader: 'Endereço do imóvel',
  ValidationRequiredField: 'Campo obrigatório',
  ValidationInvalidArea: 'Deve ser maior que zero ou menor que um milhão',
  ValidationInvalidMaxAge: 'Idade acima do  limite',
  ValidationInvalidMonthlyFees: 'O valor deve ser maior do que zero e menor do que um milhão',
  onlyGuarantee: 'O imóvel dado como garantia é o mesmo onde mora?',
  realtyOwn: 'O imóvel está em seu nome?',
  owner: 'De quem é o imóvel?',
  paidProperty: 'O imóvel esta quitado?',
  settlementDebt: 'Saldo devedor (opcional)',
};
*/
const strMessages = {
  title: 'Fale sobre o imóvel',
  submit: 'Finalizar',
  back: 'Voltar',
  propertyType: 'Tipo de imóvel',
  useType: 'Finalidade do imóvel',
  hasRealty: 'Possui um imóvel definido?',
  address: 'Endereço',
  zipcode: 'CEP',
  neighborhood: 'Bairro',
  number: 'Número',
  complement: 'Complemento',
  uf: 'UF',
  city: 'Cidade',
  localizationState: 'UF',
  localizationCity: 'Cidade',
  area: 'Área do imóvel (m²)',
  age: 'Idade do imóvel (anos)',
  monthlyFees: 'Valor do condomínio + IPTU',
  infoHeader: 'Informações do imóvel',
  localHeader: 'Localização do imóvel',
  forgetZipcode: 'Não sei meu CEP',
  addressHeader: 'Endereço do imóvel',
  ValidationRequiredField: 'Campo obrigatório',
  ValidationInvalidArea: 'Deve ser maior que zero ou menor que um milhão',
  ValidationInvalidMaxAge: 'Idade acima do  limite',
  ValidationInvalidMonthlyFees: 'O valor deve ser maior do que zero e menor do que um milhão',
  onlyGuarantee: 'O imóvel dado como garantia é o mesmo onde mora?',
  realtyOwn: 'O imóvel está em seu nome?',
  owner: 'De quem é o imóvel?',
  paidProperty: 'O imóvel esta quitado?',
  settlementDebt: 'Saldo devedor (opcional)',
};

export { scope, intlMessages, strMessages };
