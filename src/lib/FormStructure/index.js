/* eslint-disable no-console, no-param-reassign */
import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';

import Section from '../../components/Section';
import Form from '../../components/Form';
import { Back, Submit } from '../../components/Buttons';
import { Subtitle } from '../../components/Text';
import api from '../../api';

/**
 * Wrapper de formulário.<br>
 * Ele adiciona um título no topo com componente e botões no fim do componente,
 * assim como um formik. Para acessar os dados do formik, usar o componente `<Field />`
 *
 * @component
 */

function FormStructure(props) {
  const {
    children,
    initialValues,
    validationSchema,
    onSubmit,
    onBack,
    textSubmit,
    textBack,
    textSubtitle,
    disabledSubmit,
    style,
    alignSubtitle,
    sendAPI,
    setSubmitting,
    setError,
    prepareData,
    leadData,
    leadDataManipulation,
    axios,
  } = props;

  const backButton = onBack && textBack;

  const sendToAPI = (values, sendData) => {
    if (!axios) {
      console.error(`ERROR sendAPI: No formulário de título "${textSubtitle}", "sendAPI" está settado como true porém o prop obrigatório "axios" está indefinido`);
      return;
    }
    api.updateLead(sendData, axios)
      .then((res) => {
        onSubmit(values, sendData, res);
      })
      .catch((err) => {
        if (setError) {
          setError(err);
        } else {
          console.error('ERRO ao enviar para a API: ', err);
        }
      })
      .finally(() => {
        if (setSubmitting) {
          setSubmitting(false);
        }
      });
  };

  const prepareLeadData = (values) => {
    if (!leadData || !leadDataManipulation) {
      console.error(`ERROR prepareData: No formulário de título "${textSubtitle}", "sendAPI" ou "prepareData" estão settados como true porém o(s) props obrigatório(s) ${!leadData ? '"leadData" ' : ''}${!leadDataManipulation ? '"leadDataManipulation" ' : ''}está(ão) indefinido(s)`);
      return;
    }
    if (setSubmitting) {
      setSubmitting(true);
    }
    const sendData = { ...leadData };
    const leadDataManipulationResult = typeof leadDataManipulation === 'function' ? leadDataManipulation(values) : leadDataManipulation;
    Object.keys(leadDataManipulationResult).map((type) => leadDataManipulationResult[type].path.map((p) => p.split('.').reduce((lead, val, i, array) => {
      if (array.length === i + 1) {
        let value = values[type];
        if (leadDataManipulationResult[type].unmask) {
          value = leadDataManipulationResult[type].unmask(values[type], values);
        }
        lead[val] = value;
        return lead[val];
      }

      if (lead[val] === undefined) {
        lead[val] = {};
      }

      return lead[val];
    }, sendData)));
    if (sendAPI) {
      sendToAPI(values, sendData);
    } else {
      onSubmit(values, sendData);
    }
  };

  return (
    <Section>
      {textSubtitle && (
        <div style={alignSubtitle === 'center' ? { marginBottom: '40px' } : {}}>
          <Subtitle textAlign={alignSubtitle} partnerColors={style}>
            {textSubtitle}
          </Subtitle>
        </div>
      )}
      <Formik
        initialValues={initialValues}
        enableReinitialize
        onSubmit={sendAPI || prepareData ? prepareLeadData : onSubmit}
        validationSchema={validationSchema}
      >
        {({ handleSubmit }) => (
          <Form onSubmit={handleSubmit} noValidate>

            {children}

            <div className={backButton ? 'row' : ''}>
              {backButton && (
                <Back
                  width="49%"
                  onClick={onBack}
                  text={textBack}
                  partnerColors={style}
                />
              )}

              <Submit
                width={backButton ? '49%' : '100%'}
                text={textSubmit}
                disabled={disabledSubmit}
                partnerColors={style}
              />
            </div>
          </Form>
        )}
      </Formik>
    </Section>
  );
}

FormStructure.propTypes = {
  /**
 * O formulário em si, para ser criado pelo usuário.<br>
 * Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
 */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  /**
 * Valores iniciais do formulário se ele não começa vazio,
 * se entrar valores diferentes dos alterados no formulário eles vão ser retornados no submit também.
 */
  initialValues: PropTypes.object.isRequired,
  /**
 * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
 */
  validationSchema: PropTypes.object,
  /**
 * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
 * <ul>
 * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário.</li>
 * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
 * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
 * <ul>
 */
  onSubmit: PropTypes.func.isRequired,
  /**
 * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
 */
  onBack: PropTypes.func,
  /**
 * Texto que aparece no botão de submit.
 */
  textSubmit: PropTypes.string.isRequired,
  /**
 * String que altera o posicionamento do título, se o valor for 'center' também troca a margem
 * entre o título e os inputs de 25px para 40px.
 */
  alignSubtitle: PropTypes.string,
  /**
 * Texto que aparece no botão de back.
 */
  textBack: PropTypes.string,
  /**
 * Texto do título.
 */
  textSubtitle: PropTypes.string,
  /**
 * Flag que se true dá ao botão submit o atributo disabled.
 */
  disabledSubmit: PropTypes.bool,
  /**
 * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
 */
  style: PropTypes.object.isRequired,
  /**
 * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
 * os valores passados em leadDataManipulation.<br>
 * Se estiver true, leadData e leadDataManipulation são obrigatórios.
 */
  prepareData: PropTypes.bool,
  /**
 * Flag que quando true, após a confirmação do usuário, envia os dados do lead
 * atualizados para a API da credihome.
 ** Se estiver true, leadData, leadDataManipulation e axios são obrigatórios.
 */
  sendAPI: PropTypes.bool,
  /**
 * Dados do lead a serem atualizados, deve seguir o padrão da credihome se sendAPI for true.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  leadData: PropTypes.object,
  /**
 * Objeto que relaciona um valor do formulário com um campo do leadData, ele segue o seguinte formato:<br>
 * <pre>
 *  {
 *    valorNoFormulárioComUnmask: {
 *      path: ['caminho_do_campo_no_leadData', 'outro_campo_no_leadData'],
 *      unmask: (value) => funçãoQueArrumaOValor(vale)
 *    },
 *    valorNoFormulárioSemUnmask: {
 *      path: ['caminho_do_campo_no_leadData'],
 *    },
 *  }
 * </pre>
 * <i>Obrigatório quando sendAPI ou prepareData são true</i>
 */
  leadDataManipulation: PropTypes.object,
  /**
 * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  axios: PropTypes.func,
  /**
  * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
  * de rodar tudo que deveria após a submissão.
  */
  setSubmitting: PropTypes.func,
  /**
  * Função que recebe um erro caso a chamada da API retorne erro.
  */
  setError: PropTypes.func,
};

FormStructure.defaultProps = {
  onBack: null,
  textBack: null,
  textSubtitle: null,
  validationSchema: Yup.object().shape({}),
  disabledSubmit: false,
  sendAPI: false,
  setSubmitting: null,
  setError: null,
  prepareData: null,
  leadData: null,
  leadDataManipulation: null,
  axios: null,
  alignSubtitle: null,
};

export default FormStructure;
