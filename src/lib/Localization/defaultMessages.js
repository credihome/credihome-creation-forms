import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.Localization';

const intlMessages = defineMessages({
  localizationCity: {
    id: `${scope}.localizationCity`,
    defaultMessage: 'Cidade',
  },
  localizationState: {
    id: `${scope}.localizationState`,
    defaultMessage: 'Estado',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.Localization.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Localization
* @alias Mensagens de Localization
* @const
* @type {Object}
* @example
* const messages = {
  localizationState: 'Estado',
  localizationCity: 'Cidade',
  submit: 'Continuar',
  back: 'Voltar',
};
*/
const strMessages = {
  localizationState: 'Estado',
  localizationCity: 'Cidade',
  submit: 'Continuar',
  back: 'Voltar',
};

export { scope, intlMessages, strMessages };
