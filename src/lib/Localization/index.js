import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Select } from 'credihome-react-library';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from './defaultMessages';
import api from '../../api';
import utils from '../../utils/utils';

export function Localization({
  dictionary,
  values,
  handleBlur,
  setFieldValue,
  setValues,
  errors,
  touched,
  labelState,
  labelCity,
  messages,
  intl,
  style,
}) {
  const [cities, setCities] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (values[labelState]) getCities(values[labelState]);
  }, [values[labelState]]);

  if (!dictionary) return null;
  const getCities = uf => {
    setLoading(true);
    setValues({ ...values, [labelState]: uf });
    api.getCities(uf).then(ret => {
      setCities(ret.map(value => ({ label: value, key: value })));
      setLoading(false);
    });
  };

  const specificOrdenationStates = { SP: 1, RJ: 2 }

  const listStates = utils.objectToArray(dictionary.ufs)
    .map(item => ({ key: item.id, ...dictionary.ufs[item.id] }))
    .sort((a, b) => {
      const textA = a.label.toUpperCase();
      const textB = b.label.toUpperCase();
      return textA > textB ? 1 : -1
    }).sort((a, b) => {
      const textA = a.label.toUpperCase();
      const textB = b.label.toUpperCase();
      if (textA === 'SP' || textA === 'RJ') {
        return specificOrdenationStates[textA] > specificOrdenationStates[textB] ? 1 : -1
      } else {
        return textA > textB ? 1 : -1
      }
    });

  return (
    <div>
      <Select
        id={labelState}
        errors={errors}
        touched={touched}
        options={listStates}
        handleBlur={handleBlur}
        handleChange={v => {
          setFieldValue(labelState, v);
        }}
        value={values[labelState]}
        maxHeight="130px"
        readOnly
        placeholder={intlVerification(intl, 'localization', 'localizationState', intlMessages, messages, style, strMessages)}
        noUp
      />
      <Select
        id={labelCity}
        errors={errors}
        touched={touched}
        options={cities}
        handleBlur={handleBlur}
        handleFilter={(input) =>
          cities.filter(
            (option) =>
              utils
                .removeAccents(option.key)
                .toLowerCase()
                .search(utils.removeAccents(input.toLowerCase())) !== -1
          )
        }
        handleChange={v => {
          setFieldValue(labelCity, v);
        }}
        value={values[labelCity]}
        maxHeight="130px"
        disabled={values[labelState] === undefined}
        placeholder={intlVerification(intl, 'localization', 'localizationCity', intlMessages, messages, style, strMessages)}
        keyPrefix="location-"
        noOptionString={
          loading ? 'Carregando cidades...' : 'Nenhuma cidade encontrada'
        }
        noUp
      />
    </div>
  );
}

Localization.propTypes = {
  /**
* Objeto resultado de uma chamada externa para consulta da listagem das opções dos selects.
*/
  dictionary: PropTypes.object,
  /**
* Função que fornece os valores de cada campo de acordo com cada ID [restrito do Formik].
*/
  values: PropTypes.object.isRequired,
  /**
* Função que aciona o evento de blur no campo [restrito do Formik].
*/
  handleBlur: PropTypes.func.isRequired,
  /**
* Função que insere um valor no campo [restrito do Formik].
*/
  setFieldValue: PropTypes.func.isRequired,
  /**
* Função que insere o valor em multiplos campos conforme acionado [restrito do Formik].
*/
  setValues: PropTypes.func.isRequired,
  /**
 * Função que insere/valida um erro no campo [restrito do Formik].
 */
  errors: PropTypes.object.isRequired,
  /**
 * Função que insere o evento de touch no campo [restrito do Formik].
 */
  touched: PropTypes.object.isRequired,
  /**
* Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
*/
  style: PropTypes.object.isRequired,
  /**
    /**
  * ID do Select de estado
  */
  labelState: PropTypes.string,
  /**
* ID do Select de cidade
*/
  labelCity: PropTypes.string,
  /**
* Objeto contendo as mensagens a serem substituídas.<br>
* <i>Verificar o 'Mensagens de Finalidade' para ver quais as mensagens que podem ser substituídas.</i>
*/
  messages: PropTypes.object,
};

Location.defaultProps = {
  labelState: 'state',
  labelCity: 'city',
  intl: null,
  messages: null,
};

export default Localization;
