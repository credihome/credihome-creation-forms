import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'formik';
import { WLTheme, DatePicker, Select } from 'credihome-react-library';
import FormStructure from '../FormStructure';
import { InputField } from '../../components/Input/InputField';
import { default as defaultValidation } from './validationSchema';
import { intlMessages, strMessages } from './defaultMessages';
import intlVerification from '../../utils/intlVerification';
import { dateMask } from '../../utils/masks';
import api from '../../api';
import { Localization } from '../Localization';
import utils from '../../utils/utils';
import { SectionHeader } from '../../components/Text';

/**
 * Formulário de Documento.
 *
 * @category Formulários - Dados do Proponente
 * @component
 */

function Documento(props) {
  const {
    /* initialValues, */
    style,
    validationSchema,
    intl,
    onBack,
    messages,
    validationMessages,
    children,
    index,
    product,
    disabledSubmit,
    leadData,
    onSubmit,
    sendAPI,
    prepareData,
    axios,
    setError,
    setSubmitting,
  } = props;

  const { buyers } = leadData;
  const [dictionary, setDictionary] = useState(undefined);

  let docIndex = leadData.buyers[index]?.docs?.findIndex((doc) => doc.type === 'rg');
  if (leadData.buyers[index]?.docs === undefined) {
    docIndex = 0;
  } else if (docIndex === -1) {
    docIndex = leadData.buyers[index].docs.length;
  }

  useEffect(() => {
    async function getDictionary() {
      await api.getDictionary()
        .then((res) => setDictionary(res))
        .catch((err) => console.log(err));
    }
    getDictionary();
  }, []);

  if (!dictionary) return 'Carregando...';

  const getMessage = (buyerIndex) => {
    if (product === 'home_equity' || leadData.lastSimulation.type === 'home_equity') {
      if (buyerIndex === '0') {
        return intlVerification(intl, 'documento', 'primaryProponent', intlMessages, messages, style, strMessages);
      }
      return intlVerification(intl, 'documento', 'othersProponents', intlMessages, messages, style, strMessages, { index: 1 + parseInt(index, 10) });
    }
    if (buyerIndex === '0') {
      return intlVerification(intl, 'documento', 'primaryBuyer', intlMessages, messages, style, strMessages);
    }
    return intlVerification(intl, 'documento', 'othersBuyers', intlMessages, messages, style, strMessages, { index: 1 + parseInt(index, 10) });
  };

  const rg = buyers[index]?.docs?.find((doc) => doc.type === 'rg');

  const initialValues = {
    docNumber: rg?.docNumber,
    issuedBy: rg?.issuedBy,
    issueDate: (rg?.issueDate || '')
      .split('/')
      .reverse()
      .join('-'),
    issueLocation: rg?.issueLocation,
    homeTown: buyers[index]?.homeTown,
    homeState: buyers[index]?.homeState,
  };

  const specificOrdenationStates = { SP: 1, RJ: 2 };

  const listUfs = utils.objectToArray(dictionary.ufs)
    .map((item) => ({ key: item.id, ...dictionary.ufs[item.id] }))
    .sort((a, b) => {
      const textA = a.label.toUpperCase();
      const textB = b.label.toUpperCase();
      return textA > textB ? 1 : -1;
    }).sort((a, b) => {
      const textA = a.label.toUpperCase();
      const textB = b.label.toUpperCase();
      if (textA === 'SP' || textA === 'RJ') {
        return specificOrdenationStates[textA] > specificOrdenationStates[textB] ? 1 : -1;
      }
      return textA > textB ? 1 : -1;
    });

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages, buyers, index) : defaultValidation(intl, validationMessages, buyers, index)}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'documento', 'submit', intlMessages, messages, style, strMessages)}
        textBack={intlVerification(intl, 'documento', 'back', intlMessages, messages, style, strMessages)}
        textSubtitle={getMessage(index)}
        disabledSubmit={disabledSubmit}
        style={style}
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
        leadDataManipulation={{
          docNumber: { path: [`buyers.${index}.docs.${docIndex}.docNumber`] },
          issuedBy: { path: [`buyers.${index}.docs.${docIndex}.issuedBy`] },
          issueDate: { path: [`buyers.${index}.docs.${docIndex}.issueDate`] },
          issueLocation: { path: [`buyers.${index}.docs.${docIndex}.issueLocation`] },
          homeTown: { path: [`buyers.${index}.homeTown`] },
          homeState: { path: [`buyers.${index}.homeState`] },
        }}
      >
        <SectionHeader partnerColors={style}>
          {intlVerification(intl, 'documento', 'doc', intlMessages, messages, style, strMessages)}
        </SectionHeader>

        <Field>
          {({ form }) => (
            <InputField
              id="docNumber"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.docNumber}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'documento', 'docNumber', intlMessages, messages, style, strMessages)}
              partnerColors={style}
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <InputField
              id="issuedBy"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.issuedBy}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'documento', 'issuedBy', intlMessages, messages, style, strMessages)}
              partnerColors={style}
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <Select
              id="issueLocation"
              errors={form.errors}
              touched={form.touched}
              options={listUfs}
              handleBlur={form.handleBlur}
              value={form.values.issueLocation}
              handleChange={(v) => form.setFieldValue('issueLocation', v)}
              maxHeight="130px"
              readOnly
              placeholder={intlVerification(intl, 'documento', 'issueLocation', intlMessages, messages, style, strMessages)}
              keyPrefix="issueLocation-"
              noUp
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <DatePicker
              id="issueDate"
              type="text"
              mask={dateMask}
              handleChange={(v) => form.setFieldValue('issueDate', v)}
              handleBlur={() => form.setFieldTouched('issueDate', true)}
              value={form.values.issueDate}
              errors={form.errors}
              touched={form.touched}
              displayFormat="DD/MM/YYYY"
              valueFormat="YYYY-MM-DD"
              placeholder={intlVerification(intl, 'documento', 'issueDate', intlMessages, messages, style, strMessages)}
              iconColour={style.primaryBackgroundColorButton}
            />
          )}
        </Field>

        <FieldArray
          name="home"
          render={({ form }) => (
            <div>
              <SectionHeader partnerColors={style}>
                {intlVerification(intl, 'documento', 'home', intlMessages, messages, style, strMessages)}
              </SectionHeader>
              <Localization
                dictionary={dictionary}
                values={form.values}
                handleBlur={form.handleBlur}
                setFieldValue={form.setFieldValue}
                setValues={form.setValues}
                handleChange={form.handleChange}
                errors={form.errors}
                touched={form.touched}
                labelCity="homeTown"
                labelState="homeState"
                messages={messages}
                intl={intl}
              />
            </div>
          )}
        />
        {children}
      </FormStructure>
    </WLTheme>
  );
}

Documento.propTypes = {
  /**
* Continuação do formulário, caso o usuário queira adicionar algum HTML entre o último input
* e os botões.<br>
* Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
*/
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /**
  * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
  */
  validationSchema: PropTypes.func,
  /**
  * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
  * <ul>
  * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
  * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
  * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
  * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
  * <ul>
  */
  onSubmit: PropTypes.func.isRequired,
  /**
  * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
  */
  onBack: PropTypes.func,
  /**
  * Flag que se true dá ao botão submit o atributo disabled.
  */
  disabledSubmit: PropTypes.bool,
  /**
  * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
  */
  style: PropTypes.object.isRequired,
  /**
  * Objeto contendo as mensagens a serem substituídas.&lt;br>
  * Verificar Documento/defaultMessages.js para ver quais as mensagens que podem ser substituídas
  */
  messages: PropTypes.object,
  /**
  * Objeto contendo as mensagens de erro a serem substituídas.&lt;/br>
  * Verificar utils/validationMessages.js para ver quais as mensagens que podem ser substituídas
  */
  validationMessages: PropTypes.object,
  /**
  * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.finalidade.{campo}'
  */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
  * Flag que quando true, após a confirmação do usuário, envia os dados do lead
  * atualizados para a API da credihome.
  * Se estiver true, leadData e axios são obrigatórios.
  */
  sendAPI: PropTypes.bool,
  /**
  * Dados do lead a serem atualizados, deve seguir o padrão da credihome se sendAPI for true.<br>
  * <i>Obrigatório quando sendAPI ou prepareData são true<./i>
  */
  leadData: PropTypes.object,
  /**
  * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
  * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
  */
  axios: PropTypes.func,
  /**
  * Função que recebe um erro caso a chamada da API retorne erro.
  */
  setError: PropTypes.func,
  /**
  * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
  * de rodar tudo que deveria após a submissão.
  */
  setSubmitting: PropTypes.func,
  /**
   * Produto escolhido pelo usuário, se for null ele será igual a leadData.lastSimulation.type
   */
  product: PropTypes.string,
  /**
  * Posição do proponente que está sendo alterado.
  */
  index: PropTypes.string.isRequired,
};

Documento.defaultProps = {
  validationSchema: null,
  validationMessages: null,
  intl: null,
  messages: null,
  product: null,
  onBack: null,
  disabledSubmit: false,
  children: null,
  sendAPI: null,
  prepareData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
  leadData: null,
};

export default Documento;
