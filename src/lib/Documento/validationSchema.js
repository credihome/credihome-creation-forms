import * as Yup from 'yup';
import utils from '../../utils/utils';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from '../../utils/validationMessages';

const validationSchema = (intl, validationMessages, buyers, index) => {
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidRG = intlVerification(intl, 'validation', 'ValidationInvalidRG', intlMessages, validationMessages, {}, strMessages);
  const msgDuplicateRG = intlVerification(intl, 'validation', 'ValidationDuplicateRG', intlMessages, validationMessages, {}, strMessages);

  function uniqueRg(rg) {
    // Não transformar em arrow, se não não passa contexto
    const location = this.resolve(Yup.ref('issueLocation'));
    const sameRg = buyers.find((item, i) => {
      const otherRg = item.docs?.find((doc) => doc.type === 'rg');
      return (
        otherRg
        && utils.unmaskWithLetters(otherRg?.docNumber)
        === utils.unmaskWithLetters(rg)
        && `${index}` !== `${i}`
        && location === otherRg?.issueLocation
      );
    });
    return sameRg === undefined;
  }
  return Yup.object().shape({
    docNumber: Yup.string()
      .matches(/\d{1,}/g, msgInvalidRG)
      .required(msgRequiredField)
      .test('valid_unique_Rg', msgDuplicateRG, uniqueRg),
    issuedBy: Yup.string().required(msgRequiredField),
    issueDate: Yup.string().required(msgRequiredField),
    issueLocation: Yup.string().required(msgRequiredField),
    homeTown: Yup.string().required(msgRequiredField),
    homeState: Yup.string().required(msgRequiredField),
  });
};

export default validationSchema;
