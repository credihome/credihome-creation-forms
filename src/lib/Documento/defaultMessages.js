import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.Documento';

const intlMessages = defineMessages({
  primaryBuyer: {
    id: `${scope}.primaryBuyer`,
    defaultMessage: 'Informe os dados do primeiro comprador',
  },
  primaryProponent: {
    id: `${scope}.primaryProponent`,
    defaultMessage: 'Informe os dados do primeiro proponente',
  },
  othersBuyers: { 
    id: `${scope}.othersBuyers`,
    defaultMessage: 'Informe os dados do {index}º comprador',
  },
  othersProponents: {
    id: `${scope}.othersProponents`,
    defaultMessage: 'Informe os dados do {index}º proponente',
  },
  issueLocation: {
    id: `${scope}.issueLocation`,
    defaultMessage: 'UF',
  },
  issuedBy: {
    id: `${scope}.issuedBy`,
    defaultMessage: 'Órgão Expedidor',
  },
  docNumber: {
    id: `${scope}.docNumber`,
    defaultMessage: 'RG',
  },
  issueDate: {
    id: `${scope}.issueDate`,
    defaultMessage: 'Data de Expedição',
  },
  doc: {
    id: `${scope}.doc`,
    defaultMessage: 'Documento de identificação',
  },
  home: {
    id: `${scope}.home`,
    defaultMessage: 'Naturalidade',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.Documento.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Documento
* @alias Mensagens de Documento
* @const
* @type {Object}
* @example
* const messages = {
  primaryBuyer: 'Informe os dados do primeiro comprador',
  primaryProponent: 'Informe os dados do primeiro comprador',
  othersBuyers: (val) => `Informe os dados do ${val.index}º comprador`,
  othersProponents: (val) => `Informe os dados do ${val.index}º proponente`,
  issueLocation: 'UF',
  issuedBy: 'Órgão Expedidor',
  docNumber: 'RG',
  issueDate: 'Data de Expedição',
  doc: 'Documento de identificação',
  home: 'Naturalidade',
  submit: 'Continuar',
  back: 'Voltar',
};
*/
const strMessages = {
  primaryBuyer: 'Informe os dados do primeiro comprador',
  primaryProponent: 'Informe os dados do primeiro comprador',
  othersBuyers: (val) => `Informe os dados do ${val.index}º comprador`,
  othersProponents: (val) => `Informe os dados do ${val.index}º proponente`,
  issueLocation: 'UF',
  issuedBy: 'Órgão Expedidor',
  docNumber: 'RG',
  issueDate: 'Data de Expedição',
  doc: 'Documento de identificação',
  home: 'Naturalidade',
  submit: 'Continuar',
  back: 'Voltar',
};

export { scope, intlMessages, strMessages };
