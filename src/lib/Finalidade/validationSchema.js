import * as Yup from 'yup';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from '../../utils/validationMessages';

const validationSchema = (intl, validationMessages) => {
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);

  return Yup.object().shape({
    finality: Yup.array()
      .of(Yup.string())
      .min(1, msgRequiredField),
    debt: Yup.string().when('finality', (t, schema) => (t.find((item) => item === 'debt')
      ? schema.required(msgRequiredField)
      : schema.nullable(true))),
  });
};
export default validationSchema;
