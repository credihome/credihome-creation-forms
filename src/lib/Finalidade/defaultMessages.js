import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.finalidade';

const intlMessages = defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Qual a finalidade do empréstimo?',
  },
  debt: {
    id: `${scope}.debt`,
    defaultMessage: 'Qual o valor aproximado da sua dívida?',
  },
  debtPlaceholder: {
    id: `${scope}.debtPlaceholder`,
    defaultMessage: 'R$ 0,00',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.finalidade.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Finalidade
* @alias Mensagens de Finalidade
* @const
* @type {Object}
* @example
* const messages = {
  title: 'Qual a finalidade do empréstimo?',
  debt: 'Qual o valor aproximado da sua dívida?',
  debtPlaceholder: 'R$ 0,00',
  submit: 'Continuar',
  back: 'Voltar',
};
*/
const strMessages = {
  title: 'Qual a finalidade do empréstimo?',
  debt: 'Qual o valor aproximado da sua dívida?',
  debtPlaceholder: 'R$ 0,00',
  submit: 'Continuar',
  back: 'Voltar',
};

export { scope, intlMessages, strMessages };
