import * as Yup from 'yup';
import dayjs from 'dayjs';
import utils from '../../utils/utils';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from '../../utils/validationMessages';

const validationSchema = (intl, validationMessages) => {
  const msgMinAge = intlVerification(intl, 'validation', 'ValidationMinAge', intlMessages, validationMessages, {}, strMessages);
  const msgMaxAge = intlVerification(intl, 'validation', 'ValidationMaxAge', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidDate = intlVerification(intl, 'validation', 'ValidationInvalidDate', intlMessages, validationMessages, {}, strMessages);
  const msgRequiredField = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidEmail = intlVerification(intl, 'validation', 'ValidationInvalidEmail', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidCPF = intlVerification(intl, 'validation', 'ValidationInvalidCPF', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidPhone = intlVerification(intl, 'validation', 'ValidationInvalidPhone', intlMessages, validationMessages, {}, strMessages);
  const msgFullName = intlVerification(intl, 'validation', 'ValidationFullname', intlMessages, validationMessages, {}, strMessages);

  const ageMarginMin = dayjs()
    .subtract(18, 'year')
    .format('YYYY-MM-DD');

  const ageMarginMax = dayjs()
    .subtract(80, 'year')
    .format('YYYY-MM-DD');

  return Yup.object().shape({
    dateOfBirth: utils.customYupSchemas
      .date('YYYY-MM-DD')
      .required(msgInvalidDate)
      .max(ageMarginMin, msgMinAge)
      .min(ageMarginMax, msgMaxAge),
    name: Yup.string()
      .required(msgRequiredField)
      .test('fullname', msgFullName, (value) => utils.isFullNameValid(value)),
    email: Yup.string()
      .required(msgRequiredField)
      .email(msgInvalidEmail)
      .max(255),
    cpf: utils.customYupSchemas
      .masked()
      .required(msgRequiredField)
      .min(11, msgInvalidCPF)
      .test('valid_cpf', msgInvalidCPF, utils.isCPFValid),
    gender: Yup.string()
      .required(msgRequiredField),
    phone: utils.customYupSchemas
      .masked()
      .required(msgRequiredField)
      .min(11, msgInvalidPhone),
  });
};

export default validationSchema;
