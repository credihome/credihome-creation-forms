import { defineMessages } from "react-intl";

const scope = "credihomeForms.contato";

const intlMessages = defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage:
      "Precisamos saber alguns dados para fazer uma simulação personalizada"
  },
  name: {
    id: `${scope}.name`,
    defaultMessage: "Nome completo"
  },
  cpf: {
    id: `${scope}.cpf`,
    defaultMessage: "CPF"
  },
  dateOfBirth: {
    id: `${scope}.dateOfBirth`,
    defaultMessage: "Data de Nascimento"
  },
  phone: {
    id: `${scope}.phone`,
    defaultMessage: "Celular"
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: "Email"
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: "Continuar"
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: "Voltar"
  },
  gender: {
    id: `${scope}.gender`,
    defaultMessage: "Sexo"
  }
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.contato.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Contato
* @alias Mensagens de Contato
* @const
* @type {Object}
* @example
* const messages = {
  title: 'Precisamos saber alguns dados para fazer uma simulação personalizada',
  name: 'Nome completo',
  cpf: 'CPF',
  dateOfBirth: 'Data de Nascimento',
  phone: 'Celular',
  email: 'Email',
  submit: 'Continuar',
  back: 'Voltar',
};
*/
const strMessages = {
  title: "Precisamos saber alguns dados para fazer uma simulação personalizada",
  name: "Nome completo",
  cpf: "CPF",
  dateOfBirth: "Data de Nascimento",
  phone: "Celular",
  email: "Email",
  gender: "Sexo",
  submit: "Continuar",
  back: "Voltar"
};

export { scope, intlMessages, strMessages };
