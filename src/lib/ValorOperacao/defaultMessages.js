import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.valorOperacao';

const intlMessages = defineMessages({
  chooseProduct: {
    id: `${scope}.chooseProduct`,
    defaultMessage: 'Simule agora o seu crédito',
  },
  propertyFinancing: {
    id: `${scope}.propertyFinancing`,
    defaultMessage: 'Financiamento imobiliário',
  },
  homeEquity: {
    id: `${scope}.homeEquity`,
    defaultMessage: 'Crédito com garantia imobiliária',
  },
  type: {
    id: `${scope}.type`,
    defaultMessage: 'Escolha um produto',
  },
  realtyValue: {
    id: `${scope}.realtyValue`,
    defaultMessage: 'Qual o valor do Imóvel?',
  },
  mortgageValue: {
    id: `${scope}.realtyValue`,
    defaultMessage: 'Quanto você precisa?',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.valorOperacao.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof ValorOperacao
* @alias Mensagens de ValorOperacao
* @const
* @type {Object}
* @example
* const messages = {
  chooseProduct: 'Simule agora o seu crédito',
  propertyFinancing: 'Financiamento imobiliário',
  homeEquity: 'Crédito com garantia imobiliária',
  realtyValue: 'Qual o valor do Imóvel?',
  mortgageValue: 'Quanto você precisa?',
  submit: 'Continuar',
};
*/
const strMessages = {
  chooseProduct: 'Simule agora o seu crédito',
  propertyFinancing: 'Financiamento imobiliário',
  homeEquity: 'Crédito com garantia imobiliária',
  type: 'Escolha um produto',
  realtyValue: 'Qual o valor do Imóvel?',
  mortgageValue: 'Quanto você precisa?',
  submit: 'Continuar',
};

export { scope, intlMessages, strMessages };
