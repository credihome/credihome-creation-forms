import * as Yup from 'yup';
import utils from '../../utils/utils';
import { intlMessages, strMessages } from '../../utils/validationMessages';
import rolesSimulation from '../../config/rolesSimulation';
import intlVerification from '../../utils/intlVerification';

const convertReal = (value) => new Intl.NumberFormat('pt-BR', {
  style: 'currency',
  currency: 'BRL',
  minimumFractionDigits: 2,
}).format(value);

const validationSchema = (intl, validationMessages, limitsSimulation = rolesSimulation) => {
  if (!limitsSimulation || Object.keys(limitsSimulation).length === 0) {
    limitsSimulation = rolesSimulation;
  }
  const msgOptionsRequired = intlVerification(intl, 'validation', 'ValidationOptionsRequired', intlMessages, validationMessages, {}, strMessages);
  const msgFieldRequired = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);
  const msgPriceMin = intlVerification(intl, 'validation', 'ValidationRealtyPriceMinimum', intlMessages, validationMessages, {}, strMessages);
  const msgPriceMax = intlVerification(intl, 'validation', 'ValidationRealtyPriceMaximum', intlMessages, validationMessages, {}, strMessages);
  const msgMinMortgageValue = intlVerification(intl, 'validation', 'ValidationMinimumMortgageValue', intlMessages, validationMessages, {}, strMessages);
  const msgOverLTV = (val) => intlVerification(intl, 'validation', 'ValidationHomeEquityOverLTV', intlMessages, validationMessages, {}, strMessages, { percentage: val });

  const isOver = (value, number) => Number(value) >= number;
  const isOverMax = (value, number) => Number(value) <= number;

  const testsDefinitions = {
    minMaxRealtyPrice: {
      home_equity: (schema) => schema
        .test(
          'is_valid_realtyPrice_min',
          `${msgPriceMin} ${convertReal(
            limitsSimulation.home_equity.realtyPrice.min,
          )}`,
          (value) => isOver(value, limitsSimulation.home_equity.realtyPrice.min),
        )
        .test(
          'is_valid_realtyPrice_max',
          `${msgPriceMax} ${convertReal(
            limitsSimulation.home_equity.realtyPrice.max,
          )}`,
          (value) => isOverMax(value, limitsSimulation.home_equity.realtyPrice.max),
        ),
      mortgage: (schema) => schema
        .test(
          'is_valid_realtyPrice_min',
          `${msgPriceMin} ${convertReal(
            limitsSimulation.mortgage.realtyPrice.min,
          )}`,
          (value) => isOver(value, limitsSimulation.mortgage.realtyPrice.min),
        )
        .test(
          'is_valid_realtyPrice_max',
          `${msgPriceMax} ${convertReal(
            limitsSimulation.mortgage.realtyPrice.max,
          )}`,
          (value) => isOverMax(value, limitsSimulation.mortgage.realtyPrice.max),
        ),
    },

    minMortgageValue: {
      home_equity: (schema) => schema.test(
        'homeequity',
        `${msgMinMortgageValue} ${convertReal(
          limitsSimulation.home_equity.mortgageValue.min,
        )}`,
        (value) => isOver(value, limitsSimulation.home_equity.mortgageValue.min),
      ),
      mortgage: (schema) => schema.test(
        'mortgage',
        `${msgMinMortgageValue} ${convertReal(
          limitsSimulation.mortgage.mortgageValue.min,
        )}`,
        (value) => isOver(value, limitsSimulation.mortgage.mortgageValue.min),
      ),
    },

    maxLTV: {
      home_equity: (realtyPrice, schema) => schema.test(
        'homeequity_ltv',
        msgOverLTV(`${limitsSimulation.home_equity.mortgageValue.max * 100}%`),
        (value) => Number(value)
            <= limitsSimulation.home_equity.mortgageValue.max * realtyPrice,
      ),
      mortgage: (realtyPrice, schema) => schema.test(
        'mortgage_ltv',
        msgOverLTV(`${limitsSimulation.mortgage.mortgageValue.max * 100}%`),
        (value) => Number(value)
            <= limitsSimulation.mortgage.mortgageValue.max * realtyPrice,
      ),
    },
  };

  return Yup.object().shape({
    type: Yup.string()
      .nullable()
      .required(msgOptionsRequired),
    realtyPrice: utils.customYupSchemas
      .moneyMasked()
      .nullable()
      .required(msgFieldRequired)
      .when('type', (type, schema) => testsDefinitions.minMaxRealtyPrice[type || 'mortgage'](schema)),
    mortgageValue: utils.customYupSchemas
      .moneyMasked()
      .nullable()
      .required(msgFieldRequired)
      .when('type', (type, schema) => testsDefinitions.minMortgageValue[type || 'mortgage'](schema))
      .when(['realtyPrice', 'type'], (realtyPrice, type, schema) => (realtyPrice
        ? testsDefinitions.maxLTV[type || 'mortgage'](realtyPrice, schema)
        : schema)),
  });
};

export default validationSchema;
