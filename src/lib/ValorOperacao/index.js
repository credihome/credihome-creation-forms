/* eslint-disable import/no-named-default, import/no-unresolved */
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { WLTheme, Select } from 'credihome-react-library';
import styled from 'styled-components';
import FormStructure from '../FormStructure';
import { InputField } from '../../components/Input/InputField';
import { default as defaultValidation } from './validationSchema';
import { intlMessages, strMessages } from './defaultMessages';
import intlVerification from '../../utils/intlVerification';
import { numberUnmask } from '../../utils/masks';

const BoldSelect = styled(Select)`
  & input {
    font-weight: bold;
  }
`;

/**
 * Formulário de Início do WL.
*
* @category Formulários - Simulação
* @component
*/

function ValorOperacao(props) {
  const {
    children,
    initialValues,
    validationSchema,
    onSubmit,
    onBack,
    disabledSubmit,
    limitsSimulation,
    style,
    messages,
    validationMessages,
    intl,
    sendAPI,
    prepareData,
    leadData,
    axios,
    setError,
    setSubmitting,
    unmask,
  } = props;

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={({
          type: undefined,
          realtyPrice: undefined,
          mortgageValue: undefined,
          ...initialValues,
        })}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages) : defaultValidation(intl, validationMessages, limitsSimulation)}
        onSubmit={(values, sendData, res) => (unmask ? onSubmit({ ...values, realtyPrice: numberUnmask(values.realtyPrice), mortgageValue: numberUnmask(values.mortgageValue) }, sendData, res) : onSubmit(values, sendData, res))}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'valorOperacao', 'submit', intlMessages, messages, style, strMessages)}
        textSubtitle={intlVerification(intl, 'valorOperacao', 'chooseProduct', intlMessages, messages, style, strMessages)}
        disabledSubmit={disabledSubmit}
        style={style}
        alignSubtitle="center"
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        leadDataManipulation={{
          type: { path: ['lastSimulation.type', 'simulation.type'] },
          realtyPrice: { path: ['lastSimulation.realtyPrice', 'simulation.realtyPrice'], unmask: numberUnmask },
          mortgageValue: { path: ['lastSimulation.mortgageValue', 'simulation.mortgageValue'], unmask: numberUnmask },
        }}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
      >
        <Field>
          {({ form }) => (
            <BoldSelect
              id="type"
              options={[
                {
                  label: intlVerification(intl, 'valorOperacao', 'propertyFinancing', intlMessages, messages, style, strMessages),
                  key: 'mortgage',
                },
                {
                  label: intlVerification(intl, 'valorOperacao', 'homeEquity', intlMessages, messages, style, strMessages),
                  key: 'home_equity',
                },
              ]}
              value={form.values.type}
              handleBlur={form.handleBlur}
              handleFocus={form.handleFocus}
              handleChange={(v) => {
                form.setFieldValue('type', v);
              }}
              placeholder={intlVerification(intl, 'valorOperacao', 'type', intlMessages, messages, style, strMessages)}
              readOnly
              errors={form.errors}
              touched={form.touched}
              noUp
            />
          )}
        </Field>
        <Field>
          {({ form }) => (
            <InputField
              id="realtyPrice"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.realtyPrice}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'valorOperacao', 'realtyValue', intlMessages, messages, style, strMessages)}
              maskmoney
              partnerColors={style}
            />
          )}
        </Field>
        <Field>
          {({ form }) => (
            <InputField
              id="mortgageValue"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.mortgageValue}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'valorOperacao', 'mortgageValue', intlMessages, messages, style, strMessages)}
              maskmoney
              partnerColors={style}
            />
          )}
        </Field>
        {children}
      </FormStructure>
    </WLTheme>
  );
}

ValorOperacao.propTypes = {
  /**
 * Objeto contendo os valores que devem ser utilizados ao fazer a validação.<br>
 * <i>Verificar o 'Exemplo de limitsSimulation' para ver um exemplo do objeto.</i>
 */
  limitsSimulation: PropTypes.object,
  /**
 * Continuação do formulário, caso o usuário queira adicionar algum HTML entre o último input
 * e os botões.<br>
 * Usar o componente <Field /> do formik para acessar os parâmetros do formulário.
 */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  /**
 * Valores iniciais do formulário se ele não começa vazio,
 * se entrar valores diferentes dos alterados no formulário eles vão ser retornados no submit também.
 */
  initialValues: PropTypes.object,
  /**
 * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
 */
  validationSchema: PropTypes.func,
  /**
 * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
 * <ul>
 * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
 * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
 * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
 * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
 * <ul>
 */
  onSubmit: PropTypes.func.isRequired,
  /**
 * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
 */
  onBack: PropTypes.func,
  /**
 * Flag que se true dá ao botão submit o atributo disabled.
 */
  disabledSubmit: PropTypes.bool,
  /**
 * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
 */
  style: PropTypes.object.isRequired,
  /**
 * Objeto contendo as mensagens a serem substituídas.<br>
 * <i>Verificar o 'Mensagens de ValorOperacao' para ver quais as mensagens que podem ser substituídas.</i>
 */
  messages: PropTypes.object,
  /**
 * Objeto contendo as mensagens de erro a serem substituídas.</br>
 * <i>Verificar o 'Mensagens de Validação' para ver quais as mensagens que podem ser substituídas.</i>
 */
  validationMessages: PropTypes.object,
  /**
 * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.valorOperacao.{campo}'.
 * <i>Verificar o 'Mensagens de ValorOperacao' para ver quais as mensagens que podem ser substituídas.</i>
 */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
 * Flag que quando true, após a confirmação do usuário, envia os dados do lead
 * atualizados para a API da credihome.
 ** Se estiver true, leadData e axios são obrigatórios.
 */
  sendAPI: PropTypes.bool,
  /**
 * Dados do lead a serem atualizados, deve seguir o padrão da credihome se sendAPI for true.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  leadData: PropTypes.object,
  /**
 * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  axios: PropTypes.func,
  /**
 * Função que recebe um erro caso a chamada da API retorne erro.
 */
  setError: PropTypes.func,
  /**
 * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
 * de rodar tudo que deveria após a submissão.
 */
  setSubmitting: PropTypes.func,
  /**
  * Flag que quando true passa os dados de onSubmit sem máscara.
  */
  unmask: PropTypes.bool,
};

ValorOperacao.defaultProps = {
  children: null,
  initialValues: {},
  onBack: null,
  validationSchema: null,
  limitsSimulation: null,
  disabledSubmit: false,
  messages: null,
  validationMessages: null,
  intl: null,
  sendAPI: null,
  prepareData: null,
  leadData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
  unmask: null,
};

export default ValorOperacao;
