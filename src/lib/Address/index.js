import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import api from '../../api';
import { zipcodeMask } from '../../utils/masks';
import { InputField } from '../../components/Input/InputField';
import intlVerification from '../../utils/intlVerification';
import { intlMessages, strMessages } from './defaultMessages';

const AlignActions = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${(props) => (props.mobile ? 'center' : 'space-between')};
  margin: ${(props) => (props.mobile ? '16px 2em 26px 2em' : '0px 0px 20px')};
  & div {
    display: flex;
    & > a {
      text-decoration-line: underline;
      color: ${(props) => props.style?.primaryTextColor || 'white'};
      font-size: 12px;
    }
  }
`;

export function Address({
  dictionary,
  values,
  handleBlur,
  handleChange,
  setValues,
  errors,
  touched,
  messages,
  disabled,
  intl,
  setFieldError,
  setFieldTouched,
  style,
}) {
  const [validCep, setCepValid] = useState(false);
  const [LoadCep, setLoadCep] = useState(false);
  if (!dictionary) return null;

  const handleCep = (event) => {
    const cep = event.target.value.replace(/_/g, '').replace(/-/g, '');
    if (event.type === 'change') handleChange(event);
    if (
      cep.length === 8
      && (event.type === 'change'
        || (event.type === 'blur' && values.zipcode !== cep))
    ) {
      setLoadCep(true);
      api
        .getZipcode(cep)
        .then((ret) => {
          const {
            singleCep,
            streetName,
            streetType,
            neighborhood,
            ...address
          } = ret;
          setValues({
            ...values,
            ...address,
            cep_unico: singleCep,
            streetName: singleCep ? '' : `${streetType} ${streetName}`,
            neighborhood: singleCep ? '' : neighborhood,
            zipcode: cep,
          });
          setCepValid(true);
          setLoadCep(false);
        })
        .catch(() => {
          setLoadCep(false);
          setCepValid(false);
          setValues(
            {
              ...values,
              zipcode: cep,
              streetName: undefined,
              number: undefined,
              complement: undefined,
              neighborhood: undefined,
              city: undefined,
              uf: undefined,
            },
            false,
          );
          setFieldTouched('zipcode', true, false);
          setFieldError('zipcode', 'Cep não encontrado');
        });
    }
  };

  const cepLink = 'http://www.buscacep.correios.com.br/sistemas/buscacep/';

  return (
    <div>
      <InputField
        id="zipcode"
        type="text"
        mask={zipcodeMask}
        onChange={handleCep}
        onBlur={handleCep}
        value={values.zipcode}
        errors={errors}
        touched={touched}
        placeHolder={intlVerification(intl, 'address', 'zipcode', intlMessages, messages, style, strMessages)}
        disabled={disabled}
        partnerColors={style}
      />
      <AlignActions style={style}>
        <div>
          <a href={cepLink} target="_blank">
            {intlVerification(intl, 'address', 'forgetZipcode', intlMessages, messages, style, strMessages)}
          </a>
        </div>
      </AlignActions>
      {LoadCep && <span>Carregando...</span>}

      {(validCep || values.uf !== undefined) && (
        <>
          <InputField
            id="streetName"
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.streetName}
            disabled={disabled || !values.cep_unico}
            errors={errors}
            touched={touched}
            placeHolder={intlVerification(intl, 'address', 'streetName', intlMessages, messages, style, strMessages)}
            partnerColors={style}
          />
          <InputField
            id="number"
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.number}
            errors={errors}
            touched={touched}
            placeHolder={intlVerification(intl, 'address', 'number', intlMessages, messages, style, strMessages)}
            disabled={disabled}
            partnerColors={style}
          />
          <InputField
            id="complement"
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.complement}
            errors={errors}
            touched={touched}
            placeHolder={intlVerification(intl, 'address', 'complement', intlMessages, messages, style, strMessages)}
            disabled={disabled}
            partnerColors={style}
          />
          <InputField
            id="neighborhood"
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.neighborhood}
            disabled={disabled || !values.cep_unico}
            errors={errors}
            touched={touched}
            placeHolder={intlVerification(intl, 'address', 'neighborhood', intlMessages, messages, style, strMessages)}
            partnerColors={style}
          />
          <InputField
            id="city"
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.city}
            disabled
            errors={errors}
            touched={touched}
            placeHolder={intlVerification(intl, 'address', 'city', intlMessages, messages, style, strMessages)}
            partnerColors={style}
          />
          <InputField
            id="uf"
            type="text"
            value={values.uf}
            disabled
            errors={errors}
            touched={touched}
            placeHolder={intlVerification(intl, 'address', 'uf', intlMessages, messages, style, strMessages)}
            partnerColors={style}
          />
        </>
      )}
    </div>
  );
}

Address.propTypes = {
  /**
  * Objeto resultado de uma chamada externa para consulta da listagem das opções dos selects.
  */
  dictionary: PropTypes.object.isRequired,
  /**
  * Função que fornece os valores de cada campo de acordo com cada ID [vide Formik].
  */
  values: PropTypes.object.isRequired,
  /**
  * Função que aciona o evento de blur no campo [vide Formik].
  */
  handleBlur: PropTypes.func.isRequired,
  /**
  * Função que insere um valor no campo [vide Formik].
  */
  setFieldValue: PropTypes.func.isRequired,
  /**
  * Função que insere o valor em multiplos campos conforme acionado [vide Formik].
  */
  setValues: PropTypes.func.isRequired,
  /**
  * Função que insere/valida um erro no campo [vide Formik].
  */
  errors: PropTypes.object.isRequired,
  /**
  * Função que insere o evento de touch no campo [vide Formik].
  */
  touched: PropTypes.object.isRequired,
  /**
  * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
  */
  style: PropTypes.object.isRequired,
  /**
  * Objeto contendo as mensagens a serem substituídas.<br>
  * <i>Verificar o 'Mensagens de Realty' para ver quais as mensagens que podem ser substituídas.</i>
  */
  messages: PropTypes.object,
};

Address.defaultProps = {
  messages: null,
}

export default Address;
