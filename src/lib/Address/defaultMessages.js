import { defineMessages } from 'react-intl';

const scope = 'app.containers.Address';

const intlMessages = defineMessages({
  forgetZipcode: {
    id: `${scope}.forgetZipcode`,
    defaultMessage: 'Não sei meu CEP',
  },
  zipcode: {
    id: `${scope}.zipcode`,
    defaultMessage: 'CEP',
  },
  neighborhood: {
    id: `${scope}.neighborhood`,
    defaultMessage: 'Bairro',
  },
  number: {
    id: `${scope}.number`,
    defaultMessage: 'Número',
  },
  complement: {
    id: `${scope}.complement`,
    defaultMessage: 'Complemento',
  },
  uf: {
    id: `${scope}.uf`,
    defaultMessage: 'UF',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'Cidade',
  },
  localizationState: {
    id: `${scope}.localizationState`,
    defaultMessage: 'UF',
  },
  localizationCity: {
    id: `${scope}.localizationCity`,
    defaultMessage: 'Cidade',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.address.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof Address
* @alias Mensagens de Address
* @const
* @type {Object}
* @example
*const strMessages = {
  zipcode: 'CEP',
  neighborhood: 'Bairro',
  number: 'Número',
  complement: 'Complemento',
  uf: 'UF',
  city: 'Cidade',
  localizationState: 'UF',
  localizationCity: 'Cidade',
  forgetZipcode: 'Não sei meu CEP',
};
*/

const strMessages = {
  zipcode: 'CEP',
  neighborhood: 'Bairro',
  number: 'Número',
  complement: 'Complemento',
  uf: 'UF',
  city: 'Cidade',
  localizationState: 'UF',
  localizationCity: 'Cidade',
  forgetZipcode: 'Não sei meu CEP',
};

export { scope, intlMessages, strMessages };
