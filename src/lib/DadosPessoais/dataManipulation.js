import { phoneUnmask } from '../../utils/masks';

export default function spouseDataManipulation(leadData, values, index, spouseIndex, mobileIndex, residentialIndex) {
  let dataManipObj = {
    name: { path: [`buyers.${index}.name`] },
    cpf: { path: [`buyers.${index}.cpf`], unmask: (value) => phoneUnmask(value) },
    gender: { path: [`buyers.${index}.gender`] },
    email: { path: [`buyers.${index}.email`] },
    phone: { path: [`buyers.${index}.phones.${mobileIndex}.number`], unmask: (value) => phoneUnmask(value) },
    dateOfBirth: { path: [`buyers.${index}.dateOfBirth`] },
    residentialPhone: { path: [`buyers.${index}.phones.${residentialIndex}.number`], unmask: (value) => phoneUnmask(value) },
    maritalStatus: { path: [`buyers.${index}.maritalStatus`] },
    stableUnion: { path: [`buyers.${index}.stableUnion`] },
    stableUnionDate: { path: [`buyers.${index}.stableUnionDate`] },
    marriageSystem: { path: [`buyers.${index}.marriageSystem`] },
    weddingDate: { path: [`buyers.${index}.weddingDate`] },
    homeCountry: { path: [`buyers.${index}.homeCountry`] },
    mothersName: { path: [`buyers.${index}.mothersName`] },
    spouseName: { path: [`buyers.${index}.spouse.name`] },
    spouseCpf: { path: [`buyers.${index}.spouse.cpf`], unmask: (value) => phoneUnmask(value) },
    spouseGender: { path: [`buyers.${index}.spouse.gender`] },
    spouseDateOfBirth: { path: [`buyers.${index}.spouse.dateOfBirth`] },
    spouseLiveTogether: { path: [`buyers.${index}.spouse.liveTogether`] },
  };
  let hasSpouse = false;

  if (values.stableUnion) {
    hasSpouse = true;
    if (leadData.buyers[index].maritalStatus === 'married') {
      dataManipObj = {
        ...dataManipObj,
        maritalStatus: { path: [`buyers.${index}.maritalStatus`], unmask: () => undefined },
        marriageSystem: { path: [`buyers.${index}.marriageSystem`], unmask: () => undefined },
        weddingDate: { path: [`buyers.${index}.weddingDate`], unmask: () => undefined },
      };
    }
    if (spouseIndex) {
      dataManipObj.stableUnion.path.push(`buyers.${spouseIndex}.stableUnion`);
      dataManipObj.stableUnionDate.path.push(`buyers.${spouseIndex}.stableUnionDate`);
      if (leadData.buyers[spouseIndex]?.maritalStatus === 'married') {
        dataManipObj.maritalStatus.path.push(`buyers.${spouseIndex}.maritalStatus`);
        dataManipObj.marriageSystem.path.push(`buyers.${spouseIndex}.marriageSystem`);
        dataManipObj.weddingDate.path.push(`buyers.${spouseIndex}.weddingDate`);
      }
    }
  } else {
    dataManipObj = {
      ...dataManipObj,
      stableUnion: { path: [`buyers.${index}.stableUnion`] },
      stableUnionDate: { path: [`buyers.${index}.stableUnionDate`], unmask: () => undefined },
    };
    if (spouseIndex && leadData.buyers[spouseIndex]?.stableUnion) {
      dataManipObj = {
        ...dataManipObj,
        spouseStableUnion: { path: [`buyers.${spouseIndex}.stableUnion`], unmask: () => false },
        spouseStableUnionDate: { path: [`buyers.${spouseIndex}.stableUnionDate`], unmask: () => undefined },
      };
    }
  }

  if (values.maritalStatus === 'married') {
    hasSpouse = true;
    if (spouseIndex) {
      dataManipObj.maritalStatus.path.push(`buyers.${spouseIndex}.maritalStatus`);
      dataManipObj.marriageSystem.path.push(`buyers.${spouseIndex}.marriageSystem`);
      dataManipObj.weddingDate.path.push(`buyers.${spouseIndex}.weddingDate`);
    }
    if (leadData.buyers[index].stableUnion) {
      dataManipObj = {
        ...dataManipObj,
        stableUnion: { path: [`buyers.${index}.stableUnion`], unmask: () => undefined },
        stableUnionDate: { path: [`buyers.${index}.stableUnionDate`], unmask: () => undefined },
      };
    }
    if (spouseIndex && leadData.buyers[spouseIndex]?.stableUnion) {
      dataManipObj.stableUnion.path.push(`buyers.${spouseIndex}.stableUnion`);
      dataManipObj.stableUnionDate.path.push(`buyers.${spouseIndex}.stableUnionDate`);
    }
  } else if (spouseIndex && leadData.buyers[spouseIndex]?.maritalStatus === 'married') {
    dataManipObj = {
      ...dataManipObj,
      spouseMaritalStatus: { path: [`buyers.${spouseIndex}.maritalStatus`], unmask: () => undefined },
      spouseMarriageSystem: { path: [`buyers.${spouseIndex}.marriageSystem`], unmask: () => undefined },
      spouseMeddingDate: { path: [`buyers.${spouseIndex}.weddingDate`], unmask: () => undefined },
    };
  }

  if (values.maritalStatus === 'single') {
    dataManipObj = {
      ...dataManipObj,
      maritalStatus: { path: [`buyers.${index}.maritalStatus`] },
      marriageSystem: { path: [`buyers.${index}.marriageSystem`], unmask: () => undefined },
      weddingDate: { path: [`buyers.${index}.weddingDate`], unmask: () => undefined },
    };
  }

  if (values.spouseLiveTogether) {
    dataManipObj = { ...dataManipObj, endereco: { path: [`buyers.${index}.address`], unmask: () => leadData.buyers[index].address } };
    if (spouseIndex) {
      dataManipObj.endereco.path.push(`buyers.${spouseIndex}.address`);
    }
  }

  if (spouseIndex && hasSpouse) {
    dataManipObj.name.path.push(`buyers.${spouseIndex}.spouse.name`);
    dataManipObj.cpf.path.push(`buyers.${spouseIndex}.spouse.cpf`);
    dataManipObj.dateOfBirth.path.push(`buyers.${spouseIndex}.spouse.dateOfBirth`);
    dataManipObj.spouseName.path.push(`buyers.${spouseIndex}.name`);
    dataManipObj.spouseCpf.path.push(`buyers.${spouseIndex}.cpf`);
    dataManipObj.spouseGender.path.push(`buyers.${spouseIndex}.gender`);
    dataManipObj.spouseDateOfBirth.path.push(`buyers.${spouseIndex}.dateOfBirth`);
    dataManipObj.spouseLiveTogether.path.push(`buyers.${spouseIndex}.spouse.liveTogether`);
  } else if (!hasSpouse) {
    delete dataManipObj.spouseName;
    delete dataManipObj.spouseCpf;
    delete dataManipObj.spouseGender;
    delete dataManipObj.spouseDateOfBirth;
    delete dataManipObj.spouseLiveTogether;
    dataManipObj = {
      ...dataManipObj,
      spouse: { path: [`buyers.${index}.spouse`], unmask: () => undefined },
    };
    if (leadData.buyers[index].cpf === leadData.buyers[spouseIndex]?.spouse.cpf) {
      dataManipObj = {
        ...dataManipObj,
        spouseSpouse: { path: [`buyers.${spouseIndex}.spouse`], unmask: () => undefined },
      };
    }
  }

  if (index === '0') {
    dataManipObj.name.path.push('name');
    dataManipObj.cpf.path.push('cpf');
    dataManipObj.gender.path.push('gender');
    dataManipObj.email.path.push('email');
    dataManipObj.phone.path.push('phone');
    dataManipObj.dateOfBirth.path.push('dateOfBirth');
  }

  return dataManipObj;
}
