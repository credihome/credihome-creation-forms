import * as Yup from 'yup';
import dayjs from 'dayjs';
import utils from '../../utils/utils';
import { intlMessages, strMessages } from '../../utils/validationMessages';
import intlVerification from '../../utils/intlVerification';

const validationSchema = (intl, validationMessages, othersBuyers, index) => {
  const msgFieldRequired = intlVerification(intl, 'validation', 'ValidationRequiredField', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidCPF = intlVerification(intl, 'validation', 'ValidationInvalidCPF', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidEmail = intlVerification(intl, 'validation', 'ValidationInvalidEmail', intlMessages, validationMessages, {}, strMessages);
  const msgMinAge = intlVerification(intl, 'validation', 'ValidationMinAge', intlMessages, validationMessages, {}, strMessages);
  const msgMaxAge = intlVerification(intl, 'validation', 'ValidationMaxAge', intlMessages, validationMessages, {}, strMessages);
  const msgFullName = intlVerification(intl, 'validation', 'ValidationFullname', intlMessages, validationMessages, {}, strMessages);
  const msgDuplicateCPF = intlVerification(intl, 'validation', 'ValidationDuplicateCPF', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidSpouseCPF = index === 0
    ? intlVerification(intl, 'validation', 'ValidationSameSpouseCPFUser', intlMessages, validationMessages, {}, strMessages)
    : intlVerification(intl, 'validation', 'ValidationSameSpouseCPFProponent', intlMessages, validationMessages, {}, strMessages);
  const msgSpouseHasNoSpouse = intlVerification(intl, 'validation', 'ValidationSpouseCPFHasNoSpouse', intlMessages, validationMessages, {}, strMessages);
  const msgUniqueSpouseCPF = intlVerification(intl, 'validation', 'ValidationUniqueSpouseCPF', intlMessages, validationMessages, {}, strMessages);
  const msgInvalidPhone = intlVerification(intl, 'validation', 'ValidationInvalidPhone', intlMessages, validationMessages, {}, strMessages);

  const ageMargin = dayjs()
    .subtract(18, 'year')
    .format('YYYY-MM-DD');

  const ageMarginMax = dayjs()
    .subtract(80, 'year')
    .format('YYYY-MM-DD');

  const uniqueCpf = (cpf) => {
    const sameCpf = othersBuyers.filter(
      (item, i) => utils.unmask(item.cpf) === utils.unmask(cpf) && `${index}` !== `${i}`,
    );
    return sameCpf && sameCpf.length === 0;
  };

  const spouseOfNoSpouse = (cpf) => {
    const spouse = othersBuyers.findIndex(
      (buyer) => utils.unmask(buyer.cpf) === utils.unmask(cpf),
    );
    if (spouse === -1 || othersBuyers[spouse].spouse !== undefined) {
      return true;
    }
    return index < spouse;
  };

  const uniqueSpouseCpf = (cpf) => {
    const CPF = utils.unmask(cpf);
    const sameCpfInSpouse = othersBuyers.filter(
      (buyer, _index) => utils.unmask(buyer.spouse?.cpf) === CPF && `${index}` !== `${_index}`,
    );
    return sameCpfInSpouse && sameCpfInSpouse.length === 0;
  };

  Yup.addMethod(Yup.mixed, 'equalTo', function lambdaButNotArrow(ref, message) {
    const msg = message;
    return this.test('equalTo', msg, function thisBreaksWithArrowFunctions(value) {
      const refValue = utils.unmask(this.resolve(ref));
      return !refValue || !value || value !== refValue;
    });
  });

  return Yup.object().shape({
    name: Yup.string().when((_, schema) => (!index
      ? schema.nullable(true)
      : schema
        .required(msgFieldRequired)
        .test('fullname', msgFullName, (value) => utils.isFullNameValid(value)))),
    cpf: Yup.string().when((_, schema) => (!index
      ? schema.nullable(true)
      : utils.customYupSchemas
        .masked()
        .required(msgFieldRequired)
        .min(11, msgInvalidCPF)
        .test('valid_cpf', msgInvalidCPF, utils.isCPFValid)
        .test(
          'valid_unique_Cpf',
          msgDuplicateCPF,
          uniqueCpf,
        ))),
    gender: Yup.string().required(msgFieldRequired),
    dateOfBirth: Yup.string().when((_, schema) => (!index
      ? schema.nullable(true)
      : utils.customYupSchemas
        .date()
        .max(ageMargin, msgMinAge)
        .min(ageMarginMax, msgMaxAge)
        .required(msgFieldRequired))),
    email: Yup.string().when((_, schema) => (!index
      ? schema.nullable(true)
      : schema
        .required(msgFieldRequired)
        .email(msgInvalidEmail)
        .max(255))),

    phone: Yup.string().when((_, schema) => (!index
      ? schema.nullable(true)
      : utils.customYupSchemas
        .masked()
        .required(msgFieldRequired)
        .min(10, msgInvalidPhone))),

    residentialPhone: utils.customYupSchemas
      .masked()
      .required(msgFieldRequired)
      .min(10, msgInvalidPhone),
    maritalStatus: Yup.string().required(msgFieldRequired),
    homeCountry: Yup.string().required(msgFieldRequired),
    mothersName: Yup.string().when((_, schema) => (!index
      ? schema.nullable(true)
      : schema
        .required(msgFieldRequired)
        .test('fullname', msgFullName, (value) => utils.isFullNameValid(value)))),
    stableUnion: Yup.string().when(['maritalStatus'], (maritalStatus, schema) => (maritalStatus !== 'married'
      ? schema
        .required(msgFieldRequired)
      : schema.nullable(true))),
    stableUnionDate: Yup.string().when(['maritalStatus', 'stableUnion'], (maritalStatus, stableUnion, schema) => ((maritalStatus !== 'married' && stableUnion === 'true')
      ? utils.customYupSchemas
        .date()
        .required(msgFieldRequired)
      : schema.nullable(true))),
    marriageSystem: Yup.string().when(['maritalStatus'], (maritalStatus, schema) => (maritalStatus === 'married'
      ? schema
        .required(msgFieldRequired)
      : schema.nullable(true))),
    weddingDate: Yup.string().when(['maritalStatus'], (maritalStatus, schema) => (maritalStatus === 'married'
      ? utils.customYupSchemas
        .date()
        .required(msgFieldRequired)
      : schema.nullable(true))),
    spouseName: Yup.string().when(['maritalStatus', 'stableUnion'], (maritalStatus, stableUnion, schema) => ((maritalStatus === 'married' || stableUnion === 'true')
      ? schema
        .required(msgFieldRequired)
        .test('fullname', msgFullName, (value) => utils.isFullNameValid(value))
      : schema.nullable(true))),
    spouseCpf: Yup.string().when(['maritalStatus', 'stableUnion'], (maritalStatus, stableUnion, schema) => ((maritalStatus === 'married' || stableUnion === 'true')
      ? utils.customYupSchemas
        .masked()
        .required(msgFieldRequired)
        .min(11, msgInvalidCPF)
        .test('valid_cpf', msgInvalidCPF, utils.isCPFValid)
        .equalTo(Yup.ref('cpf'), msgInvalidSpouseCPF)
        .test(
          'spouse_has_no_spouse',
          msgSpouseHasNoSpouse,
          spouseOfNoSpouse,
        )
        .test(
          'valid_unique_spouse_Cpf',
          msgUniqueSpouseCPF,
          uniqueSpouseCpf,
        )
      : schema.nullable(true))),
    spouseDateOfBirth: Yup.string().when(
      ['maritalStatus', 'stableUnion'],
      (maritalStatus, stableUnion, schema) => ((maritalStatus === 'married' || stableUnion === 'true')
        ? utils.customYupSchemas
          .date()
          .max(ageMargin, msgMinAge)
          .required(msgFieldRequired)
        : schema.nullable(true)),
    ),
    spouseLiveTogether: Yup.string().when(
      ['maritalStatus', 'stableUnion'],
      (maritalStatus, stableUnion, schema) => ((maritalStatus === 'married' || stableUnion === 'true')
        ? schema
          .required(msgFieldRequired)
        : schema.nullable(true)),
    ),
    spouseGender: Yup.string().when(
      ['maritalStatus', 'stableUnion'],
      (maritalStatus, stableUnion, schema) => ((maritalStatus === 'married' || stableUnion === 'true')
        ? schema
          .required(msgFieldRequired)
        : schema.nullable(true)),
    ),

  });
};

export default validationSchema;
