/* eslint-disable import/no-named-default, no-console, no-param-reassign */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Field, FieldArray } from 'formik';
import { WLTheme, DatePicker, Select } from 'credihome-react-library';
import { RadioRound } from '../../components/Radio';
import FormStructure from '../FormStructure';
import { InputField } from '../../components/Input/InputField';
import { LabelSmall, SectionHeader } from '../../components/Text';
import api from '../../api';
import { default as defaultValidation } from './validationSchema';
import { intlMessages, strMessages } from './defaultMessages';
import intlVerification from '../../utils/intlVerification';
import {
  phoneUnmask, cpfMask, dateMask, phoneMask,
} from '../../utils/masks';
import objectToArray from '../../utils/objectToArray';
import dataManipulation from './dataManipulation';

/**
 * Formulário de Dados Pessoais do WL.
 *
 * @category Formulários - Dados do Proponente
 * @component
 */

function DadosPessoais(props) {
  const {
    // initialValues,
    validationSchema,
    onSubmit,
    onBack,
    disabledSubmit,
    style,
    messages,
    validationMessages,
    intl,
    sendAPI,
    prepareData,
    leadData,
    axios,
    setError,
    setSubmitting,
    unmask,
    index,
    product,
  } = props;

  const { buyers } = leadData;
  const [maritalStatusItems, setMaritalStatusItems] = useState([]);
  const [marriageSystemItems, setMarriageSystemItems] = useState([]);
  const [homeCountryItems, setHomeCountryItems] = useState([]);
  const [clientsGenders, setClientsGenders] = useState([]);

  useEffect(() => {
    async function getDictionary() {
      await api.getDictionary()
        .then((res) => {
          setMaritalStatusItems(objectToArray(res.maritalStatus).map(
            (item) => ({ key: item.id, label: item.name }),
          ));
          setMarriageSystemItems(objectToArray(res.marriageSystem).map(
            (item) => ({ key: item.id, label: item.name }),
          ));
          setHomeCountryItems(objectToArray(res.homeCountry)
            .map((item) => ({
              key: item.id,
              label: item.nationality,
            }))
            .sort((a, b) => {
              if (a.key === 'Brasil') return -1;
              if (b.key === 'Brasil') return 1;
              const textA = a.label.toUpperCase();
              const textB = b.label.toUpperCase();
              if (textA < textB) {
                return -1;
              }
              if (textA > textB) {
                return 1;
              }
              return 0;
            }));
          setClientsGenders(objectToArray(res.clientsGenders).map(
            (item) => ({
              key: item.id,
              label: item.label,
              order: item.order,
            }),
          ));
        })
        .catch((err) => console.log(err));
    }
    getDictionary();
  }, []);

  const yesAndNo = [{ key: true, label: 'Sim' }, { key: false, label: 'Não' }];

  if (!leadData.buyers) {
    leadData.buyers = [{}];
  }

  if (!leadData.buyers[index]) {
    let i;
    for (i = leadData.buyers.length; i <= parseInt(index, 10); i += 1) {
      leadData.buyers.push({});
    }
  }

  if (!leadData.buyers[index].phones) {
    leadData.buyers[index].phones = [];
  }

  let mobileIndex;
  mobileIndex = leadData.buyers[index].phones.findIndex((p) => p.type === 'cellphone');
  if (mobileIndex === -1) {
    mobileIndex = leadData.buyers[index].phones.length;
    leadData.buyers[index].phones.push({ type: 'cellphone', number: '' });
  }

  let residentialIndex;
  residentialIndex = leadData.buyers[index].phones.findIndex((p) => p.type === 'residential');
  if (residentialIndex === -1) {
    residentialIndex = leadData.buyers[index].phones.length;
    leadData.buyers[index].phones.push({ type: 'residential', number: '' });
  }

  const initialValues = {
    name: index === '0' ? leadData.name : buyers[index]?.name,
    cpf: index === '0' ? leadData.cpf : buyers[index]?.cpf,
    gender: buyers[index]?.gender,
    email: index === '0' ? leadData.email : buyers[index]?.email,
    phone: index === '0' ? leadData.phone : buyers[index]?.phones[mobileIndex].number,
    dateOfBirth:
      index === '0'
        ? (leadData.dateOfBirth || '')
          .split('/')
          .reverse()
          .join('-')
        : (buyers[index]?.dateOfBirth || '')
          .split('/')
          .reverse()
          .join('-'),

    residentialPhone: buyers[index]?.phones[residentialIndex].number,
    maritalStatus: buyers[index]?.maritalStatus,
    stableUnion: buyers[index]?.stableUnion,
    stableUnionDate: buyers[index]?.stableUnionDate,
    marriageSystem: buyers[index]?.marriageSystem,
    weddingDate: buyers[index]?.weddingDate,
    homeCountry: buyers[index]?.homeCountry,
    mothersName: buyers[index]?.mothersName,
    spouseName: buyers[index]?.spouse?.name,
    spouseCpf: buyers[index]?.spouse?.cpf,
    spouseGender: buyers[index]?.spouse?.gender,
    spouseDateOfBirth: buyers[index]?.spouse?.dateOfBirth
      ? buyers[index].spouse.dateOfBirth
        .split('/')
        .reverse()
        .join('-')
      : undefined,
    spouseLiveTogether: buyers[index || 0]?.spouse?.liveTogether,
  };

  const isSpouseCPF = (cpf) => {
    const i = buyers.findIndex(
      (buyer) => phoneUnmask(buyer.spouse?.cpf) === phoneUnmask(cpf),
    );
    return i >= 0 && i !== index ? i : undefined;
  };

  const isBuyerCPF = (cpf) => {
    const i = buyers.findIndex(
      (buyer) => phoneUnmask(buyer.cpf) === phoneUnmask(cpf),
    );

    if (
      i < 0
      || (buyers[i].stableUnion === false && (i < index || i === index))
    ) {
      return undefined;
    }
    return i;
  };

  const [spouseIndex, setSpouseIndex] = useState(
    initialValues.cpf ? isSpouseCPF(initialValues.cpf) : undefined,
  );

  useEffect(() => {
    setSpouseIndex(isSpouseCPF(initialValues.cpf));
  }, [buyers]);

  // if (!dictionary) return null;

  const getMessage = (buyerIndex) => {
    if (product === 'home_equity' || leadData.lastSimulation.type === 'home_equity') {
      if (buyerIndex === '0') {
        return intlVerification(intl, 'dadosPessoais', 'primaryProponent', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
      }
      return intlVerification(intl, 'dadosPessoais', 'othersProponents', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
    }
    if (buyerIndex === '0') {
      return intlVerification(intl, 'dadosPessoais', 'primaryBuyer', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
    }
    return intlVerification(intl, 'dadosPessoais', 'othersBuyers', intlMessages, messages, style, strMessages, { buyer: parseInt(index, 10) + 1 });
  };

  return (
    <WLTheme style={style}>
      <FormStructure
        initialValues={initialValues}
        validationSchema={validationSchema ? validationSchema(intl, validationMessages, buyers, index) : defaultValidation(intl, validationMessages, buyers, index)}
        onSubmit={(values, sendData, res) => (unmask ? onSubmit({
          ...values, cpf: phoneUnmask(values.cpf), phone: phoneUnmask(values.phone), residentialPhone: phoneUnmask(values.residentialPhone), spouseCpf: phoneUnmask(values.spouseCpf),
        }, sendData, res) : onSubmit(values, sendData, res))}
        onBack={onBack}
        textSubmit={intlVerification(intl, 'dadosPessoais', 'submit', intlMessages, messages, style, strMessages)}
        textBack={intlVerification(intl, 'dadosPessoais', 'back', intlMessages, messages, style, strMessages)}
        textSubtitle={getMessage(index)}
        disabledSubmit={disabledSubmit}
        style={style}
        sendAPI={sendAPI}
        prepareData={prepareData}
        leadData={leadData}
        leadDataManipulation={(values) => dataManipulation(leadData, values, index, spouseIndex, mobileIndex, residentialIndex)}
        axios={axios}
        setError={setError}
        setSubmitting={setSubmitting}
      >
        <SectionHeader partnerColors={style}>
          {intlVerification(intl, 'dadosPessoais', 'personal', intlMessages, messages, style, strMessages)}
        </SectionHeader>
        {(index !== '0' || initialValues.cpf === undefined) && (
          <Field>
            {({ form }) => (
              <InputField
                id="cpf"
                type="text"
                mask={cpfMask}
                onChange={(event) => {
                  form.handleChange(event);
                  if (phoneUnmask(event.target.value).length === 11) {
                    const sIndex = isSpouseCPF(event.target.value);
                    setSpouseIndex(sIndex);
                    if (sIndex !== undefined) {
                      const spouse = buyers[sIndex];
                      form.setValues({
                        ...form.values,
                        cpf: spouse.spouse.cpf,
                        gender: spouse.spouse.gender,
                        name: spouse.spouse.name,
                        dateOfBirth: spouse.spouse.dateOfBirth,
                        maritalStatus:
                              spouse.maritalStatus === 'married'
                                ? spouse.maritalStatus
                                : form.values.maritalStatus,
                        stableUnion: spouse.stableUnion,
                        stableUnionDate: spouse.stableUnionDate,
                        marriageSystem:
                              spouse.maritalStatus === 'married'
                                ? spouse.marriageSystem
                                : form.values.marriageSystem,
                        weddingDate:
                              spouse.maritalStatus === 'married'
                                ? spouse.weddingDate
                                : form.values.weddingDate,
                        spouseName: spouse.name,
                        spouseCpf: spouse.cpf,
                        spouseGender: spouse.gender,
                        spouseDateOfBirth: spouse.dateOfBirth,
                        spouseLiveTogether: spouse.spouse.liveTogether,
                      });
                    } else {
                      setSpouseIndex(undefined);
                    }
                  }
                }}
                onBlur={() => {
                  form.setFieldTouched('cpf', true);
                }}
                value={form.values.cpf}
                errors={form.errors}
                touched={form.touched}
                placeHolder={intlVerification(intl, 'dadosPessoais', 'labelCPF', intlMessages, messages, style, strMessages)}
                partnerColors={style}
              />
            )}
          </Field>
        )}

        {(index !== '0' || initialValues.name === undefined) && (
          <Field>
            {({ form }) => (
              <InputField
                id="name"
                type="text"
                onChange={(event) => {
                  event.target.value = event.target.value.replace(
                    /[^A-Za-z\u00C0-\u00FF\s']/gi,
                    '',
                  );
                  form.handleChange(event);
                }}
                onBlur={(e) => {
                  form.setFieldValue('name', form.values.name?.trim());
                  form.handleBlur(e);
                }}
                value={form.values.name}
                errors={form.errors}
                touched={form.touched}
                placeHolder={intlVerification(intl, 'dadosPessoais', 'labelName', intlMessages, messages, style, strMessages)}
                disabled={spouseIndex < index}
                partnerColors={style}
              />
            )}
          </Field>
        )}

        {(index !== '0' || initialValues.gender === undefined) && (
          <Field>
            {({ form }) => (
              <Select
                id="gender"
                options={clientsGenders}
                value={form.values.gender}
                handleBlur={form.handleBlur}
                handleChange={(v) => {
                  form.setFieldValue('gender', v);
                }}
                readOnly
                placeholder={intlVerification(intl, 'dadosPessoais', 'labelGender', intlMessages, messages, style, strMessages)}
                errors={form.errors}
                touched={form.touched}
                noUp
              />
            )}
          </Field>
        )}

        {(index !== '0' || initialValues.dateOfBirth === undefined) && (
          <Field>
            {({ form }) => (
              <DatePicker
                id="dateOfBirth"
                type="text"
                mask={dateMask}
                handleChange={(v) => form.setFieldValue('dateOfBirth', v)}
                handleBlur={() => form.setFieldTouched('dateOfBirth', true)}
                value={form.values.dateOfBirth}
                errors={form.errors}
                touched={form.touched}
                displayFormat="DD/MM/YYYY"
                valueFormat="YYYY-MM-DD"
                placeholder={intlVerification(intl, 'dadosPessoais', 'labelDateOfBirth', intlMessages, messages, style, strMessages)}
                disabled={spouseIndex < index}
                partnerColors={style}
              />
            )}
          </Field>
        )}

        <Field>
          {({ form }) => (
            <Select
              id="maritalStatus"
              errors={form.errors}
              touched={form.touched}
              options={
                spouseIndex < index && buyers[spouseIndex].stableUnion
                  ? maritalStatusItems.filter((i) => i.key !== 'married')
                  : maritalStatusItems
              }
              handleBlur={form.handleBlur}
              handleChange={(v) => {
                form.setFieldValue('maritalStatus', v);
              }}
              value={form.values.maritalStatus}
              maxHeight="50vh"
              readOnly
              placeholder={intlVerification(intl, 'dadosPessoais', 'labelMaritalStatus', intlMessages, messages, style, strMessages)}
              disabled={
                spouseIndex < index
                && buyers[spouseIndex].maritalStatus === 'married'
              }
              noUp
              partnerColors={style}
            />
          )}
        </Field>

        <FieldArray
          name="DadosDoCasamento"
          render={(arrayHelpers) => (
            <div>
              {arrayHelpers.form.values.maritalStatus && arrayHelpers.form.values.maritalStatus !== 'single' && (
                <div>
                  <Field>
                    {({ form }) => (
                      <Select
                        id="marriageSystem"
                        name="marriageSystem"
                        errors={form.errors}
                        touched={form.touched}
                        options={marriageSystemItems}
                        handleBlur={form.handleBlur}
                        handleChange={(v) => {
                          form.setFieldValue('marriageSystem', v);
                        }}
                        value={form.values.marriageSystem}
                        maxHeight="50vh"
                        readOnly
                        placeholder={intlVerification(intl, 'dadosPessoais', 'labelMarriageSystem', intlMessages, messages, style, strMessages)}
                        disabled={
                          spouseIndex < index
                          && buyers[spouseIndex].maritalStatus === 'married'
                        }
                        noUp
                        partnerColors={style}
                      />
                    )}
                  </Field>
                  <Field>
                    {({ form }) => (
                      <DatePicker
                        id="weddingDate"
                        type="text"
                        mask={dateMask}
                        handleChange={(v) => form.setFieldValue('weddingDate', v)}
                        handleBlur={() => form.setFieldTouched('weddingDate', true)}
                        value={form.values.weddingDate}
                        errors={form.errors}
                        touched={form.touched}
                        displayFormat="DD/MM/YYYY"
                        valueFormat="YYYY-MM-DD"
                        placeholder={intlVerification(intl, 'dadosPessoais', 'labelWeddingDate', intlMessages, messages, style, strMessages)}
                        disabled={
                          spouseIndex < index
                          && buyers[spouseIndex].maritalStatus === 'married'
                        }
                        partnerColors={style}
                      />
                    )}
                  </Field>
                </div>
              )}
            </div>
          )}
        />

        <FieldArray
          name="UniaoEstavel"
          render={(arrayHelpers) => (
            <div>
              {((arrayHelpers.form.values.maritalStatus
                && arrayHelpers.form.values.maritalStatus !== 'married')
                || (spouseIndex < index && arrayHelpers.form.values.stableUnion === true)) && (
                  <div>
                    <Field>
                      {({ form }) => (
                        <div className="row" style={{ marginBottom: '12px' }}>
                          <LabelSmall
                            inputFor="stableUnion"
                            text={intlVerification(intl, 'dadosPessoais', 'labelStableUnion', intlMessages, messages, style, strMessages)}
                            partnerColors={style}
                          />
                          <RadioRound
                            id="stableUnion"
                            caption={intlVerification(intl, 'dadosPessoais', 'labelStableUnion', intlMessages, messages, style, strMessages)}
                            errors={form.errors}
                            touched={form.touched}
                            options={yesAndNo}
                            value={form.values.stableUnion}
                            onChangeItem={(v) => {
                              form.setFieldValue('stableUnion', v);
                            }}
                            disabled={spouseIndex < index}
                            partnerColors={style}
                          />
                        </div>
                      )}
                    </Field>
                    {arrayHelpers.form.values.stableUnion && (
                      <Field>
                        {({ form }) => (
                          <DatePicker
                            id="stableUnionDate"
                            type="text"
                            mask={dateMask}
                            handleChange={(v) => form.setFieldValue('stableUnionDate', v)}
                            handleBlur={() => form.setFieldTouched('stableUnionDate', true)}
                            value={form.values.stableUnionDate}
                            errors={form.errors}
                            touched={form.touched}
                            displayFormat="DD/MM/YYYY"
                            valueFormat="YYYY-MM-DD"
                            placeholder={intlVerification(intl, 'dadosPessoais', 'labelStableUnionDate', intlMessages, messages, style, strMessages)}
                            disabled={spouseIndex < index}
                            partnerColors={style}
                          />
                        )}
                      </Field>
                    )}
                  </div>
              )}
            </div>
          )}
        />

        {(index !== '0' || initialValues.email === undefined) && (
          <Field>
            {({ form }) => (
              <InputField
                id="email"
                type="text"
                onChange={form.handleChange}
                onBlur={form.handleBlur}
                value={form.values.email}
                errors={form.errors}
                touched={form.touched}
                placeHolder={intlVerification(intl, 'dadosPessoais', 'labelEmail', intlMessages, messages, style, strMessages)}
                partnerColors={style}
              />
            )}
          </Field>
        )}

        {(index !== '0' || initialValues.phone === undefined) && (
          <Field>
            {({ form }) => (
              <InputField
                id="phone"
                type="text"
                mask={phoneMask}
                onChange={form.handleChange}
                onBlur={form.handleBlur}
                value={form.values.phone}
                errors={form.errors}
                touched={form.touched}
                placeHolder={intlVerification(intl, 'dadosPessoais', 'labelPhone', intlMessages, messages, style, strMessages)}
                partnerColors={style}
              />
            )}
          </Field>
        )}

        <Field>
          {({ form }) => (
            <InputField
              id="residentialPhone"
              type="text"
              mask={phoneMask}
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.residentialPhone}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'dadosPessoais', 'labelResidentialPhone', intlMessages, messages, style, strMessages)}
              partnerColors={style}
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <Select
              id="homeCountry"
              errors={form.errors}
              touched={form.touched}
              options={homeCountryItems}
              handleBlur={form.handleBlur}
              handleChange={(v) => {
                form.setFieldValue('homeCountry', v);
              }}
              value={form.values.homeCountry || 'Brasil'}
              maxHeight="130px"
              readOnly
              placeholder={intlVerification(intl, 'dadosPessoais', 'labelNationality', intlMessages, messages, style, strMessages)}
              dynamicAlign={false}
              noUp
              partnerColors={style}
            />
          )}
        </Field>

        <Field>
          {({ form }) => (
            <InputField
              id="mothersName"
              type="text"
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values.mothersName}
              errors={form.errors}
              touched={form.touched}
              placeHolder={intlVerification(intl, 'dadosPessoais', 'labelMothersName', intlMessages, messages, style, strMessages)}
              partnerColors={style}
            />
          )}
        </Field>

        <FieldArray
          name="DadosParceiro"
          render={(arrayHelpers) => (
            <div>
              {(arrayHelpers.form.values.maritalStatus === 'married'
                || arrayHelpers.form.values.stableUnion === true) && (
                  <div>
                    <SectionHeader partnerColors={style}>
                      {arrayHelpers.form.values.maritalStatus === 'married'
                        ? intlVerification(intl, 'dadosPessoais', 'spouse', intlMessages, messages, style, strMessages)
                        : intlVerification(intl, 'dadosPessoais', 'partner', intlMessages, messages, style, strMessages)}
                    </SectionHeader>

                    <Field>
                      {({ form }) => (
                        <InputField
                          id="spouseName"
                          type="text"
                          onChange={form.handleChange}
                          onBlur={(e) => {
                            form.setFieldValue('spouseName', form.values.spouseName?.trim());
                            form.handleBlur(e);
                          }}
                          value={form.values.spouseName}
                          errors={form.errors}
                          touched={form.touched}
                          placeHolder={intlVerification(intl, 'dadosPessoais', 'labelName', intlMessages, messages, style, strMessages)}
                          disabled={spouseIndex < index}
                          partnerColors={style}
                        />
                      )}
                    </Field>
                    <Field>
                      {({ form }) => (
                        <InputField
                          id="spouseCpf"
                          type="text"
                          mask={cpfMask}
                          onChange={(event) => {
                            form.handleChange(event);
                            if (phoneUnmask(event.target.value).length === 11) {
                              const sIndex = isBuyerCPF(event.target.value);
                              setSpouseIndex(sIndex);
                              if (sIndex !== undefined) {
                                const spouse = buyers[sIndex];
                                form.setValues({
                                  ...form.values,
                                  spouseName: spouse.name,
                                  spouseCpf: spouse.cpf,
                                  spouseGender: spouse.gender,
                                  spouseDateOfBirth: spouse.dateOfBirth,
                                });
                              } else {
                                setSpouseIndex(undefined);
                              }
                            }
                          }}
                          onBlur={form.handleBlur}
                          value={form.values.spouseCpf}
                          errors={form.errors}
                          touched={form.touched}
                          placeHolder={intlVerification(intl, 'dadosPessoais', 'labelCPF', intlMessages, messages, style, strMessages)}
                          disabled={spouseIndex < index}
                          partnerColors={style}
                        />
                      )}
                    </Field>
                    <Field>
                      {({ form }) => (
                        <Select
                          id="spouseGender"
                          options={clientsGenders}
                          value={form.values.spouseGender}
                          handleBlur={form.handleBlur}
                          handleChange={(v) => {
                            form.setFieldValue('spouseGender', v);
                          }}
                          readOnly
                          placeholder={intlVerification(intl, 'dadosPessoais', 'labelGender', intlMessages, messages, style, strMessages)}
                          errors={form.errors}
                          touched={form.touched}
                          noUp
                        />
                      )}
                    </Field>
                    <Field>
                      {({ form }) => (
                        <DatePicker
                          id="spouseDateOfBirth"
                          type="text"
                          mask={dateMask}
                          handleChange={(v) => form.setFieldValue('spouseDateOfBirth', v)}
                          handleBlur={() => form.setFieldTouched('spouseDateOfBirth', true)}
                          value={form.values.spouseDateOfBirth}
                          errors={form.errors}
                          touched={form.touched}
                          displayFormat="DD/MM/YYYY"
                          valueFormat="YYYY-MM-DD"
                          placeholder={intlVerification(intl, 'dadosPessoais', 'labelDateOfBirth', intlMessages, messages, style, strMessages)}
                          disabled={spouseIndex < index}
                          partnerColors={style}
                        />
                      )}
                    </Field>

                    <Field>
                      {({ form }) => (
                        <div className="row" style={{ marginBottom: '12px' }}>
                          <LabelSmall
                            inputFor="spouseLiveTogether"
                            text={intlVerification(intl, 'dadosPessoais', 'labelLiveTogether', intlMessages, messages, style, strMessages)}
                            partnerColors={style}
                          />
                          <RadioRound
                            id="spouseLiveTogether"
                            name="spouseLiveTogether"
                            caption={intlVerification(intl, 'dadosPessoais', 'labelLiveTogether', intlMessages, messages, style, strMessages)}
                            errors={form.errors}
                            touched={form.touched}
                            options={yesAndNo}
                            value={form.values.spouseLiveTogether}
                            onChangeItem={(v) => {
                              form.setFieldValue('spouseLiveTogether', v);
                            }}
                            disabled={spouseIndex < index}
                            partnerColors={style}
                          />
                        </div>
                      )}
                    </Field>
                  </div>
              )}
            </div>
          )}
        />
      </FormStructure>
    </WLTheme>
  );
}

DadosPessoais.propTypes = {
  // initialValues: PropTypes.object,
  /**
 * Um objeto Yup, deve ser preenchido se não quiser utilizar a validação default.
 */
  validationSchema: PropTypes.func,
  /**
 * Função que roda quando o usuário completa o formulário, recebe parâmetros diferentes dependendo das flags settadas:
 * <ul>
 * <li>Sem nenhuma flag: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (mascarados).</li>
 * <li>unmask: onSubmit(values), onde values são os valores iniciais + os dados preenchidos pelo usuário (desmascarados).</li>
 * <li>prepareData: onSubmit(values, sendData), onde sendData são os valores passados em leadData + os dados preenchidos pelo usuário propriamente parseados pelo objeto leadDataManipulation.</li>
 * <li>sendAPI: onSubmit(values, sendData, res), onde res é a resposta do request feito para a API.</li>
 * <ul>
 */
  onSubmit: PropTypes.func.isRequired,
  /**
 * Função que roda quando o usuário pressiona o botão de back, se ela for undefined o botão de back não aparece.
 */
  onBack: PropTypes.func,
  /**
 * Flag que se true dá ao botão submit o atributo disabled.
 */
  disabledSubmit: PropTypes.bool,
  /**
 * Objeto de estilo de parceitos utilizado pela credihome, o mesmo utilizado no WL.
 */
  style: PropTypes.object.isRequired,
  /**
 * Objeto contendo as mensagens a serem substituídas.<br>
 * <i>Verificar o 'Mensagens de Contato' para ver quais as mensagens que podem ser substituídas.</i>
 */
  messages: PropTypes.object,
  /**
 * Objeto contendo as mensagens de erro a serem substituídas.</br>
 * <i>Verificar o 'Mensagens de Validação' para ver quais as mensagens que podem ser substituídas.</i>
 */
  validationMessages: PropTypes.object,
  /**
 * Instância do intl com as mensagens já settadas com o id seguindo o seguinte padrão 'credihomeForms.contato.{campo}'.
 * <i>Verificar o 'Mensagens de Contato' para ver quais as mensagens que podem ser substituídas.</i>
 */
  intl: PropTypes.object,
  /**
  * Flag que quando true, após a confirmação do usuário, preenche leadData de acordo com
  * os valores passados em leadDataManipulation.<br>
  * Se estiver true, leadData e leadDataManipulation são obrigatórios.
  */
  prepareData: PropTypes.bool,
  /**
 * Flag que quando true, após a confirmação do usuário, envia os dados do lead
 * atualizados para a API da credihome.
 * Se estiver true, leadData e axios são obrigatórios.
 */
  sendAPI: PropTypes.bool,
  /**
 * <b>Obrigatório para esse formulário, substituí o que seria initialValues nos outros forms</b>
 * Dados do lead a serem atualizados, é obrigatório há muitas regras que dependem dele no WL.<br/>
 * Coisas como ter nome, CPF, email, telefone e data de nascimento do primeiro proponente já
 * escrito, ou precisar da lista de buyers para saber se os CPF não são igauis ou se o proponente
 * é spouse de alguém.
 */
  leadData: PropTypes.object.isRequired,
  /**
 * Instância do axios com a url da API da credihome no ambiente correto + o token de autorização.<br>
 * <i>Obrigatório quando sendAPI ou prepareData são true.</i>
 */
  axios: PropTypes.func,
  /**
 * Função que recebe um erro caso a chamada da API retorne erro.
 */
  setError: PropTypes.func,
  /**
 * Função que recebe true ou false dependendo de se o formulário terminou(false) ou não(true)
 * de rodar tudo que deveria após a submissão.
 */
  setSubmitting: PropTypes.func,
  /**
  * Flag que quando true passa os dados de onSubmit sem máscara.
  */
  unmask: PropTypes.bool,
  /**
  * Posição do proponente que está sendo alterado.
  */
  index: PropTypes.string.isRequired,
  /**
  * Produto escolhido pelo usuário, se for null ele será igual a leadData.lastSimulation.type
  */
  product: PropTypes.string,
};

DadosPessoais.defaultProps = {
  // initialValues: {},
  validationSchema: null,
  onBack: null,
  disabledSubmit: false,
  messages: null,
  validationMessages: null,
  intl: null,
  sendAPI: null,
  prepareData: null,
  axios: null,
  setError: null,
  setSubmitting: null,
  unmask: null,
  product: null,
};

export default DadosPessoais;
