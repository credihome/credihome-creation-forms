import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.dadosPessoais';

const intlMessages = defineMessages({
  primaryBuyer: {
    id: `${scope}.primaryBuyer`,
    defaultMessage: 'Fale mais sobre você',
  },
  primaryProponent: {
    id: `${scope}.primaryProponent`,
    defaultMessage: 'Fale mais sobre você',
  },
  othersBuyers: {
    id: `${scope}.othersBuyers`,
    defaultMessage: 'Informe os dados do {buyer}º comprador',
  },
  othersProponents: {
    id: `${scope}.othersProponents`,
    defaultMessage: 'Informe os dados do {buyer}º proponente',
  },
  personal: {
    id: `${scope}.personal`,
    defaultMessage: 'Dados Pessoais',
  },
  spouse: {
    id: `${scope}.spouse`,
    defaultMessage: 'Dados do Cônjuge',
  },
  partner: {
    id: `${scope}.partner`,
    defaultMessage: 'Dados do Parceiro',
  },
  labelName: {
    id: `${scope}.labelName`,
    defaultMessage: 'Nome',
  },
  labelGender: {
    id: `${scope}.labelGender`,
    defaultMessage: 'Sexo',
  },
  labelCPF: {
    id: `${scope}.labelCPF`,
    defaultMessage: 'CPF',
  },
  labelDateOfBirth: {
    id: `${scope}.labelDateOfBirth`,
    defaultMessage: 'Data de Nascimento',
  },
  labelPhone: {
    id: `${scope}.labelPhone`,
    defaultMessage: 'Celular',
  },
  labelResidentialPhone: {
    id: `${scope}.labelResidentialPhone`,
    defaultMessage: 'Telefone residencial',
  },
  labelEmail: {
    id: `${scope}.labelEmail`,
    defaultMessage: 'Email',
  },
  labelMaritalStatus: {
    id: `${scope}.labelMaritalStatus`,
    defaultMessage: 'Estado civil',
  },
  labelMarriageSystem: {
    id: `${scope}.labelMarriageSystem`,
    defaultMessage: 'Regime de casamento',
  },
  labelWeddingDate: {
    id: `${scope}.labelWeddingDate`,
    defaultMessage: 'Data do casamento',
  },
  labelStableUnion: {
    id: `${scope}.labelStableUnion`,
    defaultMessage: 'Vive em união estável?',
  },
  labelStableUnionDate: {
    id: `${scope}.labelStableUnionDate`,
    defaultMessage: 'Data da união',
  },
  labelNationality: {
    id: `${scope}.labelNationality`,
    defaultMessage: 'Nacionalidade',
  },
  labelMothersName: {
    id: `${scope}.labelMothersName`,
    defaultMessage: 'Nome da mãe',
  },
  labelLiveTogether: {
    id: `${scope}.labelLiveTogether`,
    defaultMessage: 'Moram no mesmo endereço?',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'Continuar',
  },
  back: {
    id: `${scope}.back`,
    defaultMessage: 'Voltar',
  },
});

/**
* Objeto que deve ser recebido em `messages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.dadosPessoais.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @memberof DadosPessoais
* @alias Mensagens de Dados Pessoais
* @const
* @type {Object}
* @example
* const messages = {
  primaryBuyer: 'Fale mais sobre você',
  primaryProponent: 'Fale mais sobre você',
  othersBuyers: (val) => `Informe os dados do ${val.buyer}º comprador`,
  othersProponents: (val) => `Informe os dados do ${val.buyer}º proponente`,
  personal: 'Dados Pessoais',
  spouse: 'Dados do Cônjuge',
  partner: 'Dados do Parceiro',
  labelName: 'Nome',
  labelCPF: 'CPF',
  labelGender: 'Sexo',
  labelDateOfBirth: 'Data de Nascimento',
  labelPhone: 'Celular',
  labelResidentialPhone: 'Telefone residencial',
  labelEmail: 'Email',
  labelMaritalStatus: 'Estado civil',
  labelMarriageSystem: 'Regime de casamento',
  labelWeddingDate: 'Data do casamento',
  labelStableUnion: 'Vive em união estável?',
  labelStableUnionDate: 'Data da união',
  labelNationality: 'Nacionalidade',
  labelMothersName: 'Nome da mãe',
  labelLiveTogether: 'Moram no mesmo endereço?',
  submit: 'Continuar',
  back: 'Voltar',
};
*/
const strMessages = {
  primaryBuyer: 'Fale mais sobre você',
  primaryProponent: 'Fale mais sobre você',
  othersBuyers: (val) => `Informe os dados do ${val.buyer}º comprador`,
  othersProponents: (val) => `Informe os dados do ${val.buyer}º proponente`,
  personal: 'Dados Pessoais',
  spouse: 'Dados do Cônjuge',
  partner: 'Dados do Parceiro',
  labelName: 'Nome',
  labelCPF: 'CPF',
  labelGender: 'Sexo',
  labelDateOfBirth: 'Data de Nascimento',
  labelPhone: 'Celular',
  labelResidentialPhone: 'Telefone residencial',
  labelEmail: 'Email',
  labelMaritalStatus: 'Estado civil',
  labelMarriageSystem: 'Regime de casamento',
  labelWeddingDate: 'Data do casamento',
  labelStableUnion: 'Vive em união estável?',
  labelStableUnionDate: 'Data da união',
  labelNationality: 'Nacionalidade',
  labelMothersName: 'Nome da mãe',
  labelLiveTogether: 'Moram no mesmo endereço?',
  submit: 'Continuar',
  back: 'Voltar',
};

export { scope, intlMessages, strMessages };
