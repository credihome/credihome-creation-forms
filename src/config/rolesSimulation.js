/**
* Esse é o objeto default de `limitsSimulation`.
* @memberof ValorOperacao
* @alias Exemplo de limitsSimulation
* @const
* @type {Object}
* @example
* const limitsSimulation = {
  mortgage: {
    realtyPrice: {
      min: 100000,
      max: 20000000,
    },
    mortgageValue: {
      min: 60000,
      max: 0.8,
    },
    duration: {
      min: 12,
      max: 360,
    },
  },
  home_equity: {
    realtyPrice: {
      min: 200000,
      max: 20000000,
    },
    mortgageValue: {
      min: 40000,
      max: 0.6,
    },
    duration: {
      min: 12,
      max: 240,
    },
  },
};
*/
const rolesSimulation = {
  mortgage: {
    realtyPrice: {
      min: 100000,
      max: 20000000,
    },
    mortgageValue: {
      min: 60000,
      max: 0.8,
    },
    duration: {
      min: 12,
      max: 360,
    },
  },
  home_equity: {
    realtyPrice: {
      min: 200000,
      max: 20000000,
    },
    mortgageValue: {
      min: 40000,
      max: 0.6,
    },
    duration: {
      min: 12,
      max: 240,
    },
  },
};

export default rolesSimulation;
