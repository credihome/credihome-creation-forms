/* eslint import/prefer-default-export: 0 */

import FormStructure from './lib/FormStructure';
import ValorOperacao from './lib/ValorOperacao';
import Contato from './lib/Contato';
import Finalidade from './lib/Finalidade';
import Tempo from './lib/Tempo';
import DadosPessoais from './lib/DadosPessoais';
import Realty from './lib/Realty';
import Documento from './lib/Documento';
import Endereco from './lib/Endereco';
import InfoProfissional from './lib/InfoProfissional';

export {
  FormStructure, 
  ValorOperacao, 
  Contato, 
  Finalidade, 
  Tempo, 
  DadosPessoais, 
  Documento, 
  Endereco, 
  InfoProfissional, 
  Realty,
};
