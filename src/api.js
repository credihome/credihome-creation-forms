import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://apis.credihome.com.br/staging',
});

const api = {
  getDictionary: () => instance.get('/dictionary').then((response) => response.data),
  updateLead: (lead, axiosInstance) => axiosInstance.put(`/leads/${lead.leadId}`, lead).then((response) => response.data),
  getCities: (uf) => instance.get(`states/${uf}/cities`).then((response) => response.data),
  getZipcode: (zipcode) => instance.get(`zipcode/${zipcode}`).then((response) => response.data),
};

export default api;
