/* eslint-disable indent */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ContainerCheckbox = styled.label`
  position: relative;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  height: 16px;
  width: 16px;
  & > input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }
  & .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 16px;
    width: 16px;
    background: ${(props) => (props.checked ? props.partner?.primaryBackgroundColorButton : 'white')};
    border: 1px solid ${(props) => props?.partner?.primaryBackgroundColorButton || 'white'};
    &:after {
      content: '';
      position: absolute;
      display: ${(props) => (props.checked ? 'block' : 'none')};
    }
  }
  & .checkmark:after {
    content: '';
    left: 2px;
    top: 3px;
    width: 9px;
    height: 4px;
    border: solid white;
    border-width: 3px 3px 0px 0px;
    -webkit-transform: rotate(130deg);
    -ms-transform: rotate(130deg);
    transform: rotate(130deg);
  }
`;

export default function Checkbox(props) {
  const { checked, onChange, partner } = props;
  return (
    <ContainerCheckbox checked={checked} partner={partner}>
      <input type="checkbox" checked={checked} onChange={onChange} />
      <i className="checkmark" />
    </ContainerCheckbox>
  );
}

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  partner: PropTypes.object.isRequired,
};
