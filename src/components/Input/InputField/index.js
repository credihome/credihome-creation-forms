/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
/* eslint-disable indent */
/**
 *
 * InputField
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import styled from 'styled-components';
import CurrencyInput from 'react-currency-format';
import { Tooltip } from 'credihome-react-library';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';
import device from '../../../config/devices';

const styleInputs = `width: 100%;
height: 35px;
border-radius: 5px;
padding: 0 0 0 10px;
transition: 0.3s all;
outline: none;
box-sizing: border-box;
@media (${device.mobileM}) {
  height: 27px;
}
@media (${device.tablet}) {
  height: 35px;
}`;

const StyledInput = styled(MaskedInput)`
  margin-bottom: 14px;
  width: ${(props) => props.width || '100%'};
  border: 1px solid ${
  (props) => (props.borderError ? props.partnerColors?.validation.error.backgroundColor : '#707070')};
  ${styleInputs};
  &:focus {
    border: 2px solid
      ${(props) => props.partnerColors?.primaryBackgroundColorButton};
  }
  &:disabled {
    background-color: ${(props) => props.partnerColors?.backgroundColorInputDisabled
      || 'rgba(255, 255, 255, 0.5)'};
    color: ${(props) => props.partnerColors?.fontColorInputDisabled || '#545454'};
  }
`;

const MoneyInput = styled(CurrencyInput)`
margin-bottom: 14px;

  width: ${(props) => props.width || '100%'};
  border: 1px solid ${
  (props) => (props.borderError ? props.partnerColors?.validation.error.backgroundColor : '#707070')};
  ${styleInputs};
  &:focus {
    border: 2px solid
      ${(props) => props.partnerColors?.primaryBackgroundColorButton};
  }
  &:disabled {
    background-color: ${(props) => props.partnerColors?.backgroundColorInputDisabled
      || 'rgba(255, 255, 255, 0.5)'};
    color: ${(props) => props.partnerColors?.fontColorInputDisabled || '#545454'};
  }
`;

const StyledInputDefault = styled.input`
margin-bottom: 14px;

  width: ${(props) => props.width || '100%'};
  border: 1px solid ${
  (props) => (props.borderError ? props.partnerColors?.validation.error.backgroundColor : '#707070')};
  ${styleInputs};
  &:focus {
    border: 2px solid
      ${(props) => props.partnerColors?.primaryBackgroundColorButton};
  }
  &:disabled {
    background-color: ${(props) => props.partnerColors?.backgroundColorInputDisabled
      || 'rgba(255, 255, 255, 0.5)'};
    color: ${(props) => props.partnerColors?.fontColorInputDisabled || '#545454'};
  }
`;

const StyledFooter = styled.p`
  color: ${(props) => props.partnerColors?.primaryBackgroundColorButton};
  text-transform: uppercase;
  font-size: 12px;
  margin-top: -12px;
  margin-bottom: 14px;
`;

export function InputField(props) {
  const [visibleTooltip, setVisibleTooltip] = useState(false);
  const {
    placeHolder,
    onChange,
    onBlur,
    value,
    mask,
    maskmoney,
    id,
    type,
    disabled,
    tooltipInfo,
    tooltipWarning,
    errors,
    touched,
    footer,
    min,
    max,
  } = props;

  return (
    <div
      onMouseMove={() => tooltipInfo && setVisibleTooltip(true)}
      onMouseOut={() => tooltipInfo && setVisibleTooltip(false)}
      onBlur={onBlur}
    >
      {errors?.[id] && touched?.[id] && (
        <Tooltip
          type={tooltipWarning && !errors[id] ? 'warning' : 'error'}
          message={errors[id] || ''}
        />
      )}

      {tooltipInfo && (
        <Tooltip
          type="info"
          message={tooltipInfo}
          visibleTooltip={!errors[id] && visibleTooltip}
        />
      )}

      {mask && (
        <StyledInput
          id={id}
          borderError={touched?.[id] && errors?.[id] && true}
          type={type || 'text'}
          mask={mask}
          placeholder={placeHolder}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          partnerColors={props.partnerColors}
          disabled={disabled}
          touched={touched}
          min={min}
          max={max}
        />
      )}

      {!mask && !maskmoney && (
        <StyledInputDefault
          id={id}
          borderError={touched?.[id] && errors?.[id] && true}
          type={type || 'text'}
          placeholder={placeHolder}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          partnerColors={props.partnerColors}
          disabled={disabled}
          touched={touched}
          min={min}
          max={max}
        />
      )}

      {/* REACT-CURRENCY-FORMAT */}
      {maskmoney && (
        <MoneyInput
          id={id}
          type="text"
          borderError={touched?.[id] && errors?.[id] && true}
          placeholder={placeHolder}
          onChange={onChange}
          onBlur={onBlur}
          value={value}
          partnerColors={props.partnerColors}
          disabled={disabled}
          touched={touched}
          thousandSeparator="."
          decimalSeparator=","
          decimalScale={2}
          fixedDecimalScale
          prefix="R$ "
        />
      )}

      {footer && (
        <StyledFooter partnerColors={props.partnerColors}>
          {footer}
        </StyledFooter>
      )}
    </div>
  );
}

InputField.propTypes = {
  placeHolder: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  mask: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  maskmoney: PropTypes.bool,
  type: PropTypes.string,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  tooltipInfo: PropTypes.string,
  tooltipWarning: PropTypes.string,
  errors: PropTypes.object,
  touched: PropTypes.object,
  partnerColors: PropTypes.object,
  footer: PropTypes.string,
  min: PropTypes.string,
  max: PropTypes.string,
};
// export default injectPartnerStyle(InputField);
export default InputField;
