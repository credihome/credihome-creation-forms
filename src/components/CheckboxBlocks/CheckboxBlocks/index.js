import styled from 'styled-components';
import PropTypes from 'prop-types';
import Checkbox from '../Checkbox';
import device from '../../../config/devices';

const CheckboxBlocks = styled(Checkbox)`
  display: grid;
  column-gap: 14px;
  row-gap: 7px;
  grid-template-columns: 1fr 1fr;
  margin-bottom: 14px;
  justify-items: center;
  align-itens: center;
  @media ${device.laptop} {
    grid-template-columns: 1fr 1fr 1fr;
    column-gap: 5px;
    row-gap: 20px;
  }

  & label {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    color: #DEDEDE;
    box-sizing: border-box;
    cursor: pointer;
    transition: background-color 0.5s;
    border: 1px solid;
    border-color: ${props => (props.partnerColors || {}).primaryBackgroundColorButton  /* eslint-disable-line prettier/prettier */};
    border-radius: 5px;
    background-color: #fff;
    user-select: none;
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
    font-size: 15px;
    min-height: 78px;
    padding: 0 6px;
    font-weight: bold;
    @media ${device.mobileM} {
      font-size: 17px;
      padding: 0 12px;
    }
    @media ${device.laptop} {
      font-size: 13px;
      font-weight: normal;
      padding: 0 16px;
      min-height: 72px;
      color: #AFAFAF;
    }
    &:hover {
      background-color: #eee;
    }
  }

  & input {
    &:checked + label {
      background-color: ${props => (props.partnerColors || {}).primaryBackgroundColorButton /* eslint-disable-line prettier/prettier */};
      color: #fff;
    &:disabled + label {
      border-color: #ddd;
      color: #ddd;
      font-weight: bold;
      cursor: default;
      &:hover {
        background-color: inherit;
      }
    }
  }

`;

CheckboxBlocks.propTypes = {
  name: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.string,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  id: PropTypes.any.isRequired,
  className: PropTypes.string,
  partnerColors: PropTypes.object,
  tooltipWarning: PropTypes.string,
};

export default CheckboxBlocks;
