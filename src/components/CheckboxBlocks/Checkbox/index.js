import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Tooltip } from 'credihome-react-library';

const CheckboxStyled = styled.input.attrs((props) => ({
  type: 'checkbox',
  value: props.value,
  name: props.nameCheckbox,
  checked: props.defaultChecked,
  onChange: props.onChange,
}))`
  display: none;
`;

function Checkbox(props) {
  const {
    name,
    options,
    onChange,
    value,
    disabled,
    errors,
    touched,
    id,
    className,
    partnerColors,
    tooltipWarning,
  } = props;

  const [values, setValues] = useState(value || []);

  useEffect(() => {
    onChange(values);
  }, [values]);

  const addValues = (key) => {
    if (values.find((item) => item === key)) {
      setValues(values.filter((item) => item !== key));
    } else {
      setValues([...values, key]);
    }
  };

  return (
    <>
      {touched?.[id] && errors?.[id] && (
        <Tooltip
          type={tooltipWarning && !errors[id] ? 'warning' : 'error'}
          message={errors[id] || ''}
        />
      )}
      <div
        className={className}
        partnerColors={partnerColors}
        error={errors && touched && touched[id] && errors[id]}
      >
        {options.map((item) => (
          <React.Fragment key={`checkbox_${name}_${item.key}`}>
            <CheckboxStyled
              nameCheckbox={name}
              value={item.key}
              checked={values.find((i) => i === item.key)}
              id={`${name}_${item.key}`}
              onChange={() => !disabled && addValues(item.key)}
              disabled={disabled}
            />
            <label htmlFor={`${name}_${item.key}`}>{item.label}</label>
          </React.Fragment>
        ))}
      </div>
    </>
  );
}

Checkbox.propTypes = {
  name: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.array,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  id: PropTypes.any.isRequired,
  className: PropTypes.string,
  partnerColors: PropTypes.object,
  tooltipWarning: PropTypes.string,
};

export default Checkbox;
