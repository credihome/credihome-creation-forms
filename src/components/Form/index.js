import styled from 'styled-components';
import device from '../../config/devices';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
  & > div > input {
    margin: 0 0 14px 0;
  }
  & .row {
    display: flex;
    justify-content: space-between;
    & label:nth-child(2) {
      padding: 0 0 0 5px;
    }
  }
  @media ${device.mobileM} {
    padding: ${(props) => props.caracteristics && '10em 2em 42px'};
  }
  @media ${device.tablet} {
  }
  @media ${device.laptop} {
    padding: 0;
    max-height: 100vh;
  }
`;

export default Form;
