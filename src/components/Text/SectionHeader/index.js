import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';
import device from '../../../config/devices';

function sectionHeaderStyled(props) {
  const { className: propsClass, children, text } = props;
  return <h2 className={`${propsClass}`}>{children || text}</h2>;
}

const SectionHeader = styled(sectionHeaderStyled)`
  font-size: 10px;
  text-align: left;
  font-weight: bold;
  color: ${(props) => props.partnerColors.primaryTextColor};
  margin-bottom: 8px;
  margin-top: 0px;
  padding: 0;
  @media ${device.mobileM} {
    font-size: 12px;
  }
  @media ${device.tablet} {
    font-size: 14px;
  }
  @media ${device.laptop} {
    font-size: 16px;
  }
`;

sectionHeaderStyled.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  text: PropTypes.string,
};
// export default injectPartnerStyle(SectionHeader);
export default SectionHeader;
