import Title from './Title';
import Subtitle from './Subtitle';
import Paragraph from './Paragraph';
import LabelSmall from './LabelSmall';
import SectionHeader from './SectionHeader';

export {
  Title, Subtitle, Paragraph, LabelSmall, SectionHeader,
};
