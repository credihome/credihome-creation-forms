import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import device from '../../../config/devices';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';

function titleStyled(props) {
  const { className: propsClass } = props;
  return (
    <h1 className={`${propsClass}`}>
      {(props.children && props.children) || props.text}
    </h1>
  );
}
const Title = styled(titleStyled)`
  font-size: 18px;
  line-height: 21px;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
  color: ${(props) => props.partnerColors?.primaryTextColor};
  /* color: black; */
  @media ${device.mobileM} {
    font-size: 21px;
  }
  @media ${device.tablet} {
    font-size: 24px;
  }
  @media ${device.laptop} {
    font-size: 30px;
    margin: 0 0 10px 0;
  }
`;

titleStyled.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  text: PropTypes.string,
};

// export default injectPartnerStyle(Title);
export default Title;
