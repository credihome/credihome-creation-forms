import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';
import device from '../../../config/devices';

function subtitleStyled(props) {
  const {
    className: propsClass, children, text, textAlign,
  } = props;
  return (
    <h2 style={{ textAlign }} className={`${propsClass}`}>
      {children || text}
    </h2>
  );
}

const Subtitle = styled(subtitleStyled)`
  font-size: 15px;
  text-transform: ${(props) => (props.noTransformText ? 'none' : 'uppercase')};
  text-align: ${(props) => props.textAlign};
  font-weight: bold;
  color: ${(props) => props.partnerColors?.primaryTextColor};
  /* color: black; */
  margin: 0;
  @media ${device.mobileM} {
    font-size: 17px;
  }
  @media ${device.tablet} {
    font-size: 19px;
  }
  @media ${device.laptop} {
    font-size: 21px;
  }
`;

subtitleStyled.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  text: PropTypes.string,
  textAlign: PropTypes.string,
};

subtitleStyled.defaultProps = {
  textAlign: 'left',
};
// export default injectPartnerStyle(Subtitle);
export default Subtitle;
