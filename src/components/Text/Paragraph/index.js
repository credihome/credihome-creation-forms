import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';

import device from '../../../config/devices';

function paragraphStyled(props) {
  const { className: propsClass, children } = props;
  return <p className={`${propsClass}`}>{children}</p>;
}

const Paragraph = styled(paragraphStyled)`
  display: inline-block;
  font-size: 13px;
  width: 100%;
  color: ${(props) => props.partnerColors?.primaryTextColor};
  /* color: black; */
  @media ${device.mobileM} {
    font-size: 15px;
    text-align: center;
  }
  @media ${device.tablet} {
    font-size: 16px;
  }
  @media ${device.laptop} {
    font-size: ${(props) => props.fontSize || '16px'};
    padding: 0px 0;
    text-align: ${(props) => props.textAlign || 'left'};
  }
`;

paragraphStyled.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
};
// export default injectPartnerStyle(Paragraph);
export default Paragraph;
