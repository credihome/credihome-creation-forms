import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';
import device from '../../../config/devices';

function labelStyled(props) {
  const { className: propsClass, inputFor } = props;
  return (
    <label htmlFor={inputFor} className={`${propsClass}`}>
      {props.text}
      {props.children}
    </label>
  );
}

const LabelSmall = styled(labelStyled)`
  font-size: 10px;
  line-height: 12px;
  flex-direction: column;
  justify-content: center;
  display: flex;
  width: 100%;
  padding: 0 0 0 0;
  box-sizing: border-box;
  font-weight: bold;
  color: ${(props) => props.partnerColors?.primaryTextColor};
  /* color: black; */
  @media ${device.mobileM} {
    font-size: 11px;
    line-height: 12px;
  }
  @media ${device.tablet} {
    font-size: 12px;
    line-height: 26px;
  }
  @media ${device.laptop} {
    font-size: 13px;
    line-height: 28px;
  }
`;

labelStyled.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  inputFor: PropTypes.string.isRequired,
};
// export default injectPartnerStyle(LabelSmall);
export default LabelSmall;
