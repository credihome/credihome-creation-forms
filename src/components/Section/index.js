/* eslint-disable indent */
import styled from 'styled-components';
import device from '../../config/devices';
// import injectPartnerStyle from '../../containers/App/partnerStyles';

const Section = styled.div`
  /* overflow: auto; */
  /* position: relative; */
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: center;
  font-family: inherit;
  width: 100%;
  /* margin-top: 60px; */
  /* background-color: ${(props) => (props.partnerColors || {}).backgroundColorSection}; */
  /* background-color: orange; */
  & > div {
    margin: 0 0 30px 0;
    width: 100%;
  }
  & div.row {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
  }
  @media ${device.laptop} {
    justify-content: center;
    /* position: absolute; */
    /* right: 0; */
    /* top: 0; */
    /* min-height: 100vh; */
    /* height: 100vh; */
    /* border-left: 10px solid
      ${(props) => (props.partnerColors || {}).borderLeftColorSection}; */
    /* border-left: 10px solid orange; */
    /* border-top: 0; */
    & > div {
      text-align: left;
      margin: 20px 0;
      width: 100%;
      &:last-child {
        margin: 5px 0 0 0;
        max-height: 100vh;
      }
    }
    &:after {
      content: '';
      position: fixed;
      bottom: 0;
      display: block;
      width: 45%;
      height: 100vh;
      z-index: -1;
    }
  }
`;

// export default injectPartnerStyle(Section);
export default Section;
