import styled from 'styled-components';
import PropTypes from 'prop-types';
import Radio from '../Radio';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';

const RadioBlocks = styled(Radio)`
  display: flex;
  flex-direction: column;
  margin-left: -9px;
  margin-top: -15px;
  margin-bottom: 14px;
  & label {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    min-height: 37px;
    width: calc(100% - 9px);
    color: #afafaf;
    box-sizing: border-box;
    cursor: pointer;
    transition: background-color 0.5s;
    border: 1px solid;
    border-color: ${
  (props) => props.partnerColors?.primaryBackgroundColorButton};
    border-radius: 5px;
    background-color: #fff;
    margin-left: 9px;
    margin-top: 15px;
    font-size: 13px;
    -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
    -khtml-user-select: none; /* Konqueror HTML */
    -moz-user-select: none; /* Old versions of Firefox */
    -ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none;
    &:hover {
      background-color: #eee;
    }
  }

  & input {
    &:checked + label {
      background-color: ${
  (props) => props.partnerColors?.primaryBackgroundColorButton};
      color: #fff;
    &:disabled + label {
      border-color: #ddd;
      color: #ddd;
      font-weight: bold;
      cursor: default;
      &:hover {
        background-color: inherit;
      }
    }
  }
`;

RadioBlocks.propTypes = {
  name: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChangeItem: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.string,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  id: PropTypes.any.isRequired,
  className: PropTypes.string,
  partnerColors: PropTypes.object,
  tooltipWarning: PropTypes.string,
};

// export default injectPartnerStyle(RadioBlocks);
export default RadioBlocks;
