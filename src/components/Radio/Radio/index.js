import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Tooltip } from 'credihome-react-library';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';

const RadioStyled = styled.input.attrs((props) => ({
  type: 'radio',
  value: props.value,
  name: props.nameRadio,
  checked: props.defaultChecked,
  onChange: props.onChange,
}))`
  display: none;
`;

function Radio(props) {
  const {
    name,
    options,
    onChangeItem,
    value,
    disabled,
    errors,
    touched,
    id,
    className,
    partnerColors,
    tooltipWarning,
  } = props;

  return (
    <div>
      {errors && touched && touched[id] && errors[id] && (
        <Tooltip
          type={tooltipWarning && !errors[id] ? 'warning' : 'error'}
          message={errors[id] || ''}
        />
      )}
      <div
        className={className}
        partnerColors={partnerColors}
        error={errors && touched && touched[id] && errors[id]}
      >
        {options.map((item) => (
          <React.Fragment key={`radio_${name}_${item.key}`}>
            <RadioStyled
              nameRadio={name}
              value={item.key}
              defaultChecked={value === item.key}
              id={`${name}_${item.key}`}
              onChange={() => !disabled && onChangeItem(item.key)}
              disabled={disabled}
            />
            <label htmlFor={`${name}_${item.key}`}>{item.label}</label>
          </React.Fragment>
        ))}
      </div>
    </div>
  );
}

Radio.propTypes = {
  name: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChangeItem: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.string,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  id: PropTypes.any.isRequired,
  className: PropTypes.string,
  partnerColors: PropTypes.object,
  tooltipWarning: PropTypes.string,
};

// export default injectPartnerStyle(Radio);
export default Radio;
