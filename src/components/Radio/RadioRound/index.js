/* eslint-disable indent */
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Radio from '../Radio';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';

const RadioRound = styled(Radio)`
  display: flex;
  margin-top: ${(props) => props.error && '0 !important'};
  & label {
    width: 50%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    height: 26px;
    min-width: 75px;
    color: #fff;
    padding: 5px 20px;
    box-sizing: border-box;
    cursor: pointer;
    border: none;
    background-color: ${(props) => (props.partnerColors || {}).primaryBackgroundColorButton};
    transition: background-color 0.5s;
    font-size: 13px;
    &:hover {
      font-weight: bold;
      background-color: ${(props) => (props.partnerColors || {}).hoverBackgroundColorButton
        || (props.partnerColors || {}).primaryBackgroundColorButton};
    }
    &:nth-child(2) {
      border-radius: 15px 0 0 15px;
    }
    &:not(:last-child) {
      border-right: none;
      margin-right: 1px;
    }
    &:last-child {
      border-radius: 0 15px 15px 0;
    }
  }

  & input {
    &:disabled + label {
      border-color: #ddd;
      color: #aaa;
      font-weight: inherit;
      cursor: default;
    }
    &:checked + label {
      font-weight: bold;
      background-color: ${(props) => (props.partnerColors || {}).activeBackgroundColorButton
        || (props.partnerColors || {}).primaryBackgroundColorButton};
    }
  }
  @media (max-width: 375px) {
    & label {
      min-width: auto;
    }
  }
`;

RadioRound.propTypes = {
  name: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChangeItem: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.string,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  id: PropTypes.any.isRequired,
  className: PropTypes.string,
  partnerColors: PropTypes.object,
  tooltipWarning: PropTypes.string,
};

// export default injectPartnerStyle(RadioRound);
export default RadioRound;
