/* eslint-disable indent */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */

import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';
import device from '../../../config/devices';

function submitStyled(props) {
  const {
    className: propsClass,
    onClick: propsEvent,
    children,
    text,
    tabIndex,
    icon,
    disabled,
    typeButton,
  } = props;

  return (
    <button
      disabled={disabled}
      className={`${propsClass}`}
      tabIndex={tabIndex}
      onClick={propsEvent}
      type={typeButton || 'submit'}
      width={props.width}
    >
      {icon && <i className={icon} />}
      {children || text}
    </button>
  );
}

const Submit = styled(submitStyled)`
  cursor: pointer;
  padding: 5px 15px;
  margin: ${(props) => props.margin || '5px 0 16px 0'};
  width: 100%;
  margin-bottom: 16px;
  min-width: ${(props) => props.width || '100%'};
  max-width: ${(props) => props.width || '100%'};
  min-height: 25px;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => props.partnerColors?.primaryBackgroundColorButton};
  color: #fff;
  text-transform: ${(props) => (props.noTransformText ? 'none' : 'uppercase')};
  border: none;
  font-size: 11px;
  flex-grow: 1;
  font-weight: ${(props) => (props.noTransformText ? 'normal' : 'bold')};
  &:hover {
    font-weight: bold;
    background-color: ${(props) => props.partnerColors?.hoverBackgroundColorButton
      || props.partnerColors?.primaryBackgroundColorButton};
  }
  &:active {
    background-color: ${(props) => props.partnerColors?.activeBackgroundColorButton
      || props.partnerColors?.primaryBackgroundColorButton};
  }
  @media ${device.mobileM} {
    height: 36px;
    font-size: 13px;
    min-width: ${(props) => props.width || '100%'};
  }
  @media ${device.tablet} {
    height: ${(props) => (props.height ? props.height : '42px')};
    font-size: ${(props) => (props.fontSize ? props.fontSize : '16px')};
  }
  & i {
    margin-right: 12px;
    margin-bottom: -4px;
    font-size: 14px;
  }
  &:disabled {
    color: #afafaf;
    background-color: #e8e8e8;
    & i {
      background-color: #afafaf;
    }
  }
`;

submitStyled.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  text: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  tabIndex: PropTypes.func,
  width: PropTypes.string,
  disabled: PropTypes.bool,
  icon: PropTypes.string,
  typeButton: PropTypes.string,
};

// export default injectPartnerStyle(Submit);
export default Submit;
