/* eslint-disable indent */
import styled from 'styled-components';
import PropTypes from 'prop-types';
import submitStyled from '../Submit';
// import injectPartnerStyle from '../../../containers/App/partnerStyles';

const Back = styled(submitStyled).attrs({ typeButton: 'button' })`
  background-color: transparent;
  font-weight: normal;
  color: ${(props) => (props.invert
      ? props.partnerColors?.primaryBackgroundColorButton
      : props.partnerColors?.secondaryBackgroundColorButton)};
  border: 2px solid
    ${(props) => (props.invert
        ? props.partnerColors?.primaryBackgroundColorButton
        : props.partnerColors?.secondaryBackgroundColorButton)
      };
  &:hover,
  &:active {
    font-weight: bold;
    border-width: 3px;
    background-color: inherit;
  }
`;

Back.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  text: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

// export default injectPartnerStyle(Back);
export default Back;
