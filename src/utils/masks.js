import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe';

export const numberUnmask = (value) => (typeof value === 'string' ? parseFloat(value.replace(/[^0-9,]+/g, '').replace(',', '.'), 10) : value);

export const integerMask = createNumberMask({
  prefix: '',
  integerLimit: 1000,
  thousandsSeparatorSymbol: '',
  requireDecimal: false,
  allowDecimal: false,
  suffix: '', // This will put the dollar sign at the end, with a space.
});

export const phoneMask = [
  '(',
  /[1-9]/,
  /\d/,
  ')',
  ' ',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];

export const phoneUnmask = (value) => (typeof value === 'string' ? value.replace(/[^0-9]+/g, '') : value);

export const zipcodeMask = [
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
];

export const dateMask = [
  /[0-3]/,
  /\d/,
  '/',
  /[0-1]/,
  /\d/,
  '/',
  /[1-2]/,
  /\d/,
  /\d/,
  /\d/,
];

export const cpfMask = [
  /\d/,
  /\d/,
  /\d/,
  '.',
  /\d/,
  /\d/,
  /\d/,
  '.',
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
];

export const cnpjMask = [
  /\d/,
  /\d/,
  '.',
  /\d/,
  /\d/,
  /\d/,
  '.',
  /\d/,
  /\d/,
  /\d/,
  '/',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
];

export const autoCorrectedDatePipe = createAutoCorrectedDatePipe('dd/dd/yyyy');
