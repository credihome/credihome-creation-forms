const intlVerification = (intl, form, id, intlMessages, userProvided, style, strMessages, values) => {
  if (intl) {
    return intl.formatMessage(intl.messages?.credihomeForms?.[form]?.[id] || intlMessages[id], values);
  }
  if (userProvided && userProvided[id]) {
    if (typeof userProvided[id] === 'string') {
      return userProvided[id];
    }
    return userProvided[id](values);
  }
  if (style?.utils && style?.utils[form] && style?.utils[form][id]) {
    if (typeof style.utils[form][id] === 'string') {
      return style.utils[form][id];
    }
    return style.utils[form][id](values);
  }
  if (strMessages[id]) {
    if (typeof strMessages[id] === 'string') {
      return strMessages[id];
    }
    return strMessages[id](values);
  }
  return undefined;
};

export default intlVerification;
