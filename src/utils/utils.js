/* eslint-disable consistent-return */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
import * as yup from 'yup';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);

const formatter = new Intl.NumberFormat('pt-BR', {
  style: 'currency',
  currency: 'BRL',
  minimumFractionDigits: 2,
});

function CustomURLSearchParams(name) {
  const results = new RegExp(`[?&]${name}=([^&#]*)`).exec(window.location.href);
  if (results == null) {
    return null;
  }

  return decodeURI(results[1]) || 0;
}

const utils = {
  CustomURLSearchParams,
  jsMoney: (regex) => formatter.format(regex),
  customYupSchemas: {
    date: (format, requiredMessage) => {
      let dateSchema = yup.date().nullable();
      dateSchema = requiredMessage
        ? dateSchema.required(requiredMessage)
        : dateSchema.notRequired();

      return dateSchema.transform((value, originalValue) => {
        if (!originalValue) return;
        if (originalValue.length < 10) return;
        const date = dayjs(originalValue, format || 'YYYY-MM-DD');
        return date.isValid() ? date.toDate() : null;
      });
    },
    masked: (regex) => yup
      .string()
      .transform((value) => value && value.replace(regex || /\D+/g, '')),
    moneyMasked: (regex) => yup
      .string()
      .transform(
        (value) => value && value.replace(regex || /[^\d,]+/g, '').replace(',', '.'),
      ),
  },
  isUnderaged: (dateOfBirth) => dayjs().diff(dateOfBirth, 'years') < 18,
  isTooOld: (dateOfBirth) => dayjs().diff(dateOfBirth, 'years') > 80,
  isCPFValid: (_cpf) => {
    if (!_cpf) return false;
    // Validação feita pela Receita Federal, de acordo com: https://www.devmedia.com.br/validar-cpf-com-javascript/23916
    const strCPF = String(_cpf.replace(/[^\d]+/g, ''));

    if (
      strCPF.length !== 11
      || strCPF === '00000000000'
      || strCPF === '11111111111'
      || strCPF === '22222222222'
      || strCPF === '33333333333'
      || strCPF === '44444444444'
      || strCPF === '55555555555'
      || strCPF === '66666666666'
      || strCPF === '77777777777'
      || strCPF === '88888888888'
      || strCPF === '99999999999'
      || strCPF === '12345678909'
      || !strCPF
    ) return false;

    let Soma = 0;
    let Resto;

    for (let i = 1; i <= 9; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;
    for (let i = 1; i <= 10; i++) Soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(strCPF.substring(10, 11))) return false;
    return true;
  },
  isCNPJValid: (_cnpj) => {
    if (!_cnpj) return false;

    const cnpj = String(_cnpj.replace(/[^\d]+/g, ''));

    if (cnpj.length !== 14) return false;

    // Elimina CNPJs invalidos conhecidos
    if (
      cnpj === '00000000000000'
      || cnpj === '11111111111111'
      || cnpj === '22222222222222'
      || cnpj === '33333333333333'
      || cnpj === '44444444444444'
      || cnpj === '55555555555555'
      || cnpj === '66666666666666'
      || cnpj === '77777777777777'
      || cnpj === '88888888888888'
      || cnpj === '99999999999999'
    ) return false;

    // Valida DVs
    let tamanho = cnpj.length - 2;
    let numeros = cnpj.substring(0, tamanho);
    const digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2) pos = 9;
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
    if (resultado !== parseInt(digitos.charAt(0))) return false;

    tamanho += 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
    if (resultado !== parseInt(digitos.charAt(1))) return false;

    return true;
  },
  isIncomeValid: (income) => income < 99000000, // 99 milhões
  isRealtyPriceValid: (realtyValue) => realtyValue < 99999999.99, // 99.999.999,99
  isFullNameValid: (name) => name && name.trim().indexOf(' ') !== -1,
  removeNullValues: (obj) => {
    const data = { ...obj };
    for (const key in data) {
      const value = data[key];
      if (value === null) delete data[key];
    }
    return data;
  },
  unmask: (val) => val && String(val).replace(/\D+/g, ''),
  unmaskWithLetters: (val) => val
    && String(val)
      .toLowerCase()
      .replace(/\W+/g, ''),
  objectToArray: obj =>
    Object.keys(obj).map(name => ({ id: name, ...obj[name] })),
  removeAccents: val => val.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
};

export default utils;
