import { defineMessages } from 'react-intl';

const scope = 'credihomeForms.validation';

const intlMessages = defineMessages({
  ValidationMinAge: {
    id: `${scope}.ValidationMinAge`,
    defaultMessage: 'O usuário deve ser maior de idade',
  },
  ValidationMaxAge: {
    id: `${scope}.ValidationMaxAge`,
    defaultMessage: 'O usuário deve ter menos de 80 anos',
  },
  ValidationInvalidDate: {
    id: `${scope}.ValidationInvalidDate`,
    defaultMessage: 'Data Inválida',
  },
  ValidationFullname: {
    id: `${scope}.ValidationFullname`,
    defaultMessage: 'Por favor informe o nome completo',
  },
  ValidationRequiredField: {
    id: `${scope}.ValidationRequiredField`,
    defaultMessage: 'Campo obrigatório',
  },
  ValidationInvalidEmail: {
    id: `${scope}.ValidationInvalidEmail`,
    defaultMessage: 'Email inválido',
  },
  ValidationInvalidCPF: {
    id: `${scope}.ValidationInvalidCPF`,
    defaultMessage: 'CPF inválido',
  },
  ValidationInvalidPhone: {
    id: `${scope}.ValidationInvalidPhone`,
    defaultMessage: 'Celular inválido',
  },
  ValidationOptionsRequired: {
    id: `${scope}.homepage.ValidationOptionsRequired`,
    defaultMessage: 'Escolha um dos produtos abaixo.',
  },
  ValidationFieldRequired: {
    id: `${scope}.homepage.ValidationFieldRequired`,
    defaultMessage: 'Campo obrigatório!',
  },
  ValidationRealtyPriceMinimum: {
    id: `${scope}.homepage.ValidationRealtyPriceMinimum`,
    defaultMessage: 'Valor mínimo do imóvel: ',
  },
  ValidationRealtyPriceMaximum: {
    id: `${scope}.homepage.ValidationRealtyPriceMaximum`,
    defaultMessage: 'Valor máximo do imóvel: ',
  },
  ValidationMinimumMortgageValue: {
    id: `${scope}.homepage.ValidationMinimumMortgageValue`,
    defaultMessage: 'Valor mínimo do financiamento é ',
  },
  ValidationHomeEquityOverLTV: {
    id: `${scope}.homepage.ValidationHomeEquityOverLTV`,
    defaultMessage: 'Valor maior do que {percentage} do imóvel.',
  },
  ValidationDuplicateCPF: {
    id: `${scope}.ValidationDuplicateCPF`,
    defaultMessage: 'CPF já utilizado',
  },
  ValidationSameSpouseCPFUser: {
    id: `${scope}.ValidationSameSpouseCPFUser`,
    defaultMessage: 'CPF do cônjuge deve ser diferente do seu CPF',
  },
  ValidationSameSpouseCPFProponent: {
    id: `${scope}.ValidationSameSpouseCPFProponent`,
    defaultMessage: 'CPF do cônjuge é igual ao do proponente',
  },
  ValidationSpouseCPFHasNoSpouse: {
    id: `${scope}.ValidationSpouseCPFHasNoSpouse`,
    defaultMessage: 'O CPF informado não possui parceiro, edite-o',
  },
  ValidationUniqueSpouseCPF: {
    id: `${scope}.ValidationUniqueSpouseCPF`,
    defaultMessage: 'Este CPF possui outro relacionamento de união',
  },
  ValidationInvalidRG: {
    id: `${scope}.ValidationInvalidRG`,
    defaultMessage: 'RG inválido',
  },
  ValidationDuplicateRG: {
    id: `${scope}.ValidationDuplicateRG`,
    defaultMessage: 'RG já utilizado',
  },
  ValidationInvalidArea: {
    id: `${scope}.ValidationInvalidArea`,
    defaultMessage: 'Deve ser maior que zero ou menor que um milhão',
  },
  ValidationInvalidMaxAge: {
    id: `${scope}.ValidationInvalidMaxAge`,
    defaultMessage: 'Idade acima do  limite',
  },
  ValidationInvalidMonthlyFees: {
    id: `${scope}.ValidationInvalidMonthlyFees`,
    defaultMessage: 'O valor deve ser maior do que zero e menor do que um milhão',
  },
  ValidationMoreThanZero: {
    id: `${scope}.ValidationMoreThanZero`,
    defaultMessage: 'Tempo deve ser 0 ou mais',
  },
  ValidationLessThanYear: {
    id: `${scope}.ValidationLessThanYear`,
    defaultMessage: 'Valor em meses deve ser menor que um ano',
  },
  ValidationInvalidCNPJ: {
    id: `${scope}.ValidationInvalidCNPJ`,
    defaultMessage: 'CNPJ inválido',
  },
});

/**
* Objeto que deve ser recebido em `validationMessages`.<br>Se estiver utilizando intl, colocar como
* id da mensagem `credihomeForms.validation.${nome_do_campo}` onde `nome_do_campo` deve
* ser uma das chaves abaixo.
* @category Variáveis utilizadas em todos formulários
* @alias Mensagens de validação
* @const
* @type {Object}
* @example
* const validationMessages = {
  ValidationMinAge: 'O usuário deve ser maior de idade',
  ValidationMaxAge: 'O usuário deve ter menos de 80 anos',
  ValidationInvalidDate: 'Data Inválida',
  ValidationFullname: 'Por favor informe o nome completo',
  ValidationRequiredField: 'Campo obrigatório',
  ValidationInvalidEmail: 'Email inválido',
  ValidationInvalidCPF: 'CPF inválido',
  ValidationInvalidPhone: 'Celular inválido',
  ValidationOptionsRequired: 'Escolha um dos produtos abaixo.',
  ValidationRealtyPriceMinimum: 'Valor mínimo do imóvel: ',
  ValidationRealtyPriceMaximum: 'Valor máximo do imóvel: ',
  ValidationMinimumMortgageValue: 'Valor mínimo do financiamento é ',
  ValidationHomeEquityOverLTV: (val) => `Valor maior do que ${val.percentage} do imóvel.`,
  ValidationInvalidRG: 'RG inválido',
  ValidationDuplicateRG: 'RG já utilizado',
  ValidationInvalidArea: 'Deve ser maior que zero ou menor que um milhão',
  ValidationInvalidMaxAge: 'Idade acima do  limite',
  ValidationInvalidMonthlyFees: 'O valor deve ser maior do que zero e menor do que um milhão',
  ValidationInvalidCNPJ: 'CNPJ inválido',
};
*/
const strMessages = {
  ValidationMinAge: 'O usuário deve ser maior de idade',
  ValidationMaxAge: 'O usuário deve ter menos de 80 anos',
  ValidationInvalidDate: 'Data Inválida',
  ValidationFullname: 'Por favor informe o nome completo',
  ValidationRequiredField: 'Campo obrigatório',
  ValidationInvalidEmail: 'Email inválido',
  ValidationInvalidCPF: 'CPF inválido',
  ValidationInvalidPhone: 'Celular inválido',
  ValidationOptionsRequired: 'Escolha um dos produtos abaixo.',
  ValidationRealtyPriceMinimum: 'Valor mínimo do imóvel: ',
  ValidationRealtyPriceMaximum: 'Valor máximo do imóvel: ',
  ValidationMinimumMortgageValue: 'Valor mínimo do financiamento é ',
  ValidationHomeEquityOverLTV: (val) => `Valor maior do que ${val.percentage} do imóvel.`,
  ValidationDuplicateCPF: 'CPF já utilizado',
  ValidationSameSpouseCPFUser: 'CPF do cônjuge deve ser diferente do seu CPF',
  ValidationSameSpouseCPFProponent: 'CPF do cônjuge é igual ao do proponente',
  ValidationSpouseCPFHasNoSpouse: 'O CPF informado não possui parceiro, edite-o',
  ValidationUniqueSpouseCPF: 'Este CPF possui outro relacionamento de união',
  ValidationInvalidRG: 'RG inválido',
  ValidationDuplicateRG: 'RG já utilizado',
  ValidationInvalidArea: 'Deve ser maior que zero ou menor que um milhão',
  ValidationInvalidMaxAge: 'Idade acima do  limite',
  ValidationInvalidMonthlyFees: 'O valor deve ser maior do que zero e menor do que um milhão',
  ValidationMoreThanZero: 'Tempo deve ser 0 ou mais',
  ValidationLessThanYear: 'Valor em meses deve ser menor que um ano',
  ValidationInvalidCNPJ: 'CNPJ inválido',
};

export { intlMessages, strMessages };
