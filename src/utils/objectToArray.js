const objectToArray = (obj) => Object.keys(obj).map((name) => ({ id: name, ...obj[name] }));

export default objectToArray;
