
    window.reactComponents = {};

    window.vueComponents = {};

  
      import React from "react";

      import ReactDOM from "react-dom";


      import ReactWrapper from '../node_modules/better-docs/lib/react-wrapper.js';

      window.React = React;

      window.ReactDOM = ReactDOM;

      window.ReactWrapper = ReactWrapper;

    
    import './styles/reset.css';

    import './styles/iframe.css';

  import Component0 from '../src/lib/Contato/index.js';
reactComponents['Contato'] = Component0;

import Component1 from '../src/lib/DadosPessoais/index.js';
reactComponents['DadosPessoais'] = Component1;

import Component2 from '../src/lib/Documento/index.js';
reactComponents['Documento'] = Component2;

import Component3 from '../src/lib/Endereco/index.js';
reactComponents['Endereco'] = Component3;

import Component4 from '../src/lib/Finalidade/index.js';
reactComponents['Finalidade'] = Component4;

import Component5 from '../src/lib/FormStructure/index.js';
reactComponents['FormStructure'] = Component5;

import Component6 from '../src/lib/InfoProfissional/index.js';
reactComponents['InfoProfissional'] = Component6;

import Component7 from '../src/lib/Tempo/index.js';
reactComponents['Tempo'] = Component7;

import Component8 from '../src/lib/ValorOperacao/index.js';
reactComponents['ValorOperacao'] = Component8;