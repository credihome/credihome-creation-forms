import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import postcss from 'rollup-plugin-postcss';
import resolve from '@rollup/plugin-node-resolve';
import url from '@rollup/plugin-url';
import svgr from '@svgr/rollup';
import json from '@rollup/plugin-json';
import pkg from './package.json';

export default {
  input: 'src/index.js',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: true,
    },
  ],
  external: ['axios', 'styled-components'],
  plugins: [
    peerDepsExternal(),
    resolve({
      preferBuiltins: true,
    }),
    babel({
      babelHelpers: 'bundled',
      // exclude: 'node_modules/**',
      plugins: [
        'babel-plugin-styled-components',
      ],
    }),
    json(),
    commonjs({
      ignoreGlobal: true,
      include: /node_modules/,
    }),
    url(),
    svgr(),
    postcss({
      modules: true,
    }),
  ],
};
